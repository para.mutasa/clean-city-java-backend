package com.cleancity.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "sms")
public class sendSMS {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;
    @Column(name = "message_id")
    private String messageId;
    @Column(name = "phone_number")
    private String phoneNumber;
    @Column(name = "notification_url")
    private String notificationUrl;
    @Column(name = "booking_id")
    private String bookingId;
    @Column(name = "service_name")
    private String serviceName;
    @Column(name = "status")
    private String status;
    @Column(name = "message_txt")
    private String messageTxt;
    @Column(name = "sender")
    private String sender;
}
