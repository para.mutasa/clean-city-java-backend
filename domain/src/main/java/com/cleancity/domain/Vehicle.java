package com.cleancity.domain;

import com.cleancity.common.enums.VehicleStatus;
import com.cleancity.domain.embeddables.PersistenceDates;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "vehicle")
public class Vehicle {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "vehicle_id")
    private Long vehicleId;
    @Column(name = "model")
    private String model;
    @Column(name = "license_plate_number")
    private String licensePlateNumber;
    @Column(name = "capacity")
    private String capacity;
    @Column(name = "franchise_id")
    private Long franchiseId;
    @Column(name = "vehicle_status")
    @Enumerated(value=EnumType.STRING)
    private VehicleStatus vehicleStatus;

    @Column(name = "make")
    private String make;

    @Column(name = "year")
    @JsonFormat(shape = JsonFormat.Shape.STRING,pattern = "dd/MM/yyyy")
    private LocalDate year;

    @Embedded
    @JsonIgnore
    private PersistenceDates persistenceDates;

}
