package com.cleancity.domain;

import com.cleancity.common.enums.CollectionType;
import com.cleancity.common.enums.Status;
import com.cleancity.common.enums.VUnit;
import com.cleancity.domain.embeddables.PersistenceDates;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;


@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Table(name = "collection_subtype")
public class CollectionSubType {



    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "collection_type")
    @Enumerated(value= EnumType.STRING)
    private CollectionType collectionType;
    @Column(name = "v_sub_type")
    private String vSubType;
    @Column(name = "status")
    @Enumerated(value= EnumType.STRING)
    private Status status;
    @Column(name = "v_show")
    private String vShow;
    @Column(name = "cost_per_unit")
    private Double costPerUnit;
    @Column(name = "rate_per_km")
    private Double ratePerKm;
    @Column(name = "i_max_qty")
    private Double iMaxQty;
    @Column(name = "v_unit")
    private VUnit vUnit;
    @Column(name = "quotation_validity_days")
    private Long quotationValidityDays;



    @Embedded
    @JsonIgnore
    private PersistenceDates persistenceDates;




}
