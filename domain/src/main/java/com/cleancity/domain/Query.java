package com.cleancity.domain;


import com.cleancity.common.enums.QueryStatus;
import com.cleancity.domain.embeddables.PersistenceDates;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Table(name = "query")
public class Query {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;
    @Column(name = "phone")
    private String phone;
    @Column(name = "email")
    private String email;
    @Column(name = "customer_id")
    private Long customerId;
    @Column(name = "message")
    private String message;
    @Column(name = "status")
    @Enumerated(value= EnumType.STRING)
    private QueryStatus status;
    @Column(name = "fullname")
    private String fullname;
    @Embedded
    @JsonIgnore
    private PersistenceDates persistenceDates;





}
