package com.cleancity.domain.dto;

import com.cleancity.common.enums.VehicleStatus;
import lombok.Data;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;

@Data
public class VehicleDTO {
    private String model;
    private String licensePlateNumber;
    private String capacity;
    private Long franchiseId;
    private VehicleStatus vehicleStatus;
    private String make;
    private LocalDate year;


}
