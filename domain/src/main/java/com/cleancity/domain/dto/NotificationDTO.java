package com.cleancity.domain.dto;

import lombok.Data;

@Data
public class NotificationDTO {


    private Integer phoneNumber;
    private String message;
}
