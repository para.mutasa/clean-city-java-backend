package com.cleancity.domain.dto;

import lombok.Data;

@Data
public class CityDTO {
    private String cityName;
    private String country;
    private String countryCode;
    private String province;
}
