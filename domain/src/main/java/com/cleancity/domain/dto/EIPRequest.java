package com.cleancity.domain.dto;

import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
public class EIPRequest {
    private String source;
    private String sourceMsisdn;
    private String destinationMsisdn;
    private String merchantCode;
    private Double amount;
    private String reference;
    private String merchantPin;
    private String currency;
    private LocalDateTime transactionTime;
    private String returnUrl;
}
