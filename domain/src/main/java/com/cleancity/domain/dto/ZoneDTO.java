package com.cleancity.domain.dto;


import lombok.Data;

@Data
public class ZoneDTO {

    private String zoneCode;
    private String zoneName;
    private String zoneLocation;
    private String description;
    private String narration;
    private String activityType;
    private String zoneAdministrator;
    private String countryCode;
    private String countryCity;
    private String zoneTypeCode;

}
