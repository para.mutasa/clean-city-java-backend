package com.cleancity.domain.dto;

import lombok.Data;

import java.time.LocalDateTime;


@Data
public class ZonePricingDTO {

    private String zoneCollectionType;

    private String zoneType;

    private Double zoneCostPerKm;

    private Double zoneCostPerBag;

    private Long maxBag;

    private Long maxTonnage;

    private String created_by;

}
