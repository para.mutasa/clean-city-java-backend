package com.cleancity.domain.dto;

import lombok.Data;

@Data
public class ContactUsDTO {
    private String fullname;
    private String phone;
    private String email;
    private String message;


}
