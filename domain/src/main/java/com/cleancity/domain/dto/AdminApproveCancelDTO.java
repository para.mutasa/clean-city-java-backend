package com.cleancity.domain.dto;

import lombok.Data;

@Data
public class AdminApproveCancelDTO {
  private Long userId;
  private Long bookingId;
}
