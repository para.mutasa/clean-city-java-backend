package com.cleancity.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class EcocashRequestCallbackDTO {
    public EcocashRequestCallbackDTO(String paymentReference) {
        this.paymentReference = paymentReference;
    }
// {responseCode=200, responseMessage=COMPLETED, paymentReference=PO1wER9047, receiptNumber=MP220226.1442.A31123, transactionStatus=SUCCESS}
    private int responseCode;
    private String responseMessage;
    private String paymentReference;
    private String receiptNumber;
    private String transactionStatus;
}
