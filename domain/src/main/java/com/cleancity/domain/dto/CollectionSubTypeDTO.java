package com.cleancity.domain.dto;


import com.cleancity.common.enums.CollectionType;
import com.cleancity.common.enums.VUnit;
import lombok.Data;

import javax.persistence.Column;

@Data
public class CollectionSubTypeDTO {

    private CollectionType collectionType;


    private String vSubType;

    private String vShow;

    private Double costPerUnit;

    private Double ratePerKm;

    private Double iMaxQty;

    private VUnit vUnit;

    private Long quotationValidityDays;

}
