package com.cleancity.domain.dto;

import com.cleancity.common.enums.Status;
import com.cleancity.domain.Customer;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import java.util.Map;

@Data
@Slf4j
//@JsonIgnoreProperties(ignoreUnknown = true)
public class ElogisticCustomerDTO {
  private Long iUserId;
  private String VEmail;
  private String countryCode;
  private String created_date;
  private String creation_status;
  private String modified_date;
  private String vFirstName;
  private String vLastName;
  private String companyName;
  private String partnerType;
  private String vCaddress;
  private String zimraBpNummber;
  private String vPhone;
/*

public ElogisticCustomerDTO getElogisticsCustomerDTOFromMao(Map<String,Object> map){


}
*/


    public Customer getCustomer(){
      //log.info("GET USER ID:  {}",getIUserId());
  //    log.info("GET USER ID: iUserId ::   {}",iUserId);
      return Customer.builder()
              .address(getVCaddress())
              .name(getVFirstName())
              .surname(getVLastName())
              .email(getVEmail())
              .status(Status.INACTIVE)
              .eLogisticsUserId(getIUserId())
              .partnerType(getPartnerType())
              .companyName(getCompanyName())
              .zimraBPNumber(getZimraBpNummber())
              .phoneNo(getVPhone())
              .build();
  }
}
