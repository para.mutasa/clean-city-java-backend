package com.cleancity.domain.dto;

import lombok.Data;

@Data
public class ResetPasswordDTO {

   private String email;
   private String resetToken;
   private String newpassword;
}
