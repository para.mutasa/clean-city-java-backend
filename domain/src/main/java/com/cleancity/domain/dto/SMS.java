package com.cleancity.domain.dto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data

    @Builder
    @AllArgsConstructor
    @NoArgsConstructor
    public class SMS {
        private String serviceName;
        private String msisdn;
        private String messageTxt;
    }

