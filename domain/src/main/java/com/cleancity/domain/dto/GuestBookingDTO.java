package com.cleancity.domain.dto;

import com.cleancity.common.enums.CollectionType;
import com.cleancity.common.enums.Recurrence;
import com.cleancity.common.enums.VUnit;
import com.cleancity.common.enums.WasteType;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.Email;
import java.time.LocalDateTime;

@Data
public class GuestBookingDTO {

    private String collectionAddress; //mandatory
    private String address; // optional (If different from collection address)
    private String name; //mandatory
    private String surname; //mandatory
    private String phoneNo; //mandatory
    private String email;//optional

    private CollectionType collectionType;
    private Long iSubCollectionTypeId;
    private String currencyCode;
    private String vCollectionsRequestLat;
    private String vCollectionsRequestLong;
    private String vCollectionsRequestAddress;
/*    @JsonFormat(shape = JsonFormat.Shape.STRING,pattern = "dd/MM/yyyy HH:mm"    )
    private LocalDateTime requestedDate;*/
    @JsonFormat(shape = JsonFormat.Shape.STRING,pattern = "dd/MM/yyyy HH:mm"    )
    private LocalDateTime collectionDate;
    private String iQuantity;
    @Enumerated(value= EnumType.STRING)
    private VUnit vUnit;
//    @Enumerated(value= EnumType.STRING)
//    private Tonnage tonnage;
    private Recurrence reccurence;
    private WasteType wasteType;
}
