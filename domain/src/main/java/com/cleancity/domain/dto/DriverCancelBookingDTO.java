package com.cleancity.domain.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.time.LocalDateTime;
@Data
public class DriverCancelBookingDTO {
    private String eCancelledBy;
    private Long iCancelledByUserId;
    private Long bookingId;
  //  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy HH:mm")
  //  private LocalDateTime dCanceledDate;
    private String vCancelReason;
}
