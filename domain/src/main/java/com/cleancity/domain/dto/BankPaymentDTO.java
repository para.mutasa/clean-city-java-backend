package com.cleancity.domain.dto;

import com.cleancity.common.enums.PaymentMethod;
import com.cleancity.common.enums.TransactionType;
import lombok.Data;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;

@Data
public class BankPaymentDTO {
    @Enumerated(value= EnumType.STRING)
    private PaymentMethod paymentMethod;
   // private Double amount;
    private Long bookingId;//customerName, phone, jobid
  //  private Long paidBy;
    private String ecocashNumber;
   /* @Enumerated(value= EnumType.STRING)
    private TransactionType transactionType;*/
    private String bankCode;
    private String customerAccount;
    private String currencyCode;




/*
    { //"transactionType":"INTERNAL_FUNDS_TRANSFER",
         //   "bankCode":       "STBLZWHX",
           // "customerAccount": "hoiugjhu9ygjiuh",
          //  "customerName":"test",
         //   "amount":"1",
          //  "currency":"ZWL",
            //"reason":"food",
          //  "msisdn":"772222519",
           // "reference":"guhijygjhbhkn",
          //  "jobId":"uo0001"
    }
*/

     /*  case "RTGS":
               return makeFundsTransfer(allRequestParams);
            case "INTERNAL_FUNDS_TRANSFER":
                    return makeFundsTransfer(allRequestParams);
            case "CHECK_TRANSACTION_STATUS":*/
}
