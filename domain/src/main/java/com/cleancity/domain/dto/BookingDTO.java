package com.cleancity.domain.dto;
import com.cleancity.common.enums.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import java.time.LocalDateTime;
@Data
public class BookingDTO {
    private Long iUserId;
    private CollectionType collectionType;
    private Long iSubCollectionTypeId;
    private String currencyCode;
    private String vCollectionsRequestLat;
    private String vCollectionsRequestLong;
    private String vCollectionsRequestAddress;
/*    @JsonFormat(shape = JsonFormat.Shape.STRING,pattern = "dd/MM/yyyy HH:mm"    )
    private LocalDateTime requestedDate;*/
    @JsonFormat(shape = JsonFormat.Shape.STRING,pattern = "dd/MM/yyyy HH:mm"    )
    private LocalDateTime collectionDate;
    private String iQuantity;
    @Enumerated(value= EnumType.STRING)
    private VUnit vUnit;
//    @Enumerated(value= EnumType.STRING)
//    private Tonnage tonnage;
    private Recurrence reccurence;
    private WasteType wasteType;
}
