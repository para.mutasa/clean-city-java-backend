package com.cleancity.domain.dto;

import lombok.Data;
import org.springframework.web.bind.annotation.PathVariable;

@Data
public class ReAssignDriverDTO {
   private Long bookingId;
   private Long driverId;
}
