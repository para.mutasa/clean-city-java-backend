package com.cleancity.domain.dto;


import lombok.Data;

@Data
public class CurrencyDTO {

    private String name;
    private String code;
    private String symbol;
    private double exchangeRate;
}
