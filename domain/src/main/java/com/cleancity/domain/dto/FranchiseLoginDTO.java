package com.cleancity.domain.dto;

import lombok.Data;

@Data
public class FranchiseLoginDTO {

    private String email;
    private String password;
}
