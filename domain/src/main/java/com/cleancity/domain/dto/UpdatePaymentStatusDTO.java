package com.cleancity.domain.dto;

import com.cleancity.common.enums.PaymentMethod;
import com.cleancity.common.enums.PaymentStatus;
import lombok.Data;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;

@Data
public class UpdatePaymentStatusDTO {

    private Long paymentId;

    @Enumerated(value=EnumType.STRING)
    private PaymentStatus paymentStatus;

}
