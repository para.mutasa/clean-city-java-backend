package com.cleancity.domain.dto;

import com.cleancity.common.enums.PaymentMethod;
import com.cleancity.common.enums.PaymentStatus;
import lombok.Data;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import java.time.LocalDateTime;
@Data
public class PaymentDTO {
    private String currencyCode;
    @Enumerated(value= EnumType.STRING)
    private PaymentMethod paymentMethod;
    private Double amount;
    private Long bookingId;
    private Long paidBy;
    private String ecocashNumber;
}
