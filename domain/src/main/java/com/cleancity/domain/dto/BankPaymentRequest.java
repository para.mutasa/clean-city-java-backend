package com.cleancity.domain.dto;

import com.cleancity.common.enums.TransactionType;
import lombok.Builder;
import lombok.Data;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
@Builder
@Data
public class BankPaymentRequest {
    private String amount;
    private String jobId;
    private String customerName;
    private String msisdn;
    @Enumerated(value= EnumType.STRING)
    private TransactionType transactionType;
    private String bankCode;
    private String customerAccount;
    private String currency;
    private String reason;
    private String reference;

/*
    { //"transactionType":"INTERNAL_FUNDS_TRANSFER",
         //   "bankCode":       "STBLZWHX",
           // "customerAccount": "hoiugjhu9ygjiuh",
          //  "customerName":"test",
         //   "amount":"1",
          //  "currency":"ZWL",
            //"reason":"food",
          //  "msisdn":"772222519",
           // "reference":"guhijygjhbhkn",
          //  "jobId":"uo0001"
    }
*/

     /*  case "RTGS":
               return makeFundsTransfer(allRequestParams);
            case "INTERNAL_FUNDS_TRANSFER":
                    return makeFundsTransfer(allRequestParams);
            case "CHECK_TRANSACTION_STATUS":*/
}
