package com.cleancity.domain.dto;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
public class FranchiseDTO {
    private String company;
    private String address;
    private String lattitude;
    private String longitude;
    private String phoneNumber;
    private String password;
    private String email;
    private String zimraBpNumber;
    private String country;
    private String province;
    private String city;
    private String language;
    private String address2;
    private String state;
    private String code;
}
