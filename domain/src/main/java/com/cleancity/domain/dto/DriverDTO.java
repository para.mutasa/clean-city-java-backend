package com.cleancity.domain.dto;

import lombok.Data;

@Data
public class DriverDTO {
    private String name;
    private String address;
    private String phoneNo;
    private Long franchiseId;
    private String driverLicence;
    private String passportUpload;
    private String rootPermit;
    private String fitnessPermit;
    private String policeClearance;
    private String roadTaxLicence;
    private String password;
    private String goodsInTransit;
    private String taxClearance;
}
