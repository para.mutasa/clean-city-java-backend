package com.cleancity.domain.dto;

import com.cleancity.common.enums.Recurrence;
import com.cleancity.common.enums.WasteType;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class QuotationDTO {

    private Long customerId;
    private WasteType wasteType;
    private Recurrence recurrence;
    private String currency;
   // private validUntil currency;

}
