package com.cleancity.domain.dto;

import lombok.Data;

@Data
public class QueryDTO {
    private String phone;
    private String email;
    private Long customerId;
    private String message;


}
