package com.cleancity.domain.dto;

import lombok.Data;

@Data
public class FranchiseDriverDTO {
    private String type = "franchiseDriverSignup";
    private String vFirstName;
    private String vLastName;
    private Long iFranchiseId;
    private String vEmail;
    private String vPhone;
    private String cAddress;
    private String  cAddress2;
    private String eGender;
    private String PhoneCode;
    private String CountryCode;
    private String vPassword;
  /*  private String name;
    private String address;
    private String phoneNo;
    private Long franchiseId;
    private String driverLicence;
    private String passportUpload;
    private String rootPermit;
    private String fitnessPermit;
    private String policeClearance;
    private String roadTaxLicence;
    private String password;
    private String goodsInTransit;
    private String taxClearance;*/

/*    private String  vFbId;
    private String  vLastName;
    private String  vEmail;
    private String eGender;
    private String iGcmRegId;
    private String vFirebaseDeviceToken;
    private String vLang;
    private String iCompanyId;
    private String vCountry;
    private String eDeviceType;
    private String vRefCode;
    private String vCadress2;
    private String dRefDate;
    private String tRegistrationDate;
    private String eSignUpType;
    private String tDeviceSessionId;
    private String tSessionId;
    private String eEmailVerified;
    private String ePhoneVerified;
    private String eType;
    private String vCode;
    private String vCurrencyDriver;*/
}
