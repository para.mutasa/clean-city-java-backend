package com.cleancity.domain.dto;

import lombok.Data;

@Data
public class ForgotPasswordDTO {
    private String email;
}
