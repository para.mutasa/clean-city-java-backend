package com.cleancity.domain.dto;

import lombok.Data;

import java.util.Date;

@Data
public class UserDTO {
    private String name;
    private String surname;
    private Date DOB;
    private String phoneNumber;
    private String email;
    private String gender;
    private String password;
    private String username;
}
