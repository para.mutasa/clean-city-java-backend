package com.cleancity.domain.dto;

import com.cleancity.common.enums.EIPTransactionStatus;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class EIPResponse {
    private int responseCode;
    private String responseMessage;
    private String paymentReference;
    private String receiptNumber;
    private EIPTransactionStatus transactionStatus;
}
