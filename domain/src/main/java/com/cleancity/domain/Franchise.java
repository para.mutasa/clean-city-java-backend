package com.cleancity.domain;

import com.cleancity.common.enums.Status;
import com.cleancity.domain.embeddables.PersistenceDates;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.time.LocalDate;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "franchise")
public class Franchise {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "franchise_id")
    private Long franchiseId;
    @Column(name = "company")
    private String company;
    @Column(name = "address")
    private String address;
    @Column(name = "lattitude")
    private String lattitude;
    @Column(name = "longitude")
    private String longitude;
    @Column(name = "phone_number")
    private String phoneNumber;
    @JsonIgnore
    @Column(name = "password")
    private String password;
    @Column(name = "email")
    private String email;
    @Column(name = "status")
    @Enumerated(value= EnumType.STRING)
    private Status status;
    @Column(name = "zimra_bp_number")
    private String ZIMRA_BP_Number;
    @Column(name = "country")
    private String country;
    @Column(name = "province")
    private String province;
    @Column(name = "city")
    private String city;
    @Column(name = "language")
    private String language;
    @Column(name = "address2")
    private String address2;
    @Column(name = "state")
    private String state;
    @Column(name = "code")
    private String code;
    @CreationTimestamp
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
    @Column(name = "registration_date",updatable = false)
    private LocalDate registrationDate;
    @Column(name = "rate_perkm")
    private String ratePerKM;

    @Column(name = "login_attempts")
    private int loginAttempts;
    @JsonIgnore
    @Column(name = "reset_token")
    private String ResetToken;
    @Embedded
    @JsonIgnore
    private PersistenceDates persistenceDates;
}
