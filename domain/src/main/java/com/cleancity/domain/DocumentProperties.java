package com.cleancity.domain;

import com.cleancity.domain.embeddables.PersistenceDates;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.usertype.UserType;

import javax.persistence.*;


//@ConfigurationProperties(prefix = "file")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "documents")
public class DocumentProperties {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer documentId;
    private Long userId;
//    //@Enumerated(EnumType.STRING)
//    private String userType;
//    @Column(name = "file_name")
    private String fileName;
    @Column(name = "document_type")
//    private String documentType;
//    @Column(name = "document_format")
    private String documentFormat;
    @Column(name = "upload_dir")
    private String uploadDir;
    @Column(name = "booking_id")
    private Long bookingId;
    @Column(name = "customer_id")
    private Long customerId;
    @Embedded
    private PersistenceDates persistenceDates;

}
