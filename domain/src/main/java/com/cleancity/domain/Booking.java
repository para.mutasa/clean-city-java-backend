package com.cleancity.domain;

import com.cleancity.common.enums.*;
import com.cleancity.common.enums.WasteType;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import com.cleancity.domain.embeddables.PersistenceDates;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "booking")
public class Booking {
    @Id
    @Column(name = "i_booking_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long IBookingId;
    @Column(name = "i_franchise_id")
    private Long iFranchiseId;
    @Column(name = "i_zone_id")
    private Long iZoneId;
    @Column(name = "i_user_id")
    private Long iUserId;
    @Column(name = "i_driver_id")
    private Long iDriverId;
    @Column(name = "i_collection_type")
    @Enumerated(value= EnumType.STRING)
    private CollectionType collectionType;
    @Column(name = "i_sub_collection_type_id")
    private Long iSubCollectionTypeId;
    @Column(name = "currency_code")
    private String currencyCode;
    @Column(name = "v_booking_no")
    private String VBookingNo;

    @Column(name = "v_collections_request_lat", columnDefinition = "varchar(255) default '0000000000000'")
    private String vCollectionsRequestLat;
    @Column(name = "v_collections_request_long", columnDefinition = "varchar(255) default '1111111111111'")
    private String vCollectionsRequestLong;
    @Column(name = "v_collections_request_address")
    private String vCollectionsRequestAddress;

    @Column(name = "v_franchise_address_lat", columnDefinition = "varchar(255) default '0000000000000'")
    private String vFranchiseAddressLat;
    @Column(name = "v_franchise_address_long", columnDefinition = "varchar(255) default '1111111111111'")
    private String vFranchiseAddressLong;
    @Column(name = "v_franchise_address")
    private String vFranchiseAddress;
    @Column(name = "v_collection_point_lat", columnDefinition = "varchar(255) default '0000000000000'")
    private String vCollectionPointLat;
    @Column(name = "v_collection_point_long", columnDefinition = "varchar(255) default '1111111111111'")
    private String vCollectionPointLong;
    @Column(name = "v_collection_point_address")
    private String vCollectionPointAddress;
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy HH:mm:ss")
    @Column(name = "d_collected_date")
    private LocalDateTime dCollectedDate;
    @Column(name = "v_distance",columnDefinition = "float default 0.0")
    private Float vDistance;
    @Column(name = "v_duration")
    private Integer vDuration;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy HH:mm")
    @Column(name = "requested_date")
    private LocalDateTime requestedDate;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy HH:mm")
    @Column(name = "collection_date")
    private LocalDateTime collectionDate;
    @Enumerated(value = EnumType.STRING)
    @Column(name = "e_status")
    private BookingStatus eStatus;
    @Column(name = "e_pay_type")
    private String ePayType;
    @Column(name = "e_cancelled_by")
    private String eCancelledBy;
    @Column(name = "cancellation_approve_by")
    private Long cancellationApproveBy;
    @Column(name = "i_cancelled_by_user_id")
    private Long iCancelledByUserId;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy HH:mm")
    @Column(name = "d_canceled_date")
    private LocalDateTime dCanceledDate;
    @Column(name = "v_cancel_reason")
    private String vCancelReason;
    @Column(name = "e_type")
    private String eType;
    @Column(name = "i_quantity")
    private String iQuantity;

    @Enumerated(value = EnumType.STRING)
    @Column(name = "v_unit")
    private VUnit vUnit;
    @JsonIgnore
    @Column(name = "is_paid")
    private Integer isPaid;
    @Column(name = "f_price")
    private Double fPrice;
    @Column(name = "zwl_price")
    private Double ZWLPrice;
    @Column(name = "v_time_zone")
    private String vTimeZone;
    @Column(name = "v_ride_country")
    private String vRideCountry;
    @Column(name = "v_coupon_code")
    private String vCouponCode;
    @Enumerated(value = EnumType.STRING)
    @Column(name = "payment_status")
    private PaymentStatus paymentStatus;
    @Column(name = "recurrence")
    private Recurrence recurrence;
   @Enumerated(value= EnumType.STRING)
    private WasteType wasteType;
    @Column(name = "proof_of_payment")
    private String proofOfPayment;
 
    @Embedded
    @JsonIgnore
    private PersistenceDates persistenceDates;
}
