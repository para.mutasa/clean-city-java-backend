package com.cleancity.domain.embeddables;

import org.apache.commons.lang3.RandomStringUtils;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class UniqueIdGenerator {
    private UniqueIdGenerator(){
        super();
    }

    public static String generate(Long iUserId) {
        return LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMddHHmmss")).concat("-").concat(String.valueOf(iUserId));
    }

    public static String tokenGenerator(){

        String token = RandomStringUtils.randomAlphanumeric(12);
        return token;
    }
}