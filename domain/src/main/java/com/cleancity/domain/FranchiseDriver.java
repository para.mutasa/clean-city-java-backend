package com.cleancity.domain;

import com.cleancity.common.enums.DriverStatus;
import com.cleancity.domain.embeddables.PersistenceDates;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "driver")
public class  FranchiseDriver {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "driver_id")
    private Long driverId;
    @Column(name = "e_logistics_driver_id")
    private Long eLogisticsDriverId;
    @Column(name = "phone")
    private String phoneNo;
    @Column(name = "email")
    private String email;
    @Column(name = "name")
    private String name;
    @Column(name = "surname")
    private String surname;
    @Enumerated(value = EnumType.STRING)
    @Column(name = "status")
    private DriverStatus status;
    @Column(name = "franchise_id")
    private Long franchiseId;
    @Column(name = "partnerType")
    private String partnerType;

    @Embedded
    @JsonIgnore
    private PersistenceDates persistenceDates;
}
