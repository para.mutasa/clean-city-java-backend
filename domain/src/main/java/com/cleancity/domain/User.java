package com.cleancity.domain;

import com.cleancity.common.enums.Status;
import com.cleancity.domain.embeddables.PersistenceDates;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Date;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "user")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "user_id")
    private Long userId;

    @Column(name = "username")
    private String username;

    @Column(name = "e_logistics_user_id")
    private Long eLogisticsUserId;

    @CreationTimestamp
    @Column(name = "user_joined_date",updatable = false)
    private LocalDateTime userJoinedDate;

    @Embedded
    @JsonIgnore
    private PersistenceDates persistenceDates;

}
