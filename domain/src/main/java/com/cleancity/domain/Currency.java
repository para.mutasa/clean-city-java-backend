package com.cleancity.domain;

import com.cleancity.common.enums.Status;
import com.cleancity.domain.embeddables.PersistenceDates;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;


@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "currency")
public class Currency {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "currency_id")
    private Long currencyId;
    @Column(name = "name",unique=true)
    private String name;
    @Column(name = "code",unique=true)
    private String code;
    @Enumerated(value=EnumType.STRING)
    @Column(name = "status")
    private Status status;
    @Column(name = "symbol")
    private String symbol;
    @Column(name = "exchange_rate")
    private double exchangeRate;

    @Column(name = "is_default", columnDefinition="tinyint(1) default 0")
    private boolean isDefault;
    @Embedded
    @JsonIgnore
    private PersistenceDates persistenceDates;
}
