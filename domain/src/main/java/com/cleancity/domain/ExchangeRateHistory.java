package com.cleancity.domain;

import com.cleancity.domain.embeddables.PersistenceDates;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Table(name = "exchange_rate_history")
public class ExchangeRateHistory {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;
    @Column(name = "currency")
    private String currency;
    @Column(name = "old_rate")
    private double oldRate;
    @Column(name = "new_rate")
    private double newRate;
    @Column(name = "date_changed")
    private LocalDateTime dateChanged;

    @Embedded
    @JsonIgnore
    private PersistenceDates persistenceDates;


}
