package com.cleancity.domain;

import javax.persistence.*;

import com.cleancity.domain.embeddables.PersistenceDates;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


@Entity
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "notification")
public class Notification {
    @Id
    @Column(name = "notification_id")
    private Long notificationId;
    @Column(name = "notification_status")
    private String notificationStatus;
    @Column(name = "phone_number")
    private Integer phoneNumber;
    @Column(name = "message")
    private String message;
    @Embedded
    @JsonIgnore
    private PersistenceDates persistenceDates;


}
