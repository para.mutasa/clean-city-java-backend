package com.cleancity.domain;


import com.cleancity.common.enums.Recurrence;
import com.cleancity.common.enums.WasteType;
import com.cleancity.domain.embeddables.PersistenceDates;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Table(name = "quotation")
public class Quotation {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;
    @Column(name = "amount")
    private Double amount;
    @Column(name = "zwl_amount")
    private Double zwlAmount;
    @Column(name = "customer_id")
    private Long customerId;// to put user as type add a many to one relationship
    @JsonFormat(shape = JsonFormat.Shape.STRING,pattern = "dd/MM/yyyy HH:mm")
    @Column(name = "created_date_time")
    private LocalDateTime createdDateTime;
    @Enumerated(value= EnumType.STRING)
    @Column(name = "waste_type")
    private WasteType wasteType;
    @Column(name = "recurrence")
    private Recurrence recurrence;
    @Column(name = "currency")
    private String currency; //to put Currency as type
    @Column(name = "booking_id")
    private Long bookingId;

    @JsonFormat(shape = JsonFormat.Shape.STRING,pattern = "dd/MM/yyyy HH:mm")
    @Column(name = "valid_until")
    private LocalDateTime validUntil;

    @Embedded
    @JsonIgnore
    private PersistenceDates persistenceDates;







}
