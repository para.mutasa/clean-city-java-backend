package com.cleancity.domain;

import com.cleancity.common.enums.PaymentMethod;
import com.cleancity.common.enums.PaymentStatus;
import com.cleancity.domain.embeddables.PersistenceDates;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@Entity
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "payment")
public class Payment implements Serializable {
    @Id
     @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "payment_id")
   private Long paymentId;
/*    @Column(name = "currency_id")
    private Long currencyId;*/
    @Column(name = "currency_code")
    private String currencyCode;
    @Enumerated(value=EnumType.STRING)
    @Column(name = "payment_method")
    private PaymentMethod paymentMethod; //transactionType
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    @Column(name = "payment_date_time")
    private LocalDateTime paymentDateTime;
    @Column(name = "amount")
    private Double amount;
    @Column(name = "booking_id")
    private Long bookingId;
    @Column(name = "paid_by")
    private Long paidBy;

    @Enumerated(value=EnumType.STRING)
    @Column(name = "payment_status")
    private PaymentStatus paymentStatus;
    @Column(name = "approved_by")
    private String approvedBy;
    @Column(name = "ecocash_number")
    private String ecocashNumber;

    @Column(name = "msisdn")
    private String msisdn;

    @Column(name = "bank_code")
    private String bankCode;

    @Column(name = "customer_account")
    private String customerAccount;
    @Column(name = "customer_name")
    private String customerName;//customerName
    @Column(name = "reference")
    private String reference;
    @Column(name = "proof_of_payment")
    private String proofOfPayment;
    @Embedded
    @JsonIgnore
    private PersistenceDates persistenceDates;
    /*
    { "transactionType":"INTERNAL_FUNDS_TRANSFER",
         "bankCode":       "STBLZWHX",
          "customerAccount": "hoiugjhu9ygjiuh",
          "customerName":"test",
            "amount":"1",
            "currency":"ZWL",
            "reason":"food",
             "msisdn":"772222519",
           "reference":"guhijygjhbhkn",
          "jobId":"uo0001"
    }
*/

}
