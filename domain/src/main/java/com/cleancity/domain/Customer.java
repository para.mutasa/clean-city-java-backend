package com.cleancity.domain;


import com.cleancity.common.enums.Status;
import com.cleancity.domain.embeddables.PersistenceDates;
import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import javax.persistence.*;
import java.time.LocalDateTime;


@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "customer")
public class Customer {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "user_id")
    @JsonIgnore
    private Long userId;

    @Column(name = "e_logistics_user_id",  unique = true)
    private Long eLogisticsUserId;

    @Column(name = "name")
    private String name;

    @Column(name = "surname")
    private String surname;

    @Column(name = "phone")
    private String phoneNo;

    @Column(name = "email")
    private String email;

    @Column(name = "partner_type")
    private String partnerType;

    @Column(name = "company_name")
    private String companyName;

    @Column(name = "address")
    private String address;

    @Column(name = "zimra_bp_number")

    private String zimraBPNumber;

    @Enumerated(value = EnumType.STRING)
    @Column(name = "status")
    private Status status;

    @CreationTimestamp
    @Column(name = "user_joined_date",updatable = false)
    private LocalDateTime userJoinedDate;

    @Column(name="national_id")
    private String nationId;

    @Column(name="proof_of_residence")
    private String proofOfResidence;

    @Column(name="bank_statement")//for credit
    private String bankStatement;

    @Column(name="trade_license") //optional
    private String tradeLicense;



    @Embedded
    @JsonIgnore
    private PersistenceDates persistenceDates;
}
