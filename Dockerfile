# Our Java base image
FROM openjdk:11-jre-slim-buster
#FROM openjdk:8-jre-alpine
# Set server timezone
RUN echo "Africa/Harare" > /etc/timezone

# Run the jar
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","-Dspring.config.location=${SPRING_APP_PROPS_LOCATION}", "-Dspring.profiles.active=${SPRING_PROFILES_ACTIVE}", "/opt/cassava/clean-city-backend.jar"]