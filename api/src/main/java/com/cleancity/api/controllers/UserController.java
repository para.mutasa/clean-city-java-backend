package com.cleancity.api.controllers;


import com.cleancity.common.GeneralResponse;
import com.cleancity.domain.dto.LoginDTO;
import com.cleancity.service.api.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/users",
        consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
public class UserController {

    @Autowired
    IUserService iUserService;

    @PostMapping("/login")
    public GeneralResponse login(@RequestBody LoginDTO loginDTO) {
        return iUserService.login(loginDTO.getUsername(), loginDTO.getPassword());
    }
    @GetMapping("/find-all-elogistics-drivers")
    public GeneralResponse getAllElogisticsDrivers() {
        return iUserService.getAllElogisticsDrivers();
    }
    @GetMapping("/find-all-elogistics-users")
    public GeneralResponse getAllElogisticsUsers() {
        return iUserService.getAllElogisticsDrivers();
    }

}
