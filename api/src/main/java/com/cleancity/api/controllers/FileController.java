package com.cleancity.api.controllers;

import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

@RestController
@RequestMapping(value = "/download")
public class FileController {

    //root path for image files
   @Value("${file.upload-dir.local}")
    private String FILE_PATH_ROOT;

    @GetMapping("/{filename}")
    public ResponseEntity<byte[]> getImage(@PathVariable("filename") String filename) {
        byte[] image = new byte[0];
        try {
            image = FileUtils.readFileToByteArray(new File(FILE_PATH_ROOT + "/" + filename));
        } catch (IOException e) {
            e.printStackTrace();
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().contentType(MediaType.IMAGE_PNG).body(image);
    }

/*    @GetMapping("docs/{filename}")
    public ResponseEntity<byte[]> getDocs(@PathVariable("filename") String filename) {
        byte[] docs = new byte[0];
        try {
            docs = FileUtils.readFileToByteArray(new File(FILE_PATH_ROOT + "/" + filename));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return ResponseEntity.ok().contentType(MediaType.ALL).body(docs);
    }*/

}