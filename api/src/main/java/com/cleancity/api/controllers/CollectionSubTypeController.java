package com.cleancity.api.controllers;


import com.cleancity.common.enums.CollectionType;
import com.cleancity.common.enums.Status;
import com.cleancity.domain.embeddables.PersistenceDates;
import com.cleancity.service.api.ICollectionSubTypeService;
import com.cleancity.common.GeneralResponse;
import com.cleancity.domain.CollectionSubType;
import com.cleancity.domain.dto.CollectionSubTypeDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/collectionSubtype",
        consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)

public class CollectionSubTypeController {

    @Autowired
    private ICollectionSubTypeService iCollectionSubTypeService;


    @PostMapping("/create-collection-sub-type")
    public GeneralResponse createCollectionSubType(@RequestBody CollectionSubTypeDTO collectionSubTypeDTO) {
        CollectionSubType collectionSubType = CollectionSubType.builder()
                .vSubType(collectionSubTypeDTO.getVSubType())
                .vShow(collectionSubTypeDTO.getVShow())
                .costPerUnit(collectionSubTypeDTO.getCostPerUnit())
                .ratePerKm(collectionSubTypeDTO.getRatePerKm())
                .iMaxQty(collectionSubTypeDTO.getIMaxQty())
                .vUnit(collectionSubTypeDTO.getVUnit())
                .collectionType(collectionSubTypeDTO.getCollectionType())
                .persistenceDates(new PersistenceDates())
                .status(Status.ACTIVE)
                .quotationValidityDays(collectionSubTypeDTO.getQuotationValidityDays())
                .build();

        return iCollectionSubTypeService.createCollectionSubType(collectionSubType);
    }


    @PostMapping("/update-collection-sub-type")
    public GeneralResponse updateCollectionSubType(@RequestBody CollectionSubType collectionSubType) {
        return iCollectionSubTypeService.updateCollectionSubType(collectionSubType);
    }

    @GetMapping("/get-collection-sub-type-by-id/{collectionSubTypeId}")
    public GeneralResponse getCollectionSubTypeById(@PathVariable Long collectionSubTypeId) {
        return iCollectionSubTypeService.getCollectionSubTypeById(collectionSubTypeId);
    }

    @GetMapping("/get-all-collection-sub-types")
    public GeneralResponse getAllCollectionSubType() {
        return iCollectionSubTypeService.getAllCollectionSubType();
    }

    @GetMapping("/get-all-collectionsubtype-By-CollectionType/{collectionType}")
    public GeneralResponse getAllCollectionSubTypeByTypeId(@PathVariable CollectionType collectionType) {
        return iCollectionSubTypeService.getAllCollectionSubTypeByCollectionType(collectionType);
    }


}
