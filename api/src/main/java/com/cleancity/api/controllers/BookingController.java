package com.cleancity.api.controllers;


import com.cleancity.common.GeneralResponse;
import com.cleancity.common.enums.BookingStatus;
import com.cleancity.common.enums.PaymentStatus;
import com.cleancity.common.enums.WasteType;
import com.cleancity.domain.Booking;
import com.cleancity.domain.dto.AdminApproveCancelDTO;
import com.cleancity.domain.dto.BookingDTO;
import com.cleancity.domain.dto.ReAssignDriverDTO;
import com.cleancity.domain.embeddables.PersistenceDates;
import com.cleancity.service.api.IBookingService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/bookings",
        consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
public class BookingController {

    @Autowired
    IBookingService iBookingService;

    @PostMapping("/create-booking")
    public GeneralResponse createBooking(@RequestBody BookingDTO bookingDto) {
        Booking booking = Booking.builder()
                .vUnit(bookingDto.getVUnit())
                .requestedDate(LocalDateTime.now())
                .iQuantity(bookingDto.getIQuantity())
                .vCollectionsRequestAddress(bookingDto.getVCollectionsRequestAddress())
                .vCollectionsRequestLong(bookingDto.getVCollectionsRequestLong())
                .vCollectionsRequestLat(bookingDto.getVCollectionsRequestLat())
                .iSubCollectionTypeId(bookingDto.getISubCollectionTypeId())
                .collectionType(bookingDto.getCollectionType())
                .iUserId(bookingDto.getIUserId())
                .currencyCode(bookingDto.getCurrencyCode())
                .collectionDate(bookingDto.getCollectionDate())
                .eStatus(BookingStatus.PENDING)
                .recurrence(bookingDto.getReccurence())
                .persistenceDates(new PersistenceDates())
                .wasteType(bookingDto.getWasteType())
                .paymentStatus(PaymentStatus.UNPAID)
                //.proofOfPayment();
                .build();
        return iBookingService.createBooking(booking);
    }

    @GetMapping("/find-by-waste-type/{wasteType}")
    public GeneralResponse getBookingByWasteType(@PathVariable WasteType wasteType) {
        return iBookingService.getBookingsByWasteType(wasteType);
    }

    @GetMapping("/find-by-payment-status/{paymentStatus}")
    public GeneralResponse getBookingsByPaymentsStatus(@PathVariable PaymentStatus paymentStatus) {
        return iBookingService.getBookingsByPaymentsStatus(PaymentStatus.PAID);
    }

    @GetMapping("/find-by-payment-status-and-franchise-id/{iFranchiseId}/{paymentStatus}")
    public GeneralResponse getBookingsByPaymentsStatusAndFranchiseId(@PathVariable Long iFranchiseId, @PathVariable PaymentStatus paymentStatus) {
        return iBookingService.getBookingsByIFranchiseIdAndPaymentStatus(iFranchiseId, PaymentStatus.PAID);
    }

    @PostMapping("/update-booking")
    public GeneralResponse updateBooking(@RequestBody Booking booking) {
        return iBookingService.updateBooking(booking);
    }

    //todo:: update names
    @GetMapping("/clean-city-confirm-request/{bookingId}")
    public GeneralResponse cleanCityConfirmRequest(@PathVariable Long bookingId) {
        return iBookingService.cleanCityConfirmRequest(bookingId);
    }

    @GetMapping("/find-by-booking-number/{bookingNumber}")
    public GeneralResponse findBookingByBookingNumber(@PathVariable String bookingNumber) {
        return iBookingService.getBookingByBookingNumber(bookingNumber);
    }

    @GetMapping("/find-by-booking-id/{bookingId}")
    public GeneralResponse findBookingByBookingId(@PathVariable Long bookingId) {
        return iBookingService.getBookingByBookingId(bookingId);
    }

    @GetMapping("/find-by-booking-franchise-id/{iFranchiseId}")
    public GeneralResponse findBookingsByiFranchiseId(@PathVariable Long iFranchiseId) {
        return iBookingService.getBookingsByiFranchiseId(iFranchiseId);
    }


    @GetMapping("/find-all-bookings")
    public GeneralResponse findAllBookings() {
        return iBookingService.getAllBookings();
    }

    @GetMapping("/find-all-bookings-by-driver-id/{driverId}")
    public GeneralResponse findAllBookingsByDriverId(@PathVariable Long driverId) {
        return iBookingService.getAllBookingsByDriverId(driverId);
    }

    /*   @GetMapping("/reassign-driver/{bookingId}/{driverId}")
       public GeneralResponse reassignDriver(@RequestBody ReAssignDriverDTO reAssignDriverDTO ) {
           return iBookingService.reAssignDriver(reAssignDriverDTO.getBookingId(), reAssignDriverDTO.getDriverId());
       }
   */
    @GetMapping("/reassign-driver/{bookingId}/{driverId}")
    public GeneralResponse reassignDriver(@PathVariable Long bookingId, @PathVariable Long driverId) {
        return iBookingService.reAssignDriver(bookingId, driverId);
    }

    @GetMapping("/history-by-driver-id/{driverId}")
    public GeneralResponse historyByDriverId(@PathVariable Long driverId) {
        return iBookingService.historyByDriverId(driverId);
    }

    @PostMapping("/cancel-booking/{bookingId}")
    public GeneralResponse cancelBooking(@PathVariable Long bookingId) {
        return iBookingService.cancelBooking(bookingId);
    }

    @GetMapping("/accept-booking/{bookingId}/{iDriverId}/{iFranchiseId}")
    public GeneralResponse acceptBooking(@PathVariable Long bookingId, @PathVariable Long iDriverId, @PathVariable Long iFranchiseId) {
        //  Status change
        //  assign to another driver
        return iBookingService.acceptBooking(bookingId, iDriverId, iFranchiseId);
    }

    @PostMapping("/reassign-franchise/{bookingId}/{iFranchiseId}")
    public GeneralResponse reassignFranchise(@PathVariable Long bookingId, @PathVariable Long iFranchiseId) {
        //  Status change
        //  assign to another driver
        return iBookingService.reAssignFranchise(bookingId, iFranchiseId);
    }

    @PostMapping("/update-collection")
    public GeneralResponse updateCollection(@RequestBody Booking booking) {
        return iBookingService.updateCollection(booking);
    }

    @PostMapping("/start-trip/{IBookingId}")
    public GeneralResponse startTrip(@PathVariable Long IBookingId) {
        return iBookingService.startTrip(IBookingId);
    }

    @PostMapping("/end-trip/{IBookingId}")
    public GeneralResponse endTrip(@PathVariable Long IBookingId) {
        return iBookingService.endTrip(IBookingId);
    }


    @GetMapping("/get-bookings-by-user-id/{iUserId}")
    public GeneralResponse getBookingsByIUserId(@PathVariable Long iUserId) {
        return iBookingService.getBookingsByiUserId(iUserId);
    }

    @GetMapping("/get-bookings-by-status/{eStatus}")
    public GeneralResponse getBookingsByeStatus(@PathVariable BookingStatus eStatus) {
        return iBookingService.getBookingsByeStatus(eStatus);
    }

    @PostMapping("/admin-approve-cancel")
    public GeneralResponse adminApproveCancel(@RequestBody AdminApproveCancelDTO approveCancelDTO) {
        return iBookingService.approveCancel(approveCancelDTO);
    }


}
