package com.cleancity.api.controllers;

import com.cleancity.domain.embeddables.PersistenceDates;
import com.cleancity.service.api.IVehicleService;
import com.cleancity.common.GeneralResponse;
import com.cleancity.domain.Vehicle;
import com.cleancity.domain.dto.VehicleDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/vehicles",
        consumes = MediaType.APPLICATION_JSON_VALUE,produces = MediaType.APPLICATION_JSON_VALUE)

public class VehicleController {

    @Autowired
    IVehicleService iVehicleService;

    @PostMapping("/create-vehicle")
    public GeneralResponse createVehicle(@RequestBody VehicleDTO vehicleDto){

        Vehicle vehicle = Vehicle.builder()
                .model(vehicleDto.getModel())
                .licensePlateNumber(vehicleDto.getLicensePlateNumber())
                .capacity(vehicleDto.getCapacity())
                .franchiseId(vehicleDto.getFranchiseId())
                .vehicleStatus(vehicleDto.getVehicleStatus())
                .make(vehicleDto.getMake())
                .year(vehicleDto.getYear())
                .persistenceDates(new PersistenceDates())
                .build();

        return iVehicleService.createVehicle(vehicle);
    }

    @PostMapping("/update-vehicle")
    public GeneralResponse updateVehicle(@RequestBody Vehicle vehicle){
        return iVehicleService.updateVehicle(vehicle);
    }

    @GetMapping("/find-by-vehicleId/{vehicleId}")
    public GeneralResponse findVehicleByVehicleId(@PathVariable Long vehicleId){

        return iVehicleService.getVehicleByVehicleId(vehicleId);
    }

    @GetMapping("/find-by-Licence-Plate-Number/{licensePlateNumber}")
    public GeneralResponse findVehicleByLicencePlateNumber(@PathVariable Long licensePlateNumber){

        return iVehicleService.getVehicleByLicensePlateNumber(licensePlateNumber);
    }

    @GetMapping("/find-by-model/{model}")
    public GeneralResponse findVehiclesByModel(@PathVariable String  model){

        return iVehicleService.getVehiclesByModel(model);
    }

    @GetMapping("/find-by-franchise-id/{franchiseId}")
    public GeneralResponse findVehiclesByFranchiseId(@PathVariable Long franchiseId ){

        return iVehicleService.getVehiclesByFranchiseId(franchiseId);
    }

    @GetMapping("/find-by-vehicle-status/{vehicleStatus}")
    public GeneralResponse findVehiclesByVehicleStatus(@PathVariable String  vehicleStatus){

        return iVehicleService.getVehiclesByVehicleStatus(vehicleStatus);
    }

    @GetMapping("/find-vehicles")
    public GeneralResponse findVehicles(){

        return iVehicleService.getAllVehicles();
    }

}
