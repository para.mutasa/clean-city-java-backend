package com.cleancity.api.controllers;

import com.cleancity.common.GeneralResponse;
import com.cleancity.service.api.IDocumentStorageService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;


@RequiredArgsConstructor
@RestController
@RequestMapping(value = "/documentStorage",
        consumes = MediaType.APPLICATION_JSON_VALUE,produces = MediaType.APPLICATION_JSON_VALUE)
public class DocumentStorageController {

    @Autowired
    IDocumentStorageService documentStorageService;

    @PostMapping(value ="/uploadFile", consumes={"multipart/form-data"})
    public GeneralResponse uploadFile(@RequestParam("file") MultipartFile file, @RequestParam("reference") String reference,@RequestParam("bookingId") Long bookingId)
                                      {
        return documentStorageService.storeProofOfPayment(file, reference,bookingId);
    }

    @GetMapping(value ="/downloadFile",consumes = MediaType.APPLICATION_JSON_VALUE, produces ={"multipart/form-data"})
    public ResponseEntity<Resource> downloadFile(@RequestParam("userId") Long userId,
                                                 @RequestParam("bookingId") Long bookingId,
                                                 HttpServletRequest request){
        return documentStorageService.loadFileAsResource(userId,bookingId, request);
    }

}

