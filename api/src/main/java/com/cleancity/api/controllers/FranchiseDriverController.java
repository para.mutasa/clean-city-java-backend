package com.cleancity.api.controllers;

import com.cleancity.common.GeneralResponse;
import com.cleancity.common.enums.DriverStatus;
import com.cleancity.domain.FranchiseDriver;
import com.cleancity.domain.dto.DriverCancelBookingDTO;
import com.cleancity.service.api.IFranchiseDriverService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
@Slf4j
@RestController
@RequestMapping(value = "/franchise-driver",
        consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
public class FranchiseDriverController {

    @Autowired
    IFranchiseDriverService iDriverService;

    @PostMapping("/update-franchise-driver")
    public GeneralResponse updateDriver(@RequestBody FranchiseDriver driver) {
        return iDriverService.updateFranchiseDriver(driver);
    }

    @GetMapping("/find-franchise-driver-by-id/{driverId}")
    public GeneralResponse findDriverById(@PathVariable Long driverId) {
        return iDriverService.getFranchiseDriverById(driverId);
    }
    @GetMapping("/approve-franchise-driver/{driverId}")
    public GeneralResponse approveDriver(@PathVariable Long driverId) {
        return iDriverService.approveDriver(driverId);
    }

    @GetMapping("/find-franchise-driver-by-elogistics-id/{eLogisticsDriverId}")
    public GeneralResponse findDriverByElogisticsId(@PathVariable Long eLogisticsDriverId) {
        return iDriverService.getFranchiseDriverByElogisticsId(eLogisticsDriverId);
    }

    @GetMapping("/find-driver-by-franchise-id/{franchiseId}")
    public GeneralResponse findDriversByFranchiseId(@PathVariable Long franchiseId) {
        return iDriverService.getFranchiseDriversByFranchiseId(franchiseId);
    }

    @PostMapping("/cancel-booking")
    public GeneralResponse cancelBooking(@RequestBody DriverCancelBookingDTO cancelBookingDTO) {
         return iDriverService.cancelBooking(cancelBookingDTO);
    }

    @GetMapping("/find-franchise-driver-by-status/{status}")
    public GeneralResponse findDriversByStatus(@PathVariable DriverStatus status) {
        return iDriverService.getFranchiseDriversByStatus(status);
    }

    @GetMapping("/find-all-franchise-drivers")
    public GeneralResponse findAllFranchiseDrivers() {
        return iDriverService.getAllFranchiseDrivers();
    }

    @GetMapping("/find-franchise-driver-documents/{eLogisticsId}")
    public GeneralResponse findFranchiseDriverDocuments(@PathVariable Long eLogisticsId) {
        return iDriverService.getFranchiseDriverDocuments(eLogisticsId);
    }

    @GetMapping("/assign-franchise/{franchiseId}/{eLogisticsDriverId}")
    public GeneralResponse assignFranchise(@PathVariable Long franchiseId, @PathVariable Long eLogisticsDriverId) {
        //todo: only assign if documents are uploaded
        return iDriverService.assignFranchise(franchiseId, eLogisticsDriverId);
    }

}
