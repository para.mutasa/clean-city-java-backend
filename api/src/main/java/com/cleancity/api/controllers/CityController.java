package com.cleancity.api.controllers;

import com.cleancity.common.enums.Status;
import com.cleancity.domain.embeddables.PersistenceDates;
import com.cleancity.service.api.ICityService;
import com.cleancity.common.GeneralResponse;
import com.cleancity.domain.City;
import com.cleancity.domain.dto.CityDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/cities", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)

public class CityController {

    @Autowired
    ICityService iCityService;

    @PostMapping("/create-city")
    public GeneralResponse createCity(@RequestBody CityDTO cityDto) {
        City city = City.builder()
                .cityName(cityDto.getCityName())
                .country(cityDto.getCountry())
                .countryCode(cityDto.getCountryCode())
                .province(cityDto.getProvince())
                .persistenceDates(new PersistenceDates())
                .status(Status.ACTIVE)
                .build();

        return iCityService.createCity(city);
    }

    @PostMapping("/update-city")
    public GeneralResponse updateCity(@RequestBody City city) {

        return iCityService.updateCity(city);
    }

    @GetMapping("/find-by-id/{id}")
    public GeneralResponse findCityById(@PathVariable Long id) {

        return iCityService.getCityById(id);

    }

    @GetMapping("/find-by-city-name/{cityName}")
    public GeneralResponse findCityByCityName(@PathVariable String cityName) {

        return iCityService.getCityByCityName(cityName);

    }

    @GetMapping("/find-by-country/{country}")
    public GeneralResponse findCitiesByCountry(@PathVariable String country) {

        return iCityService.getCitiesByCountry(country);

    }

    @GetMapping("/find-by-country-code/{countryCode}")
    public GeneralResponse findCitiesByCountryCode(@PathVariable String countryCode) {

        return iCityService.getCitiesByCountryCode(countryCode);

    }

    @GetMapping("/find-by-province/{province}")
    public GeneralResponse findCitiesByProvince(@PathVariable String province) {

        return iCityService.getCitiesByProvince(province);

    }

    @GetMapping("/find-cities")
    public GeneralResponse findCities() {

        return iCityService.getAllCities();

    }
}
