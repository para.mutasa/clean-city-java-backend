package com.cleancity.api.controllers;


import com.cleancity.common.GeneralResponse;
import com.cleancity.common.GeneralResponseBuilder;
import com.cleancity.common.GenerateShortUUID;
import com.cleancity.common.enums.PaymentMethod;
import com.cleancity.common.enums.PaymentStatus;
import com.cleancity.domain.Payment;
import com.cleancity.domain.dto.BankPaymentDTO;
import com.cleancity.domain.dto.EcocashRequestCallbackDTO;
import com.cleancity.domain.dto.PaymentDTO;
import com.cleancity.domain.dto.UpdatePaymentStatusDTO;
import com.cleancity.domain.embeddables.PersistenceDates;
import com.cleancity.service.api.IDocumentStorageService;
import com.cleancity.service.api.IPaymentService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.time.LocalDateTime;

@Slf4j
@RestController
@RequestMapping(value = "/payment",
        consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
public class PaymentController {

    @Autowired
    IPaymentService iPaymentService;

    @Autowired
    IDocumentStorageService documentStorageService;

    @PostMapping("/create-payment")
    public GeneralResponse createPayment(@RequestBody PaymentDTO paymentDTO) {
        Payment payment = buildPayment(paymentDTO);

        return iPaymentService.createPayment(payment);
    }

   @PostMapping("/make-bank-payment")
    public GeneralResponse makeBankPayment(@RequestBody BankPaymentDTO paymentDTO) {

        return iPaymentService.makeBankPayment(paymentDTO);
    }

    @GetMapping("/find-all-banks")
    public GeneralResponse getAllBanks() {
        return iPaymentService.getAllBanks();
    }


    @PostMapping("/make-ecocash-payment")
    public GeneralResponse makeEcocashPayment(@RequestBody PaymentDTO paymentDTO) {
        Payment payment = buildPayment(paymentDTO);
        return iPaymentService.makeEcocashPayment(payment);
    }


    @PostMapping("/update-payment-status")
    public GeneralResponse updatePaymentStatus(@RequestBody UpdatePaymentStatusDTO paymentStatusDTO) {
        return iPaymentService.updatePaymentStatus(paymentStatusDTO);
    }

    @PostMapping("/ecocash-payment-callback")
    public GeneralResponse ecocashPaymentCallback(@RequestBody EcocashRequestCallbackDTO requestCallback) {
        log.info("\n\n****************** ECOCASH RESPONSE ******************\n {} \n\n ", requestCallback);
        iPaymentService.processEcocashPaymentCallback(requestCallback);
        return GeneralResponseBuilder.buildSuccess(null, "Update received");
    }


    @PostMapping("/update-payment")
    public GeneralResponse updatePayment(@RequestBody Payment payment) {

        return iPaymentService.updatePayment(payment);

    }

    @GetMapping("/find-by-payment-id/{paymentId}")
    public GeneralResponse findPaymentByPaymentId(@PathVariable Long paymentId) {

        return iPaymentService.getPaymentByPaymentId(paymentId);
    }

    @GetMapping("/find-payments-by-currency-code/{currencyCode}")
    public GeneralResponse findPaymentsByCurrencyCode(@PathVariable String currencyCode) {
        return iPaymentService.getPaymentsByCurrencyCode(currencyCode);
    }

    @GetMapping("/find-payments-by-payment-method-id/{paymentMethod}")
    public GeneralResponse findPaymentsByPaymentMethod(@PathVariable PaymentMethod paymentMethod) {

        return iPaymentService.getPaymentsByPaymentMethod(paymentMethod);
    }

    @GetMapping("/find-payment-by-booking-id/{bookingId}")
    public GeneralResponse findPaymentByBookingId(@PathVariable Long bookingId) {

        return iPaymentService.getPaymentByBookingId(bookingId);
    }

    @GetMapping("/find-payment-by-status/{paymentStatus}")
    public GeneralResponse findPaymentsByPaymentStatus(@PathVariable PaymentStatus paymentStatus) {

        return iPaymentService.getPaymentsByPaymentStatus(paymentStatus);
    }

    @GetMapping("/find-payment-by-paid-by/{paidBy}")
    public GeneralResponse findPaymentsByPaidBy(@PathVariable String paidBy) {

        return iPaymentService.getPaymentsByPaidBy(paidBy);
    }

    @GetMapping("/find-payment-by-approved-by/{approvedBy}")
    public GeneralResponse findPaymentByApprovedBy(@PathVariable String approvedBy) {

        return iPaymentService.getPaymentByApprovedBy(approvedBy);
    }


    @GetMapping("/find-all-payments")
    public GeneralResponse findAllAllPayments() {

        return iPaymentService.getAllPayments();
    }


    @PostMapping(value ="/upload-proof-of-payment", consumes={"multipart/form-data"})
    public GeneralResponse uploadProofOfPayment(@RequestParam("file") MultipartFile file, @RequestParam("reference") String reference, @RequestParam("bookingId") Long bookingId)
    {
        return documentStorageService.storeProofOfPayment(file, reference,bookingId);
    }

    private Payment buildPayment(PaymentDTO paymentDTO) {
        final String reference = GenerateShortUUID.next();
        return Payment.builder()
                .currencyCode(paymentDTO.getCurrencyCode())
                .paymentMethod(paymentDTO.getPaymentMethod())
                .paymentDateTime(LocalDateTime.now())
                .amount(paymentDTO.getAmount())
                .bookingId(paymentDTO.getBookingId())
                .paidBy(paymentDTO.getPaidBy())
                .persistenceDates(new PersistenceDates())
                .reference(reference)
                .ecocashNumber(paymentDTO.getEcocashNumber())
                .paymentStatus(PaymentStatus.UNPAID)
                .build();
    }


}
