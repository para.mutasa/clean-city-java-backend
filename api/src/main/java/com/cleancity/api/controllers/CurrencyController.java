package com.cleancity.api.controllers;

import com.cleancity.common.GeneralResponse;
import com.cleancity.common.enums.Status;
import com.cleancity.domain.Currency;
import com.cleancity.domain.dto.CurrencyDTO;
import com.cleancity.domain.embeddables.PersistenceDates;
import com.cleancity.service.api.ICurrencyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@RequestMapping(value = "/currency",
        consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
@RestController
public class CurrencyController {

    @Autowired
    private ICurrencyService iCurrencyService;

    @PostMapping("/create-currency")
    public GeneralResponse createCurrency(@RequestBody CurrencyDTO currencyDTO) {
        Currency currency = Currency.builder()
                .name(currencyDTO.getName())
                .code(currencyDTO.getCode())
                .symbol(currencyDTO.getSymbol())
                .exchangeRate(currencyDTO.getExchangeRate())
                .status(Status.ACTIVE)
                .persistenceDates(new PersistenceDates())
                .build();

        return iCurrencyService.createCurrency(currency);
    }

    @PostMapping("/update")
    public GeneralResponse updateCurrency(@RequestBody Currency currency) {

        return iCurrencyService.updateCurrency(currency);
    }

    @GetMapping("/get-currency-by-id/{currencyId}")
    public GeneralResponse getCurrencyById(@PathVariable Long currencyId) {

        return iCurrencyService.getCurrencyById(currencyId);

    }

    @GetMapping("/get-currency-by-code/{currencyCode}")
    public GeneralResponse getCurrencyById(@PathVariable String currencyCode) {

        return iCurrencyService.getCurrencyByCode(currencyCode);

    }

    @GetMapping("/get-currency-by-status/{status}")
    public GeneralResponse getCurrencyByStatus(@PathVariable Status status) {

        return iCurrencyService.getCurrencyByStatus(status);

    }

    @GetMapping("/get-all-currency")
    public GeneralResponse getAllCurrency() {

        return iCurrencyService.getAllCurrency();
    }

}
