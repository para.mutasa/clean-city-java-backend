package com.cleancity.api.controllers;

import com.cleancity.domain.dto.ForgotPasswordDTO;
import com.cleancity.domain.dto.ResetPasswordDTO;
import com.cleancity.common.enums.Status;
import com.cleancity.domain.dto.ForgotPasswordDTO;
import com.cleancity.domain.dto.FranchiseLoginDTO;
import com.cleancity.domain.dto.ResetPasswordDTO;
import com.cleancity.service.api.PasswordEncryptionService;
import com.cleancity.domain.embeddables.PersistenceDates;
import com.cleancity.service.api.IFranchiseService;
import com.cleancity.common.GeneralResponse;
import com.cleancity.domain.Franchise;
import com.cleancity.domain.dto.FranchiseDTO;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RequiredArgsConstructor
@RestController
@RequestMapping(value = "/franchise",
        consumes = MediaType.APPLICATION_JSON_VALUE,produces = MediaType.APPLICATION_JSON_VALUE)

public class FranchiseController {

    @Autowired
    private IFranchiseService iFranchiseService;
    @Autowired
    private  PasswordEncryptionService passwordEncryptionService;

    @PostMapping("/create-franchise")
    public GeneralResponse createFranchise(@RequestBody FranchiseDTO franchiseDTO){
        Franchise franchiseBuild= Franchise.builder()
                .company(franchiseDTO.getCompany())
                .address(franchiseDTO.getAddress())
                .lattitude(franchiseDTO.getLattitude())
                .longitude(franchiseDTO.getLongitude())
                .phoneNumber(franchiseDTO.getPhoneNumber())
                .email(franchiseDTO.getEmail())
                .password(passwordEncryptionService.encrypt(franchiseDTO.getPassword()))
                .ZIMRA_BP_Number(franchiseDTO.getZimraBpNumber())
                .country(franchiseDTO.getCountry())
                .province(franchiseDTO.getProvince())
                .city(franchiseDTO.getCity())
                .language(franchiseDTO.getLanguage())
                .address2(franchiseDTO.getAddress2())
                .state(franchiseDTO.getState())
                .code(franchiseDTO.getCode())
                .loginAttempts(0)
                .persistenceDates(new PersistenceDates())
                .status(Status.INACTIVE)
                .build();
        return iFranchiseService.createFranchise(franchiseBuild);
    }

    @PostMapping ("/update-franchise")
    public GeneralResponse updateFranchise(@RequestBody Franchise franchise){
        return iFranchiseService.updateFranchise(franchise);
    }

    @GetMapping("/get-Franchise-By-Id/{franchiseId}")
    public GeneralResponse getFranchiseById(@PathVariable Long franchiseId){
        return iFranchiseService.getFranchiseById(franchiseId);
    }

    @GetMapping("/get-Franchise-By-Email/{email}")
    public GeneralResponse getFranchiseByEmail(@PathVariable String email){
        return iFranchiseService.getFranchiseByEmail(email);
    }

    @GetMapping("/get-Franchise-Vehicles-By-Id/{franchiseId}")
    public GeneralResponse getFranchiseVehiclesById(@PathVariable Long franchiseId){
        return iFranchiseService.getFranchiseById(franchiseId);
    }

    @GetMapping("/get-All-Franchises")
    public GeneralResponse getAllFranchises(){
        return iFranchiseService.getAllFranchises();
    }

    @PostMapping("/franchise-login")
    public GeneralResponse getFranchiseByUsernameAndPassword(@RequestBody FranchiseLoginDTO franchiseLoginDTO){
        return iFranchiseService.getFranchiseByUsernameAndPassword(franchiseLoginDTO.getEmail(), franchiseLoginDTO.getPassword());
    }

    @PostMapping("/franchise-forgot-password")
    public GeneralResponse forgotPassword(@RequestBody ForgotPasswordDTO passwordResetDTO){
        log.info("EMAIL",""+passwordResetDTO.getEmail());
        return iFranchiseService.forgotPassword(passwordResetDTO.getEmail());
    }

    @PostMapping("/franchise-reset-password")
    public GeneralResponse resetPassword(@RequestBody ResetPasswordDTO resetPasswordDTO){
        log.info("EMAIL {}",""+resetPasswordDTO.getEmail());
        log.info("password {}",""+resetPasswordDTO.getNewpassword());
        log.info("password {}",""+resetPasswordDTO.getResetToken());
        return iFranchiseService.resetPassword(resetPasswordDTO.getEmail(),resetPasswordDTO.getResetToken(),resetPasswordDTO.getNewpassword());
    }




}
