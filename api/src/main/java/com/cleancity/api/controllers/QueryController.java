package com.cleancity.api.controllers;


import com.cleancity.common.GeneralResponse;
import com.cleancity.common.enums.QueryStatus;
import com.cleancity.domain.Query;
import com.cleancity.domain.dto.QueryDTO;
import com.cleancity.domain.embeddables.PersistenceDates;
import com.cleancity.service.api.IQueryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/query",
        consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
public class QueryController {

    @Autowired
    IQueryService iQueryService;


    @PostMapping("/create-query")
    public GeneralResponse createQuery(@RequestBody QueryDTO queryDTO) {

        Query query = Query.builder()
                .customerId(queryDTO.getCustomerId())
                .phone(queryDTO.getPhone())
                .email(queryDTO.getEmail())
                .message(queryDTO.getMessage())
                .persistenceDates(new PersistenceDates())
                .status(QueryStatus.PENDING)
                .build();
        return iQueryService.createQuery(query);
    }

    @PostMapping("/update-query")
    public GeneralResponse updateQuery(@RequestBody Query query) {

        return iQueryService.updateQuery(query);

    }

    @GetMapping("/find-by-query-id/{Id}")
    public GeneralResponse findQueryById(@PathVariable Long Id) {

        return iQueryService.getQueryById(Id);
    }

    @GetMapping("/find-query-by-status/{queryStatus}")
    public GeneralResponse findQueriesByStatus(@PathVariable String queryStatus) {

        return iQueryService.getQueriesByStatus(queryStatus);
    }

    @GetMapping("/find-query-by-phone/{phone}")
    public GeneralResponse findQueriesByPhone(@PathVariable String phone) {

        return iQueryService.getQueriesByPhone(phone);
    }

    @GetMapping("/find-query-by-email/{email}")
    public GeneralResponse findQueriesByEmail(@PathVariable String email) {

        return iQueryService.getQueriesByEmail(email);
    }

    @GetMapping("/find-query-by-customer-id/{customerId}")
    public GeneralResponse findQueriesByCustomerId(@PathVariable String customerId) {

        return iQueryService.getQueriesByCustomerId(customerId);
    }

    @GetMapping("/find-all-queries")
    public GeneralResponse findAllAllQueries() {

        return iQueryService.getAllQueries();
    }
}
