package com.cleancity.api.controllers;


import com.cleancity.common.GeneralResponse;
import com.cleancity.common.enums.Status;
import com.cleancity.domain.Customer;
import com.cleancity.service.api.ICustomerService;
import com.cleancity.service.api.IDocumentStorageService;
import com.cleancity.service.api.IELogisticsDbLookUpService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.Column;
import java.util.HashMap;

@RequiredArgsConstructor
@RestController
@RequestMapping(value = "/customer",
        consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
@Slf4j
public class CustomerController {

    @Autowired
    private IELogisticsDbLookUpService dbLookUpService;

    @Autowired
    private ICustomerService customerService;

    @Autowired
    IDocumentStorageService documentStorageService;

/*

    @GetMapping("/find-by-id/{userId}")
    public GeneralResponse findById(@PathVariable Long userId) {
        return dbLookUpService.getCustomerByUserId(userId);
    }
*/

    @GetMapping("/find-by-elogistics-id/{elogisticsId}")
    public GeneralResponse findByElogisticsId(@PathVariable Long elogisticsId) {
        return customerService.getCustomerByElogisticsId(elogisticsId);
    }

    @GetMapping("/find-all")
    public GeneralResponse findAll() {
        dbLookUpService.importAllCustomersFromElogistics();
        return customerService.getAllCustomers();
    }

    @GetMapping("/find-all-elogistics")
    public GeneralResponse findAllElogistics() {
        return customerService.getAllElogisticsCustomers();
    }

    //
    @PostMapping("/update")
    public GeneralResponse update(@RequestBody Customer customer) {
        return customerService.updateCustomer(customer);
    }


    @PostMapping("/update-email")
    public GeneralResponse updateCustomerEmail(@RequestBody HashMap<String, Object> params) {
        return customerService.updateCustomerEmail(params);
    }

    @Column(name="bank_statement")//for credit
    private String bankStatement;
    @Column(name="trade_license") //optional
    private String tradeLicense;
    @PostMapping(value ="/upload-customer-documents", consumes={"multipart/form-data"})
    public GeneralResponse uploadFile(@RequestParam("nationalId") MultipartFile nationalId,
                                      @RequestParam("proofOfResidence") MultipartFile proofOfResidence,
                                      @RequestParam(value ="bankStatement", required=false) MultipartFile bankStatement,
                                      @RequestParam(value ="tradeLicense", required=false) MultipartFile tradeLicense,
                                      @RequestParam("userId") Long userId)
    {

      log.info(" upload-customer-documents  HIT ---  ")  ;
      log.info(" upload-customer-documents  HIT ---  {}",userId);
        return documentStorageService.storeCustomerDocuments(nationalId, proofOfResidence,bankStatement,tradeLicense,userId);
    }

    @GetMapping("/approve-customer/{eLogisticsUserId}/{status}")
    public GeneralResponse approveCustomer(@PathVariable Long eLogisticsUserId, @PathVariable Status status) {
        return customerService.approveCustomer(eLogisticsUserId,status);
    }

}
