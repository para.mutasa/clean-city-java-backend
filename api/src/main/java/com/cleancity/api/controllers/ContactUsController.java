package com.cleancity.api.controllers;


import com.cleancity.common.GeneralResponse;
import com.cleancity.domain.ContactUs;
import com.cleancity.domain.dto.ContactUsDTO;
import com.cleancity.domain.embeddables.PersistenceDates;
import com.cleancity.service.api.IContactUsService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@RestController
@AllArgsConstructor
@RequestMapping(value = "/contactus",
        consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
public class ContactUsController {

    @Autowired
    IContactUsService iContactUsService;


    @PostMapping("/create-contact-us")
    public GeneralResponse createContactUS(@RequestBody ContactUsDTO contactUsDTO) {

        ContactUs contactUs = ContactUs.builder()
                .phone(contactUsDTO.getPhone())
                .fullname(contactUsDTO.getFullname())
                .email(contactUsDTO.getEmail())
                .message(contactUsDTO.getMessage())
                .persistenceDates(new PersistenceDates())
                .build();
        return iContactUsService.createContactUS(contactUs);
    }

    @GetMapping("/find-by-contact-us-id/{Id}")
    public GeneralResponse getContactUsById(@PathVariable Long Id) {

        return iContactUsService.getContactUsById(Id);
    }


    @GetMapping("/find-contact-us-by-phone/{phone}")
    public GeneralResponse getContactUsByPhone(@PathVariable String phone) {

        return iContactUsService.getContactUsByPhone(phone);
    }

    @GetMapping("/find-contact-us-by-email/{email}")
    public GeneralResponse getContactUSByByEmail(@PathVariable String email) {

        return iContactUsService.getContactUSByByEmail(email);
    }


    @GetMapping("/find-all-contact-us")
    public GeneralResponse getAllContactUs() {

        return iContactUsService.getAllContactUs();
    }
}
