package com.cleancity.api.controllers;

import com.cleancity.common.GeneralResponse;
import com.cleancity.domain.Notification;
import com.cleancity.domain.dto.NotificationDTO;
import com.cleancity.domain.embeddables.PersistenceDates;
import com.cleancity.service.api.INotificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping(value = "/notification",
        consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
public class NotificationController {

    @Autowired
    INotificationService iNotificationService;

    @PostMapping("/create-notification")
    public GeneralResponse createNotification(@RequestBody NotificationDTO notificationDTO) {
        Notification notification = Notification.builder()
                .message(notificationDTO.getMessage())
                .phoneNumber(notificationDTO.getPhoneNumber())
                .persistenceDates(new PersistenceDates())
                .build();
        return iNotificationService.createNotification(notification);
    }


    @PostMapping("/update-notification/{}")
    public GeneralResponse updateNotification(Notification notification) {
        return iNotificationService.updateNotification(notification);
    }

    @GetMapping("/get-notification-by-id/{notificationId}")
    public GeneralResponse getNotificationById(Long notificationId) {
        return iNotificationService.getNotificationById(notificationId);
    }

    @GetMapping("/get-all-notifications")
    public GeneralResponse getAllNotifications() {
        return iNotificationService.getAllNotifications();
    }

    @GetMapping("/get-notification-by-status/{notificationStatus}")
    public GeneralResponse getAllNotificationsByStatus(String notificationStatus) {
        return iNotificationService.getAllNotificationsByStatus(notificationStatus);
    }

}
