package com.cleancity.api.controllers;

import com.cleancity.common.GeneralResponse;
import com.cleancity.domain.Quotation;
import com.cleancity.domain.dto.QuotationDTO;
import com.cleancity.domain.embeddables.PersistenceDates;
import com.cleancity.service.api.IQuotationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/quotations",
        consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)


public class QuotationController {

    @Autowired
    IQuotationService iQuotationService;

    @PostMapping("/create-quotation")
    public GeneralResponse createQuotation(@RequestBody QuotationDTO quotationDto) {

        Quotation quotation = Quotation.builder()
                .customerId(quotationDto.getCustomerId())
                .wasteType(quotationDto.getWasteType())
                .recurrence(quotationDto.getRecurrence())
                .currency(quotationDto.getCurrency())
                .persistenceDates(new PersistenceDates())
                .build();

        return iQuotationService.createQuotation(quotation);
    }

    @PostMapping("/update-quotation")
    public GeneralResponse updateQuotation(@RequestBody Quotation qoutation) {

        return iQuotationService.updateQuotation(qoutation);

    }

    @GetMapping("/find-by-id/{id}")
    public GeneralResponse findQuotationById(@PathVariable Long id) {

        return iQuotationService.getQuotationById(id);
    }

    @GetMapping("/find-by-booking-id/{bookingId}")
    public GeneralResponse findQuotationsByBookingId(@PathVariable Long bookingId) {
        return iQuotationService.getQuotationsByBookingId(bookingId);
    }

    @GetMapping("/find-all-quotations")
    public GeneralResponse findAllQuotations() {

        return iQuotationService.getAllQuotations();
    }

}