package com.cleancity.api.configurations.elogistics;

import com.zaxxer.hikari.HikariDataSource;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(
        entityManagerFactoryRef = "vayaElogisticsEntityManagerFactory",
        transactionManagerRef = "vayaElogisticsTransactionManager",
        basePackages = {
                "com.cleancity.persistence.elogistics.repository"
        }
)
public class VayaConfig {

    @Bean(name = "vayaElogisticsDataSource")
    @ConfigurationProperties(prefix = "db2.datasource")
    public HikariDataSource vayaDataSource() {
        return DataSourceBuilder.create().type(HikariDataSource.class).build();
    }

    @Bean(name = "vayaElogisticsEntityManagerFactory")
    public LocalContainerEntityManagerFactoryBean
    barEntityManagerFactory(
            EntityManagerFactoryBuilder builder,
            @Qualifier("vayaElogisticsDataSource") DataSource dataSource
    ) {
        return
                builder
                        .dataSource(dataSource)
                        .packages("com.cleancity.persistence.elogistics.model")
                        .persistenceUnit("vaya_bidding_platform_db")
                        .build();
    }

    @Bean(name = "vayaElogisticsTransactionManager")
    public PlatformTransactionManager vayaTransactionManager(
            @Qualifier("vayaElogisticsEntityManagerFactory") EntityManagerFactory vayaEntityManagerFactory
    ) {
        return new JpaTransactionManager(vayaEntityManagerFactory);
    }

    @Bean
    NamedParameterJdbcTemplate namedParameterJdbcTemplateVaya() {
        return new NamedParameterJdbcTemplate(vayaDataSource());
    }

    @Bean
    JdbcTemplate jdbcTemplateVaya() {
        return new JdbcTemplate(vayaDataSource());
    }


}
