package com.cleancity.api.configurations;


import com.cleancity.domain.EntityMarker;
import com.cleancity.persistence.RepositoryMarker;
import com.cleancity.service.ServiceMarker;
import com.zaxxer.hikari.HikariDataSource;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.*;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;
import org.springframework.web.servlet.handler.MappedInterceptor;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;


@Configuration
@EntityScan(basePackageClasses = EntityMarker.class,
        basePackages = {"com.cleancity.domain"}
)
@EnableJpaRepositories(
        basePackageClasses = RepositoryMarker.class)
@ComponentScan(
        basePackageClasses = {
                EntityMarker.class,
                RepositoryMarker.class,
                ServiceMarker.class
        })
@EnableTransactionManagement
@PropertySource("classpath:messages.properties")
public class ServiceConfiguration {


    @Bean
    public MessageSource messageSource() {
        ReloadableResourceBundleMessageSource messageSource
                = new ReloadableResourceBundleMessageSource();

        messageSource.setBasename("classpath:messages");
        messageSource.setDefaultEncoding("UTF-8");
        return messageSource;
    }

    @Primary
    @Bean(name = "ccDataSource")
    @ConfigurationProperties(prefix = "spring.datasource")
    public HikariDataSource ussdDataSource() {
        return DataSourceBuilder.create().type(HikariDataSource.class).build();
    }

    @Primary
    @Bean(name = "entityManagerFactory")
    public LocalContainerEntityManagerFactoryBean
    entityManagerFactory(
            EntityManagerFactoryBuilder builder,
            @Qualifier("ccDataSource") DataSource dataSource
    ) {
        Map<String, Object> properties = new HashMap<String, Object>();
        properties.put("hibernate.hbm2ddl.auto", "update");
        return builder
                .dataSource(dataSource)
                .properties(properties)
                .packages("com.cleancity.domain")
                .persistenceUnit("clean_city_waste_db")
                .build();
    }


    @Primary
    @Bean(name = "transactionManager")
    public PlatformTransactionManager ussdTransactionManager(
            @Qualifier("entityManagerFactory") EntityManagerFactory ussdEntityManagerFactory
    ) {
        return new JpaTransactionManager(ussdEntityManagerFactory);
    }


}
