package com.cleancity.api;

import com.cleancity.api.configurations.ServiceConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

@SpringBootApplication
@Import({ServiceConfiguration.class})
public class CleanCityBackendApplication {

    public static void main(String[] args) {
        SpringApplication.run(CleanCityBackendApplication.class, args);
    }

}
