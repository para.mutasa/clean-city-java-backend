package com.cleancity.api.interceptors;

import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.UUID;

@Slf4j
public class ApiInterceptor extends HandlerInterceptorAdapter {
    Logger logger = LoggerFactory.getLogger(ApiInterceptor.class);

    private static final String X_REQUEST_START = "X-Request-Start";
    private static final String X_TRANSACTION_UUID = "X-Request-UUID";

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
            throws Exception {
        long startTime = System.currentTimeMillis();
        String uuid = UUID.randomUUID().toString();
        request.setAttribute(X_REQUEST_START, startTime);
        request.setAttribute(X_TRANSACTION_UUID, uuid);
        return super.preHandle(request, response, handler);
    }

    @Override
    public void postHandle(
            HttpServletRequest request,
            HttpServletResponse response,
            Object handler,
            ModelAndView modelAndView)
            throws Exception {
        super.postHandle(request, response, handler, modelAndView);
        long startTime = (Long) request.getAttribute(X_REQUEST_START);
        String uuid = (String) request.getAttribute(X_TRANSACTION_UUID);
        long endTime = System.currentTimeMillis();
        long executionTime = endTime - startTime;
        logger.info(
                "transaction time uuid: {} startTime: {} endTime: {} executionTime: {}",
                uuid,
                startTime,
                endTime,
                executionTime);
    }
}
