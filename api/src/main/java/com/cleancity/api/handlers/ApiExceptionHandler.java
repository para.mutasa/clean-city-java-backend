package com.cleancity.api.handlers;//package com.cleancity.com.cleancity.service.api.handlers;
//
//import com.cassavasmartech.vaya.vayaridermanager.commons.exceptions.ExceptionResponse;
//import com.cassavasmartech.vaya.vayaridermanager.commons.exceptions.InvalidRequestException;
//import com.fasterxml.jackson.databind.ObjectMapper;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.http.HttpStatus;
//import org.springframework.http.MediaType;
//import org.springframework.http.ResponseEntity;
//import org.springframework.web.bind.annotation.ControllerAdvice;
//import org.springframework.web.bind.annotation.ExceptionHandler;
//import org.springframework.web.context.request.ServletWebRequest;
//import org.springframework.web.context.request.WebRequest;
//import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import javax.validation.ConstraintViolation;
//import javax.validation.ConstraintViolationException;
//import javax.validation.Valid;
//import javax.validation.constraints.NotNull;
//import java.io.IOException;
//import java.io.PrintWriter;
//import java.nio.charset.StandardCharsets;
//import java.util.Objects;
//
//@ControllerAdvice
//@Slf4j
//public class ApiExceptionHandler extends ResponseEntityExceptionHandler {
//
//    private static final ObjectMapper mapper = new ObjectMapper();
//
//    private static final String NO_REQUEST_URI_AVAILABLE = "uri unavailable";
//    private static final String NO_MESSAGE_AVAILABLE = "no message ";
//
//    private static String getRequestURI(WebRequest request) {
//        if (Objects.isNull(request)) return NO_REQUEST_URI_AVAILABLE;
//        ServletWebRequest servletWebRequest = (ServletWebRequest) request;
//        HttpServletRequest httpServletRequest = servletWebRequest.getRequest();
//        return httpServletRequest.getRequestURI();
//    }
//
//    public static void sendExceptionResponse(
//            HttpServletRequest httpServletRequest,
//            HttpServletResponse httpServletResponse,
//            @NotNull Exception ex,
//            @NotNull HttpStatus status)
//            throws IOException {
//
//        if (Objects.nonNull(httpServletResponse)) {
//            String requestURI =
//                    Objects.nonNull(httpServletRequest)
//                            ? httpServletRequest.getRequestURI()
//                            : NO_REQUEST_URI_AVAILABLE;
//
//            String exMessage;
//
//            if (Objects.nonNull(ex)) {
//                exMessage = ex.getMessage();
//            } else {
//                exMessage = NO_MESSAGE_AVAILABLE;
//            }
//
//            ExceptionResponse exceptionResponse =
//                    ExceptionResponse.builder()
//                            .error(status.getReasonPhrase())
//                            .status(status.value())
//                            .message(exMessage)
//                            .path(requestURI)
//                            .timestamp(System.currentTimeMillis())
//                            .build();
//            String jsonObject = mapper.writeValueAsString(exceptionResponse);
//            httpServletResponse.setContentType(MediaType.APPLICATION_JSON_VALUE);
//            httpServletResponse.setCharacterEncoding(StandardCharsets.UTF_8.name());
//            httpServletResponse.setStatus(status.value());
//            PrintWriter out = httpServletResponse.getWriter();
//            out.print(jsonObject);
//            out.flush();
//            out.close();
//        }
//    }
//
//    @ExceptionHandler(
//            value = {
//                    IllegalArgumentException.class,
//                    InvalidRequestException.class,
//                    ConstraintViolationException.class
//            })
//    @Valid
//    protected ResponseEntity<?> handleApiException(RuntimeException ex, WebRequest request) {
//        logger.error("Validation error: {}", ex);
//        HttpStatus status = HttpStatus.BAD_REQUEST;
//        String requestURI = getRequestURI(request);
//
//        String exceptionMessage = ex.getMessage();
//
//        if (ex instanceof ConstraintViolationException) {
//            ConstraintViolationException constraintViolationException = (ConstraintViolationException) ex;
//            exceptionMessage =
//                    constraintViolationException.getConstraintViolations().stream()
//                            .findFirst()
//                            .map(ConstraintViolation::getMessage)
//                            .orElse(null);
//        }
//
//        ExceptionResponse exceptionResponse =
//                ExceptionResponse.builder()
//                        .error(status.getReasonPhrase())
//                        .status(status.value())
//                        .message(exceptionMessage)
//                        .path(requestURI)
//                        .timestamp(System.currentTimeMillis())
//                        .build();
//        return handleExceptionInternal(ex, exceptionResponse, null, HttpStatus.BAD_REQUEST, request);
//    }
//}
