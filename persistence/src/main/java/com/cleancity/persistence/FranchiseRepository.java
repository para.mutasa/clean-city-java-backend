package com.cleancity.persistence;


import com.cleancity.domain.Franchise;
import com.cleancity.domain.Vehicle;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface FranchiseRepository extends JpaRepository<Franchise, Number> {


    List<Vehicle> getFranchiseVehiclesByFranchiseId(Long franchiseId);


    Optional<Franchise> getFranchiseByEmail(String email);

    @Query("select c from Franchise  c where c.status <> 'DELETED'")
    List<Franchise> findAll();

    @Query("select c from Franchise  c where c.status <> 'DELETED' and c.franchiseId = :id")
    Optional<Franchise> findById(Long id);






}

