package com.cleancity.persistence;

import com.cleancity.domain.City;
import com.cleancity.domain.Currency;
import com.cleancity.domain.ExchangeRateHistory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ExchangeRateHistoryRepository extends JpaRepository<ExchangeRateHistory, Long> {
    List<Currency> findAllByCurrency(String currencyName);

}


