package com.cleancity.persistence;


import com.cleancity.domain.DocumentProperties;
import org.hibernate.usertype.UserType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;
import java.util.Optional;

public interface DocumentPropertiesRepository extends JpaRepository<DocumentProperties,Integer>,
        JpaSpecificationExecutor<DocumentProperties> {
    Optional<DocumentProperties> findByUserIdAndBookingId(Long iUserId,Long bookingId );
    List<DocumentProperties> getAllByUserIdAndBookingId(Long driverId, String bookingId);
    Optional<DocumentProperties> findByCustomerId(Long customerId);
 }
