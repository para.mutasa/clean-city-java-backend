package com.cleancity.persistence;


import com.cleancity.common.enums.Status;
import com.cleancity.domain.Currency;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;


@Repository
public interface CurrencyRepository extends JpaRepository<Currency, Number> {



    List<Currency> findAllByStatus(Status status);
 //   Optional<Currency> findCurrencyByName(String name);
    Optional<Currency> findCurrencyByCode(String code);
    // Optional<Currency> findCurrencyByDefault(boolean isDefault);

    @Query("select c from Currency  c where c.status <> 'DELETED'")
    List<Currency> findAll();
    @Query("select c from Currency  c where c.status <> 'DELETED' and c.currencyId=:id")
    Optional<Currency> findById(Long id);
}



