package com.cleancity.persistence;


import com.cleancity.domain.Quotation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;


@Repository
public interface QuotationRepository extends JpaRepository<Quotation, Long> {
List<Quotation> findQuotationsByBookingId(Long bookingId);
}



