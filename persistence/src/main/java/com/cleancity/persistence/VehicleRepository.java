package com.cleancity.persistence;

import com.cleancity.domain.Vehicle;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;


@Repository
public interface VehicleRepository extends JpaRepository<Vehicle, Long> {
   // Optional<Vehicle> findAllVehicles(Query query);
   @Query("select c from Vehicle  c where c.vehicleStatus <> 'DELETED' and c.id=:vehicleId")
   Optional<Vehicle> findVehicleByVehicleId(Long vehicleId);
    List<Vehicle> findVehiclesByModel(String model);
    Optional<Vehicle> findVehicleByLicensePlateNumber(Long licensePlateNumber);
    List<Vehicle> findVehiclesByFranchiseId(Long franchiseId);
    List<Vehicle> findVehiclesByVehicleStatus(String vehicleStatus);
    @Query("select c from Vehicle  c where c.vehicleStatus <> 'DELETED'")
    List<Vehicle> findAll();

    @Query("select c from Vehicle  c where c.vehicleStatus <> 'DELETED' and c.id=:id")
    Optional<Vehicle> findById(Long id);
}
