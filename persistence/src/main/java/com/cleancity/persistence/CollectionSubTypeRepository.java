package com.cleancity.persistence;

import com.cleancity.common.enums.CollectionType;
import com.cleancity.domain.CollectionSubType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface CollectionSubTypeRepository extends JpaRepository<CollectionSubType, Long> {

    Optional<CollectionSubType> findCollectionSubTypeById(Long typeId);
    Optional<CollectionSubType> findCollectionSubTypeByCollectionType(CollectionType collectionType);
   // List<CollectionSubType> fi(String typeId);
   List<CollectionSubType> findCollectionSubTypesByCollectionType(CollectionType collectionType);

    @Query("select c from CollectionSubType  c where c.status <> 'DELETED'")
    List<CollectionSubType> findAll();

    @Query("select c from CollectionSubType  c where c.status <> 'DELETED' and c.id=:id")
    Optional<CollectionSubType> findById(Long id);


}
