package com.cleancity.persistence;

import com.cleancity.common.enums.PaymentMethod;
import com.cleancity.common.enums.PaymentStatus;
import com.cleancity.domain.Payment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface PaymentRepository extends JpaRepository<Payment, Long > {

    Optional<Payment> findPaymentByPaymentId(Long paymentId);
    List<Payment> findPaymentByCurrencyCode(String code);
    // Optional<Payment> findAllPayments(Payment Payment);
    List<Payment> findPaymentsByPaymentMethod(PaymentMethod paymentMethod);
    Optional<Payment> findPaymentByBookingId(Long bookingId);
    List<Payment> findPaymentsByPaymentStatus( PaymentStatus paymentStatus);
    List<Payment> findPaymentsByPaidBy( String paidBy);
    List<Payment> findPaymentByApprovedBy( String approvedBy);
   /* List<Payment> findPaymentByRefe( String approvedBy);*/
    Optional<Payment> findPaymentByReference(String reference);
}