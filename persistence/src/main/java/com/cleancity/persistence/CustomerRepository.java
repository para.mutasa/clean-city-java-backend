package com.cleancity.persistence;

import com.cleancity.domain.Customer;
import com.cleancity.domain.User;
import org.jetbrains.annotations.NotNull;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;


@Repository
public interface CustomerRepository extends JpaRepository<Customer, Long> {
    Optional<Customer> findCustomerByeLogisticsUserId(Long elogisticsId);
    Optional<Customer> findCustomerByUserId(Long userId);
    Optional<Customer> findCustomerByEmail(String email);
    @NotNull
    @Query("select c from Customer  c where c.status <> 'DELETED'")
    List<Customer> findAll();
}
