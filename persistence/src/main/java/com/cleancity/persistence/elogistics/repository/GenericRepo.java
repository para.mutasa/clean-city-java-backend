package com.cleancity.persistence.elogistics.repository;

import java.util.List;
import java.util.Map;

public interface GenericRepo {
    List<Map<String, Object>> getGenericList(Map<String, Object> params, String query);

    int genericInsert(Map<String, Object> params, String query);

    Map<String, Object> getGenericMap(Map<String, String> params, String query);

    List<Map<String, Object>> getGenericMapList(String query);
}
