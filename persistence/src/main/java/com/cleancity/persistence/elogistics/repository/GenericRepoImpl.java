package com.cleancity.persistence.elogistics.repository;


import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
@Slf4j
public class GenericRepoImpl implements GenericRepo {

    @Autowired
    @Qualifier("namedParameterJdbcTemplateVaya")
    private NamedParameterJdbcTemplate namedJdbcTemplate;

    @Autowired
    @Qualifier("jdbcTemplateVaya")
    private JdbcTemplate jdbcTemplate;

    @Override
    public List<Map<String, Object>> getGenericList(Map<String, Object> params, String query) {
        SqlParameterSource namedParameters = new MapSqlParameterSource()
                .addValues(params);
        return namedJdbcTemplate.queryForList(query, namedParameters);
    }

    @Override
    public int genericInsert(Map<String, Object> params, String query) {
        SqlParameterSource namedParameters = new MapSqlParameterSource()
                .addValues(params);
        return namedJdbcTemplate.update(query, namedParameters);
    }

    @Override
    public Map<String, Object> getGenericMap(Map<String, String> params, String query) {
        SqlParameterSource namedParameters = new MapSqlParameterSource()
                .addValues(params);
        return namedJdbcTemplate.queryForMap(query, namedParameters);
    }

    @Override
    public List<Map<String, Object>> getGenericMapList( String query) {
        return jdbcTemplate.queryForList(query);
    }



}
