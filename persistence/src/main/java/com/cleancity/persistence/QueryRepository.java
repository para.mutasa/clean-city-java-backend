package com.cleancity.persistence;

import com.cleancity.domain.Query;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
@Repository
public interface QueryRepository extends JpaRepository<Query, Long> {

    List<Query> findQueriesByCustomerId(String customerId);
    List<Query> findQueriesByEmail(String email);
    Optional<Query> findQueryById(Long id);
    List<Query> findQueriesByPhone(String phone);
    List<Query> findQueriesByStatus(String queryStatus);
}
