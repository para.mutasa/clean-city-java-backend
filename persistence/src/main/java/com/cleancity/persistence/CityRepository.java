package com.cleancity.persistence;

import com.cleancity.domain.City;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface CityRepository extends JpaRepository<City, Long> {


    @Query("select c from City  c where c.status <> 'DELETED' and c.id=:id")
    Optional<City> findCityById(Long id);
    Optional<City> findCityByCityName(String cityName);
    List<City> findCitiesByCountry(String country);
    List<City> findCitiesByCountryCode(String countryCode);
    List<City> findCitiesByProvince(String province);
    @Query("select c from City  c where c.status <> 'DELETED'")
    List<City> findAll();
}


