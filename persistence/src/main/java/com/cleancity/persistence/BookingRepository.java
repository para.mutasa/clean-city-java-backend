package com.cleancity.persistence;

import com.cleancity.common.enums.BookingStatus;
import com.cleancity.common.enums.PaymentStatus;
import com.cleancity.common.enums.WasteType;
import com.cleancity.domain.Booking;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface BookingRepository extends JpaRepository<Booking, Long> {
   Optional<Booking> findBookingByIBookingId(Long bookingId);
    Optional<Booking> findBookingByVBookingNo(String bookingNumber);
    List<Booking> findBookingsByiFranchiseId(Long iFranchiseId);
    List<Booking> findBookingsByiUserId(Long iUserId);
    List<Booking> findBookingsByiDriverId(Long driverId);
    List<Booking> findBookingsByeStatus(BookingStatus eStatus);
    List<Booking> findBookingsByPaymentStatus(PaymentStatus paymentStatus);
    List<Booking> findBookingsByiFranchiseIdAndPaymentStatus(Long iFranchiseId , PaymentStatus paymentStatus);
    List<Booking> findBookingsByWasteType(WasteType wasteType);
    Object queryBy();


}
