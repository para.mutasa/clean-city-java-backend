package com.cleancity.persistence;

import com.cleancity.domain.ContactUs;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
@Repository
public interface ContactUsRepository extends JpaRepository<ContactUs, Long> {

    List<ContactUs> findContactUsByEmail(String email);

    List<ContactUs> findContactUsByPhone(String phone);

}
