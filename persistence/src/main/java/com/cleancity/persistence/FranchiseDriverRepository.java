package com.cleancity.persistence;

import com.cleancity.common.enums.DriverStatus;
import com.cleancity.domain.FranchiseDriver;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;


@Repository
public interface FranchiseDriverRepository extends JpaRepository<FranchiseDriver,Long> {

    Optional<FranchiseDriver> findFranchiseDriverByeLogisticsDriverId(Long eLogisticsDriverId);
    List<FranchiseDriver> getAllFranchiseDriversByStatus(DriverStatus status);
    List<FranchiseDriver> getAllFranchiseDriverByFranchiseId(Long franchiseId);


}
