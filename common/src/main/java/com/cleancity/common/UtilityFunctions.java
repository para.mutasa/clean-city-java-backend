package com.cleancity.common;


import java.math.RoundingMode;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Locale;
import java.util.TimeZone;

public class UtilityFunctions {
    public static Double roundToTwoDecimals(Double value) {
        DecimalFormat decimalFormat = new DecimalFormat("#.##");
        decimalFormat.setRoundingMode(RoundingMode.CEILING);
        return Double.valueOf(decimalFormat.format(value));
    }
    public static String convertToTimeZone(Timestamp time, String fromTimeZone, String toTimeZone){
        String output = null;
        DateFormat dateFormatFrom = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ROOT);
        dateFormatFrom.setTimeZone(TimeZone.getTimeZone(fromTimeZone));
        DateFormat dateFormatTo = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ROOT);
        dateFormatTo.setTimeZone(TimeZone.getTimeZone(toTimeZone));
        try {
            return dateFormatTo.format(dateFormatFrom.parse(time.toString()));
        } catch (ParseException e) {
            return output;
        }
    }

    public static String generateReference() {
        final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyMMddHHmmssSSS");
        return "CCWM"+formatter.format(LocalDateTime.now()).toString();
    }

/*
    public static String formatMobileNumber(String mobileNumber) {
        final String invalidMobileNumberMessage = "Invalid Mobile Number Supplied";
        if(mobileNumber == null) {
            throw new InvalidRequestException(invalidMobileNumberMessage);
        }
        String newMobileNumber = removeAllSpaces( mobileNumber);

        try {
            if (newMobileNumber.length() > 9) {
                newMobileNumber = newMobileNumber.substring(newMobileNumber.length() - 9);
            }
            if (isNumeric(newMobileNumber)) {
                if (newMobileNumber.startsWith("77") || newMobileNumber.startsWith("78")) {
                    return newMobileNumber;
                } else if(newMobileNumber.startsWith("73") || newMobileNumber.startsWith("71")){
                    return newMobileNumber;
                }
                else {
                    return "772222533";
                }
            } else {
                throw new InvalidRequestException(invalidMobileNumberMessage);
            }
        } catch (Exception ex) {
            throw new InvalidRequestException(ex.getMessage());
        }
    }
*/

    private static String removeAllSpaces(final String msisdn) {
        return msisdn.replaceAll(" ", "");
    }

    private static boolean isNumeric(final String str) {
        return str.matches("-?\\d+(\\.\\d+)?");
    }

}
