package com.cleancity.common;
import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class GeneralResponse {
    private Enum responseStatus;
    private String responseMessage;
    private Enum responseCode;
    private Object eobjResponse;

}
