package com.cleancity.common;


import com.cleancity.common.enums.ResponseCode;
import com.cleancity.common.enums.ResponseStatus;
import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class GeneralResponseBuilder {


    public static GeneralResponse buildSuccess(Object obj,String message){

        return GeneralResponse.builder()
                .responseStatus(ResponseStatus.SUCCESS) //enum
                .responseMessage(message) //put in properties file
                .responseCode(ResponseCode.RESPONSE_SUCCESS_CODE) //enum
                .eobjResponse(obj)
                .build();

    }

    public static GeneralResponse buildFailed(Object obj, String message){

        return GeneralResponse.builder()
                .responseStatus(ResponseStatus.FAILED) //enum
                .responseMessage(message) //put in properties file
                .responseCode(ResponseCode.RESPONSE_FAILED_CODE) //enum
                .eobjResponse(obj)
                .build();

    }

}
