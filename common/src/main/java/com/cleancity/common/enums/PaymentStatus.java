package com.cleancity.common.enums;

import com.fasterxml.jackson.annotation.JsonCreator;

public enum PaymentStatus {


    PAID,UNPAID;

    public String getStatus() {
        return this.name();
    }
    @JsonCreator
    public static PaymentStatus getPaymentFromCode(String value) {

        for (PaymentStatus paymentStatus : PaymentStatus.values()) {

            if (paymentStatus.getStatus().equals(value)) {

                return paymentStatus;
            }
        }

        return null;
    }

}
