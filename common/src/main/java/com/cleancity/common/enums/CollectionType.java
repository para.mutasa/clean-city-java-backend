package com.cleancity.common.enums;

public enum CollectionType {

    HouseHoldCollection, CommercialCollection, HouseHoldAdHocCollection , CommercialAdhocCollection
}
