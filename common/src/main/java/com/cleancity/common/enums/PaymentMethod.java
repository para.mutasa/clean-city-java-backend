package com.cleancity.common.enums;

public enum PaymentMethod {
    ECOCASH, CASH, VISA_MASTERCARD, RTGS,
    INTERNAL_FUNDS_TRANSFER, POP
  //  ECOCASH, CASH, VISA_MASTERCARD, NOSTRO, EFT, POP
}
