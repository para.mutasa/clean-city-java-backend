package com.cleancity.common.enums;

import java.util.Arrays;

public enum EIPTransactionStatus {

    SUCCESS(200), FAILED(500), REVERSAL(600), PENDING_REVERSAL(604), PENDING(601), REPOSTED(602), TIMEOUT(799), PENDING_VALIDATION(603);

    final int statusCode;

    EIPTransactionStatus(int statusCode) {
        this.statusCode = statusCode;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public static EIPTransactionStatus fromStatusCode(int statusCode) {
        return Arrays.stream(EIPTransactionStatus.values()).filter(t -> t.statusCode == statusCode).findFirst().orElse(null);
    }
}
