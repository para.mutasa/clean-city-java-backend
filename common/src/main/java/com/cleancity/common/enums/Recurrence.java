package com.cleancity.common.enums;

public enum Recurrence {

    OneMonth,TwoMonths,ThreeMonths, OnceOff
}
