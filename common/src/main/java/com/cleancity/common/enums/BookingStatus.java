package com.cleancity.common.enums;

public enum BookingStatus {

    PENDING,
    ACCEPTED,
    DECLINED,
    ASSIGNED,
    DRIVER_CANCELLATION_IN_PROGRESS,
    CANCELLED,
    REASSIGNED,
    DISPATCHED,
    COMPLETED;
}

