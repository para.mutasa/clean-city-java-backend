package com.cleancity.common.enums;

public enum TransactionType {
     RTGS,
    INTERNAL_FUNDS_TRANSFER,
    CHECK_TRANSACTION_STATUS;
}
