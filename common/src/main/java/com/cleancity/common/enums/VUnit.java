package com.cleancity.common.enums;

public enum VUnit {
    BAGS, TONNAGE
}
