package com.cleancity.common.enums;

import com.fasterxml.jackson.annotation.JsonCreator;

public enum VehicleStatus {

    AVAILABLE, UNAVAILABLE,DELETED;

    public String getStatus() {
        return this.name();
    }

    @JsonCreator
    public static VehicleStatus getVehicleFromCode(String value) {

        for (VehicleStatus vehicleStatus : VehicleStatus.values()) {

            if (vehicleStatus.getStatus().equals(value)) {

                return vehicleStatus;
            }
        }
        return null;
    }

}
