package com.cleancity.common;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Validator {
    private Validator(){
        super();
    }

    public static boolean isValidDate(String dateString){
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        try {
            LocalDateTime.parse(dateString, dateTimeFormatter);
        }catch (Exception e){
            return false;
        }
        return true;
    }

    public static boolean isValidEmail(String email) {
        return email.matches("^[\\w-_\\.+]*[\\w-_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$");
    }

    public static boolean isValidCustomerName(String customerName) {
        return customerName.matches("[A-Z][a-zA-Z]*");
    }

    public static boolean isValidCustomerRepresentative(String customerRepresentative) {
        return customerRepresentative.matches("[a-zA-z]+([ '-][a-zA-Z]+)*");
    }

    public static boolean isValidPhoneNumber(String phoneNumber){
        return phoneNumber.matches("^\\+(?:[0-9] ?){6,14}[0-9]$");
    }


}
