package com.cleancity.service.api;

import com.cleancity.common.GeneralResponse;
import com.cleancity.common.enums.CollectionType;
import com.cleancity.domain.CollectionSubType;

public interface ICollectionSubTypeService {

    GeneralResponse createCollectionSubType( CollectionSubType collectionSubType);

    GeneralResponse updateCollectionSubType(CollectionSubType collectionSubType);

    GeneralResponse getCollectionSubTypeById(Long collectionSubTypeId);

    GeneralResponse getAllCollectionSubType();

  GeneralResponse getAllCollectionSubTypeByCollectionType(CollectionType collectionType);






}
