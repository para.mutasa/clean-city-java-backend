package com.cleancity.service.api;

import org.springframework.stereotype.Service;

@Service
public interface PasswordEncryptionService {
    String encrypt(String password);
    boolean checkPassword(String password, String storedPassword);
}
