package com.cleancity.service.api;

import com.cleancity.common.GeneralResponse;
import com.cleancity.common.enums.BookingStatus;
import com.cleancity.common.enums.PaymentStatus;
import com.cleancity.common.enums.WasteType;
import com.cleancity.domain.Booking;
import com.cleancity.domain.dto.AdminApproveCancelDTO;

public interface IBookingService {

    GeneralResponse createBooking(Booking booking);

    GeneralResponse updateBooking(Booking booking);

    GeneralResponse getBookingByBookingId(Long bookingId);

    GeneralResponse historyByDriverId(Long driverId);

    GeneralResponse getBookingByBookingNumber(String bookingNumber);

    GeneralResponse getAllBookings();


    GeneralResponse cancelBooking(Long bookingId);

    GeneralResponse reAssignFranchise(Long bookingId, Long iFranchiseId);

    GeneralResponse acceptBooking(Long bookingId, Long iDriverId, Long iFranchiseId);

    GeneralResponse updateCollection(Booking booking);

    GeneralResponse cleanCityConfirmRequest(Long iCabBookingId);

    GeneralResponse getBookingsByiFranchiseId(Long iFranchiseId);

    GeneralResponse getBookingsByiUserId(Long iUserId);

    GeneralResponse startTrip(Long IBookingId);

    GeneralResponse endTrip(Long IBookingId);

    GeneralResponse getAllBookingsByDriverId(Long driverId);

    GeneralResponse reAssignDriver( Long bookingId,Long driverId);

    GeneralResponse getBookingsByeStatus(BookingStatus eStatus);

    GeneralResponse approveCancel(AdminApproveCancelDTO approveCancelDTO);

    GeneralResponse getBookingsByPaymentsStatus(PaymentStatus paymentStatus);

    GeneralResponse getBookingsByIFranchiseIdAndPaymentStatus(Long iFranchiseId,PaymentStatus paymentStatus);

    GeneralResponse getBookingsByWasteType(WasteType wasteType);


    //   GeneralResponse assignFranchiseDriver( Long iDriverId, Long iFranchiseId);



}
