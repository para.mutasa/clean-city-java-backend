package com.cleancity.service.api;

import com.cleancity.common.GeneralResponse;
import com.cleancity.domain.Vehicle;

public interface IVehicleService {
    GeneralResponse createVehicle(Vehicle vehicle);
    GeneralResponse updateVehicle(Vehicle vehicle);
    GeneralResponse getVehicleByVehicleId(Long vehicleId);
    GeneralResponse getVehicleByLicensePlateNumber(Long licensePlateNumber);

    GeneralResponse getVehiclesByModel(String model);
    GeneralResponse getVehiclesByFranchiseId(Long franchiseId);
    GeneralResponse getVehiclesByVehicleStatus(String vehicleStatus);
    GeneralResponse getAllVehicles();
}






