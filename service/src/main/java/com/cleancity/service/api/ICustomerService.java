package com.cleancity.service.api;

import com.cleancity.common.GeneralResponse;
import com.cleancity.common.enums.Status;
import com.cleancity.domain.Customer;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public interface ICustomerService {

    GeneralResponse updateCustomer(Customer customer);

    GeneralResponse getCustomerById(Long customerId);

    GeneralResponse updateCustomerEmail(HashMap<String, Object> params);

    GeneralResponse getCustomerByElogisticsId(Long elogisticsId);

    GeneralResponse getAllCustomers();

    GeneralResponse getAllElogisticsCustomers();

    GeneralResponse approveCustomer(Long eLogisticsUserId, Status status);

    GeneralResponse deleteCustomer(Long userId);


    Optional<Map<String, Object>> getAndUpdateCustomerDetails(Long elogisticsId);
}
