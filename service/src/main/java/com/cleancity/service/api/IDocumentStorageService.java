package com.cleancity.service.api;


import com.cleancity.common.GeneralResponse;
import org.hibernate.usertype.UserType;
import org.springframework.core.io.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;

public interface IDocumentStorageService {
    // uploads proof of payment
    GeneralResponse storeProofOfPayment(MultipartFile file, String reference, Long bookingId);

    ResponseEntity<Resource> loadFileAsResource(Long iDriverId,Long bookingId, HttpServletRequest request);

  //  GeneralResponse storeCustomerFile(MultipartFile file, Long customerId);

    GeneralResponse storeCustomerDocuments(MultipartFile nationalId, MultipartFile proofOfResidence, MultipartFile bankStatement, MultipartFile tradeLicense, Long userId);


    // GeneralResponse listDocumentsByPartnerId(HashMap<String, String> allRequestParams);
}
