package com.cleancity.service.api;

import com.cleancity.common.GeneralResponse;
import com.cleancity.common.enums.Status;
import com.cleancity.domain.Currency;


public interface ICurrencyService {


    GeneralResponse createCurrency(Currency currency);

   // GeneralResponse getDefaultCurrency();

    GeneralResponse updateCurrency(Currency currency);

    GeneralResponse getCurrencyById(Long currencyId);

    GeneralResponse getCurrencyByCode(String currencyCode);

    GeneralResponse  getAllCurrency();

    GeneralResponse getCurrencyByStatus(Status status);





}
