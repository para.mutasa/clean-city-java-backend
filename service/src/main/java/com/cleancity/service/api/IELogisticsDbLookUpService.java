package com.cleancity.service.api;

import com.cleancity.common.GeneralResponse;

import java.util.List;
import java.util.Map;
import java.util.Optional;

public interface IELogisticsDbLookUpService {
    Optional<Map<String, Object>> getCustomerByPhone(String msisdn);
    GeneralResponse getCustomerByUserId(Long userId);
    Optional<Map<String, Object>> getDriverByUserId(Long msisdn);
    List<Map<String, Object>> getAllDrivers();
    List<Map<String, Object>> getAllCustomers();
    List<Map<String, Object>> getDriverDocuments(Long eLogisticsId);

    void importAllCustomersFromElogistics();
}
