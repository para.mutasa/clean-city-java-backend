package com.cleancity.service.api;

import com.cleancity.common.GeneralResponse;
import com.cleancity.domain.Franchise;

public interface IFranchiseService {



    GeneralResponse createFranchise(Franchise franchise);

    GeneralResponse updateFranchise(Franchise franchise);

    GeneralResponse getFranchiseById(Long franchiseId);

    GeneralResponse getFranchiseByUsernameAndPassword(String email , String password);

    GeneralResponse getFranchiseVehiclesById(Long franchiseId);

    GeneralResponse getFranchiseByEmail(String email);

    GeneralResponse forgotPassword(String email);

    GeneralResponse resetPassword(String email,String resetToken,String newpassword);

    GeneralResponse getAllFranchises();
}
