package com.cleancity.service.api;

import com.cleancity.common.GeneralResponse;
import com.cleancity.common.enums.DriverStatus;
import com.cleancity.domain.FranchiseDriver;
import com.cleancity.domain.dto.DriverCancelBookingDTO;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

public interface IFranchiseDriverService {


    GeneralResponse updateFranchiseDriver(FranchiseDriver driver);

    GeneralResponse getFranchiseDriverById(Long driverId);

    @Transactional("transactionManager")
    GeneralResponse approveDriver(Long driverId);

    GeneralResponse getFranchiseDriverByElogisticsId(Long elogisticsDriverId);

    GeneralResponse getFranchiseDriversByFranchiseId(Long franchiseId);

    GeneralResponse getFranchiseDriversByStatus(DriverStatus status);

    GeneralResponse getAllFranchiseDrivers();

    GeneralResponse getFranchiseDriverDocuments(Long eLogisticsId);

    GeneralResponse assignFranchise(Long franchiseId, Long driverId);

    Optional<FranchiseDriver> getAndSaveDriverDetails(Long eLogisticsDriverId);

    GeneralResponse cancelBooking(DriverCancelBookingDTO cancelBookingDTO);

}
