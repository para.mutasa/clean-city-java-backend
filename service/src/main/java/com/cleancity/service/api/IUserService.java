package com.cleancity.service.api;

import com.cleancity.common.GeneralResponse;
import com.cleancity.common.enums.Status;
import com.cleancity.domain.User;
import org.hibernate.annotations.CreationTimestamp;
import org.springframework.data.jpa.repository.Query;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.time.LocalDateTime;

public interface IUserService {

    //GeneralResponse createUser(User user);
   // GeneralResponse updateUser(User user);
    GeneralResponse getUserByUserId(Long userId);
    GeneralResponse getAllUsers();

    GeneralResponse getAllElogisticsDrivers();

    GeneralResponse getAllElogisticsUsers();

    GeneralResponse login(String username, String password);
    // GeneralResponse getUserByName(String name);
    //GeneralResponse getUserBySurname(String surname);
   // GeneralResponse getUserByPhoneNumber(String phoneNumber);
   // GeneralResponse getUserByEmail(String email);
    //GeneralResponse getUsersByGender(String gender);
 //   GeneralResponse getUsersByStatus(Status status);
 //   GeneralResponse getUsersByUserType(String userType);
  //  GeneralResponse getUsersByUserCreatedBy(String userCreatedBy);
//    GeneralResponse sendSMSNotification(Long userId);

}
