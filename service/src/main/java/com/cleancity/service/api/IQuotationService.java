package com.cleancity.service.api;

import com.cleancity.common.GeneralResponse;
import com.cleancity.domain.Quotation;


public interface IQuotationService {

    GeneralResponse createQuotation(Quotation qoutation);

    GeneralResponse updateQuotation(Quotation qoutation);

    GeneralResponse getQuotationById(Long id);

    GeneralResponse getQuotationsByBookingId(Long Id);

    GeneralResponse getAllQuotations();







}
