package com.cleancity.service.api;

import com.cleancity.common.GeneralResponse;
import com.cleancity.domain.City;

public interface ICityService {

    GeneralResponse createCity(City city);
    GeneralResponse updateCity(City city);
    GeneralResponse getCityById(Long id);
    GeneralResponse getCityByCityName(String cityName);
    GeneralResponse getCitiesByCountry(String country);
    GeneralResponse getCitiesByCountryCode(String countryCode);
    GeneralResponse getCitiesByProvince(String province);
    GeneralResponse getAllCities();

}


