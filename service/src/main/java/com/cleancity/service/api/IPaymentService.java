package com.cleancity.service.api;

import com.cleancity.common.GeneralResponse;
import com.cleancity.common.enums.PaymentMethod;
import com.cleancity.common.enums.PaymentStatus;
import com.cleancity.domain.dto.BankPaymentDTO;
import com.cleancity.domain.dto.EcocashRequestCallbackDTO;
import com.cleancity.domain.Payment;
import com.cleancity.domain.dto.UpdatePaymentStatusDTO;

public interface IPaymentService {
    GeneralResponse createPayment(Payment payment);

    GeneralResponse makeEcocashPayment(Payment payment);
    GeneralResponse getAllBanks();

    GeneralResponse updatePayment(Payment payment);
    GeneralResponse updatePaymentStatus(UpdatePaymentStatusDTO payment);
    GeneralResponse processEcocashPaymentCallback(EcocashRequestCallbackDTO requestCallback);

    GeneralResponse getPaymentByPaymentId(Long paymentId);
    GeneralResponse getPaymentsByCurrencyCode(String code);
    GeneralResponse getPaymentsByPaymentMethod(PaymentMethod paymentMethod);
    GeneralResponse getPaymentByBookingId(Long bookingId);
    GeneralResponse getPaymentsByPaymentStatus( PaymentStatus paymentStatus);
    GeneralResponse getPaymentsByPaidBy( String paidBy);
    GeneralResponse getPaymentByApprovedBy( String approvedBy);
    GeneralResponse getAllPayments();

    GeneralResponse makeBankPayment(BankPaymentDTO payment);
}










