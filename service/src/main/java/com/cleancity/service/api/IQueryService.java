package com.cleancity.service.api;

import com.cleancity.common.GeneralResponse;
import com.cleancity.domain.Query;

public interface IQueryService {
    GeneralResponse createQuery(Query query);
    GeneralResponse updateQuery(Query query);
    GeneralResponse getQueryById(Long Id);
    GeneralResponse getAllQueries();
    GeneralResponse getQueriesByStatus(String queryStatus);
    GeneralResponse getQueriesByPhone(String phone);
    GeneralResponse getQueriesByEmail(String email);
    GeneralResponse getQueriesByCustomerId(String customerId);
}
