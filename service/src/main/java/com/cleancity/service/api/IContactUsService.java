package com.cleancity.service.api;

import com.cleancity.common.GeneralResponse;
import com.cleancity.domain.ContactUs;

public interface IContactUsService {
    GeneralResponse createContactUS(ContactUs contactUs);

    GeneralResponse getContactUsById(Long Id);

    GeneralResponse getAllContactUs();

    GeneralResponse getContactUsByPhone(String phone);

    GeneralResponse getContactUSByByEmail(String email);

}
