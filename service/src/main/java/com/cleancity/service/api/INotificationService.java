package com.cleancity.service.api;

import com.cleancity.common.GeneralResponse;
import com.cleancity.domain.Notification;


public interface INotificationService {


    GeneralResponse createNotification(Notification notification);

    GeneralResponse updateNotification(Notification notification);

    GeneralResponse getNotificationById(Long notificationId);

    GeneralResponse  getAllNotifications();

    GeneralResponse getAllNotificationsByStatus(String notificationStatus);





}
