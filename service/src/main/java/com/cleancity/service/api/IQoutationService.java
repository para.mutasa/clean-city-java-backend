package com.cleancity.service.api;

import com.cleancity.common.GeneralResponse;
import com.cleancity.domain.Quotation;


public interface IQoutationService {

    GeneralResponse createQoutation(Quotation qoutation);

    GeneralResponse updateQoutation(Quotation qoutation);

    GeneralResponse getQoutationById(Long id);

    GeneralResponse getAllQoutations();







}
