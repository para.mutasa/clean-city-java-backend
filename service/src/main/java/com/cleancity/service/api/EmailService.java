package com.cleancity.service.api;

public interface EmailService {

    void sendEmail(String to, String body);

}
