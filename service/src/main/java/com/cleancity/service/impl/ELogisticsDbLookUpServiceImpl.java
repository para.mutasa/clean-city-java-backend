package com.cleancity.service.impl;


import com.cleancity.common.GeneralResponse;
import com.cleancity.common.GeneralResponseBuilder;
import com.cleancity.domain.Customer;
import com.cleancity.domain.Franchise;
import com.cleancity.domain.FranchiseDriver;
import com.cleancity.domain.dto.ElogisticCustomerDTO;
import com.cleancity.persistence.CustomerRepository;
import com.cleancity.persistence.FranchiseDriverRepository;
import com.cleancity.persistence.elogistics.repository.GenericRepo;
import com.cleancity.service.api.IELogisticsDbLookUpService;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.json.JsonMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.google.gson.Gson;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Slf4j
@Service
@RequiredArgsConstructor
public class ELogisticsDbLookUpServiceImpl implements IELogisticsDbLookUpService {

    final GenericRepo genericRepo;


    @Autowired
    FranchiseDriverRepository franchiseDriverRepository;

    @Autowired
    CustomerRepository customerRepository;


    @Value("${elogistics.customer.lookup.by-phone}")
    String customerLookUpByPhoneQuery;

    @Value("${elogistics.customer.lookup.by-id}")
    String customerLookUpByIdQuery;

    @Value("${elogistics.driver.lookup.by-id}")
    String driverLookUpByIdQuery;

    @Value("${elogistics.customer.lookup.all}")
    String customerLookUpAllQuery;

    @Value("${elogistics.driver.lookup.all}")
    String driverLookUpAllQuery;

    @Value("${elogistics.driver.lookup.all-documents}")
    String driverDocumentsLookUpByIdQuery;


    @Override
    public Optional<Map<String, Object> > getCustomerByPhone(String msisdn){
        final String validatedPhone = phoneValidator(msisdn);
        Map<String, Object> lookupParams = new HashMap<>();
        lookupParams.put("msisdn", validatedPhone);
        log.info("look up query {}", customerLookUpByPhoneQuery);
        final List<Map<String, Object>> userDetailsList = genericRepo.getGenericList(lookupParams, customerLookUpByPhoneQuery);
        if (userDetailsList.isEmpty()){
            return Optional.empty();
        }
        Map<String, Object> registerUser = userDetailsList.get(0);
        //   Integer iUserId = (Integer) registerUser.get("iUserId");
        return Optional.ofNullable(registerUser);
    }

    @Override
    public GeneralResponse getCustomerByUserId(Long userId){
        Map<String, Object> lookupParams = new HashMap<>();
        lookupParams.put("iUserId", userId);
        log.info("look up query {}", customerLookUpByIdQuery);
        final List<Map<String, Object>> userDetailsList = genericRepo.getGenericList(lookupParams, customerLookUpByIdQuery);
        if (userDetailsList.isEmpty()){
          //  return Optional.empty();
         return   GeneralResponseBuilder.buildFailed(null,"Customer not found");
        }
        Map<String, Object> registerUser = userDetailsList.get(0);
        return   GeneralResponseBuilder.buildSuccess(registerUser,"Success");
    }


    @Override
    public Optional<Map<String, Object> > getDriverByUserId(Long driverId){
        Map<String, Object> lookupParams = new HashMap<>();
        lookupParams.put("IDriverId", driverId);
        log.info("look up query {}", driverLookUpByIdQuery);
        final List<Map<String, Object>> userDetailsList
                = genericRepo.getGenericList(lookupParams, driverLookUpByIdQuery);
        if (userDetailsList.isEmpty()){
            return Optional.empty();
        }
        Map<String, Object> registerUser = userDetailsList.get(0);
        return Optional.ofNullable(registerUser);
    }

    @Override
    public List<Map<String, Object>> getAllDrivers() {

        return   genericRepo.getGenericMapList( driverLookUpAllQuery);
    }

    @Override
    public List<Map<String, Object>> getAllCustomers() {
        return    genericRepo.getGenericMapList( customerLookUpAllQuery);
    }

    @Override
    public List<Map<String, Object>> getDriverDocuments(Long eLogisticsId) {
      //  return genericRepo.getGenericMapList( customerLookUpAllQuery);
        Map<String, Object> lookupParams = new HashMap<>();
        lookupParams.put("id", eLogisticsId);
        log.info("look up query {}", customerLookUpByIdQuery);
        return genericRepo.getGenericList(lookupParams, driverDocumentsLookUpByIdQuery);
    }

    @Override
    public void importAllCustomersFromElogistics(){
        //Slow on first import
        List<Map<String, Object>> all =  genericRepo.getGenericMapList( customerLookUpAllQuery);
        all.parallelStream().forEach(stringObjectMap -> {
            Gson gson = new Gson();
            ElogisticCustomerDTO customerDTO =  gson.fromJson(gson.toJson(stringObjectMap),ElogisticCustomerDTO.class);//mapper.convertValue(stringObjectMap, ElogisticCustomerDTO.class);
               Customer  customer = customerDTO.getCustomer();
            Optional<Customer> optionalCustomer = customerRepository.findCustomerByeLogisticsUserId(customer.getELogisticsUserId());
            if(optionalCustomer.isEmpty())
                            customerRepository.save(customer);
      });
    }

/* add driver
    @Override
    public void importAllDriversFromElogistics(){
        //Slow on first import
        List<Map<String, Object>> all =  genericRepo.getGenericMapList( customerLookUpAllQuery);
        all.parallelStream().forEach(stringObjectMap -> {
            Gson gson = new Gson();
            ElogisticCustomerDTO customerDTO =  gson.fromJson(gson.toJson(stringObjectMap),ElogisticCustomerDTO.class);//mapper.convertValue(stringObjectMap, ElogisticCustomerDTO.class);
            FranchiseDriver customer = customerDTO.getCustomer();
            Optional<Customer> optionalCustomer = customerRepository.findCustomerByeLogisticsUserId(customer.getELogisticsUserId());
            if(optionalCustomer.isEmpty())
                customerRepository.save(customer);
        });
    }
*/



    private String phoneValidator(String vPhone){
        if (vPhone.startsWith("0")){
            return vPhone.substring(1);
        }
        else if (vPhone.startsWith("263")){
            return vPhone.substring(3);
        }
        else if (vPhone.startsWith("00263")){
            return vPhone.substring(5);
        }
        else if (vPhone.startsWith("+263")){
            return vPhone.substring(4);
        }
        return vPhone;
    }
}
