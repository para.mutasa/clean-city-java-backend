package com.cleancity.service.impl;

import com.cleancity.service.api.IVehicleService;
import com.cleancity.common.GeneralResponse;
import com.cleancity.common.GeneralResponseBuilder;
import com.cleancity.domain.Vehicle;
import com.cleancity.persistence.VehicleRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Locale;
import java.util.Optional;
@Service
@RequiredArgsConstructor
public class VehicleServiceImpl implements IVehicleService {
    @Autowired
    VehicleRepository vehicleRepository;
    @Autowired
    private MessageSource messageSource;

    //@Value("${general.response.success.vehicle}")
    private String vehicle_success="";

    //@Value("${general.response.fail.vehicle}")
    private String vehicle_fail="";


    @Override
    public GeneralResponse createVehicle(Vehicle vehicle) {
        vehicleRepository.save(vehicle);


        return GeneralResponseBuilder.buildSuccess(vehicle, messageSource.getMessage("general.response.success.vehicle",new Object[0],new Locale("en")));   }

    @Override
    public GeneralResponse updateVehicle(Vehicle vehicle) {
        Optional<Vehicle> vehicleVal = vehicleRepository.findVehicleByVehicleId(vehicle.getVehicleId());
        if (vehicleVal.isPresent()){
            vehicleRepository.save(vehicle);
            return GeneralResponseBuilder.buildSuccess(vehicle, messageSource.getMessage("general.response.success.vehicle",new Object[0],new Locale("en")));
        }
        return GeneralResponseBuilder.buildFailed(null, messageSource.getMessage("general.response.fail.vehicle",new Object[0],new Locale("en")));    }


    @Override
    public GeneralResponse getVehicleByVehicleId(Long vehicleId) {
        Optional<Vehicle> vehicleVal = vehicleRepository.findVehicleByVehicleId(vehicleId);

        if (vehicleVal.isPresent()) {

            return GeneralResponseBuilder.buildSuccess(vehicleVal.get(), messageSource.getMessage("general.response.success.vehicle",new Object[0],new Locale("en")));


        } else {
            return GeneralResponseBuilder.buildFailed(null, messageSource.getMessage("general.response.fail.vehicle",new Object[0],new Locale("en")));
        }    }

    @Override
    public GeneralResponse getVehicleByLicensePlateNumber(Long licensePlateNumber) {
        Optional<Vehicle> vehicleVal = vehicleRepository.findVehicleByLicensePlateNumber(licensePlateNumber);
        if (vehicleVal.isPresent()) {
            return GeneralResponseBuilder.buildSuccess(vehicleVal.get(), messageSource.getMessage("general.response.success.vehicle",new Object[0],new Locale("en")));
        } else {
            return GeneralResponseBuilder.buildFailed(null, messageSource.getMessage("general.response.fail.vehicle",new Object[0],new Locale("en")));
        }
    }


    @Override
    public GeneralResponse getVehiclesByModel(String model) {
        List<Vehicle> vehiclesVal = vehicleRepository.findVehiclesByModel(model);
        if (!vehiclesVal.isEmpty()){
            return GeneralResponseBuilder.buildSuccess(vehiclesVal, messageSource.getMessage("general.response.success.vehicle",new Object[0],new Locale("en")));
        }
        return GeneralResponseBuilder.buildFailed(null, messageSource.getMessage("general.response.fail.vehicle",new Object[0],new Locale("en")));
    }

    @Override
    public GeneralResponse getVehiclesByFranchiseId(Long franchiseId) {
        List<Vehicle> vehiclesVal = vehicleRepository.findVehiclesByFranchiseId(franchiseId);
        if (!vehiclesVal.isEmpty()){
            return GeneralResponseBuilder.buildSuccess(vehiclesVal, messageSource.getMessage("general.response.success.vehicle",new Object[0],new Locale("en")));
        }
        return GeneralResponseBuilder.buildFailed(null, messageSource.getMessage("general.response.fail.vehicle",new Object[0],new Locale("en")));
    }


    @Override
    public GeneralResponse getVehiclesByVehicleStatus(String vehicleStatus) {
        List<Vehicle> vehiclesVal = vehicleRepository.findVehiclesByVehicleStatus(vehicleStatus);
        if (!vehiclesVal.isEmpty()){
            return GeneralResponseBuilder.buildSuccess(vehiclesVal, messageSource.getMessage("general.response.success.vehicle",new Object[0],new Locale("en")));
        }
        return GeneralResponseBuilder.buildFailed(null, messageSource.getMessage("general.response.fail.vehicle",new Object[0],new Locale("en")));
    }

    @Override
    public GeneralResponse getAllVehicles() {

        return GeneralResponseBuilder.buildSuccess(vehicleRepository.findAll(), messageSource.getMessage("general.response.success.vehicle",new Object[0],new Locale("en")));
    }


}
