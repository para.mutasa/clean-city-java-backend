package com.cleancity.service.impl;

import com.cleancity.common.enums.CollectionType;
import com.cleancity.common.enums.PaymentStatus;
import com.cleancity.common.enums.Recurrence;
import com.cleancity.domain.Booking;
import com.cleancity.domain.CollectionSubType;
import com.cleancity.domain.Currency;
import com.cleancity.service.api.IQuotationService;
import com.cleancity.common.GeneralResponse;
import com.cleancity.common.GeneralResponseBuilder;
import com.cleancity.domain.Quotation;
import com.cleancity.persistence.QuotationRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
@Service
@RequiredArgsConstructor
public class QuotationServiceImpl implements IQuotationService {

    @Autowired
    private QuotationRepository quotationRepository;
    @Autowired
    private MessageSource messageSource;

    //@Value("${general.response.success.createQuotation}")
    private String success_message="";
    //@Value("${general.response.fail.createQuotation}")
    private String failed_message="";




    @Override
    public GeneralResponse createQuotation(Quotation quotation) {
        quotationRepository.save(quotation);

        return GeneralResponseBuilder.buildSuccess(quotation,    messageSource.getMessage("general.response.success.createQuotation",new Object[0],new Locale("en"))
        );
    }

    @Override
    public GeneralResponse updateQuotation(Quotation quotation) {
        Optional<Quotation> quotationVal= quotationRepository.findById(quotation.getId());
        if (quotationVal.isPresent()){

            quotationRepository.save(quotation);
            return GeneralResponseBuilder.buildSuccess(quotationVal.get(),
           messageSource.getMessage("general.response.success.updateQuotation",new Object[0],new Locale("en")));


        }else {
            return GeneralResponseBuilder.buildFailed(null,messageSource.getMessage("general.response.fail.updateQuotation",new Object[0],new Locale("en"))
            );
        }
    }

    @Override
    public GeneralResponse getQuotationById(Long Id) {
        Optional<Quotation> quotationVal= quotationRepository.findById(Id);
        if (quotationVal.isPresent()){

            return GeneralResponseBuilder.buildSuccess(quotationVal.get(),messageSource.getMessage("general.response.success.getQuotation",new Object[0],new Locale("en"))
            );

        }else {
            return GeneralResponseBuilder.buildFailed(null,messageSource.getMessage("general.response.fail.getQuotation",new Object[0],new Locale("en"))
            );
        }

    }

    @Override
    public GeneralResponse getQuotationsByBookingId(Long Id) {
        List<Quotation> quotationVal= quotationRepository.findQuotationsByBookingId(Id);

            return GeneralResponseBuilder.buildSuccess(quotationVal,messageSource.getMessage("general.response.success.getQuotation",new Object[0],new Locale("en")));
     }



    @Override
    public GeneralResponse getAllQuotations() {


        List<Quotation> quotationList = quotationRepository.findAll();

        return GeneralResponseBuilder.buildSuccess(quotationList, messageSource.getMessage("general.response.success.getQuotation",new Object[0],new Locale("en"))
        );
    }
/*

    boolean updateQuotation(Long bookingId) {
        Optional<Booking> optionalBooking = bookingRepository.findBookingByIBookingId(bookingId);
        if (optionalBooking.isEmpty()) return false;
        Booking booking = optionalBooking.get();

        Optional<Currency> currencyOptional = currencyRepository.findById(booking.getCurrencyId());
        if (currencyOptional.isEmpty()) return false;
        Currency currency = currencyOptional.get();

        Optional<Quotation> optionalQuotation = quotationRepository.findQuotationByBookingId(bookingId);
        Quotation quotation = Quotation.builder()
                .currency(currency.getCode())
                .build();

        if (optionalQuotation.isPresent()) {
            quotation = optionalQuotation.get();

            if (LocalDateTime.now().isBefore(quotation.getValidUntil()) || optionalBooking.get().getPaymentStatus().equals(PaymentStatus.PAID))
                return false;
        }
        double exchangeRate = currency.getExchangeRate();
        double quantity = booking.getIQuantity();

        Long vSubTypeId = optionalBooking.get().getISubCollectionTypeId();
        CollectionType collectionType = optionalBooking.get().getCollectionType();
        Optional<CollectionSubType> collectionSubType = collectionSubTypeRepository.findCollectionSubTypeById(vSubTypeId);

        if (collectionSubType.isEmpty()) {
            collectionSubType = collectionSubTypeRe.63
        +pository.findCollectionSubTypeByCollectionType(collectionType);
            if (collectionSubType.isEmpty()) {
                return false;
            }
        }
        Double amount = (collectionSubType.get().getCostPerUnit() * quantity * exchangeRate);
        //todo: recurrence
        Recurrence recurrence = booking.getRecurrence();

        if(collectionSubType.get().getPricePoint().equals(PricePoint.PerBag)) {
            switch (recurrence) {

                case OnceOff:
                    amount *= 1;

                    break;

                case Weekly:
                    amount *= 2;
                    break;

                case Monthly:
                    break;

                case Fortnightly:
                    break;
            }
        }



        Long daysToExpire = collectionSubType.get().getQuotationValidityDays();
        LocalDate now = LocalDate.now();
        LocalDate date = now.plusDays(daysToExpire);
        quotation.setValidUntil(date.atTime(18, 0));
        quotation.setAmount(amount);
        quotation.setWasteType(booking.getWasteType());
        quotation.setBookingId(booking.getIBookingId());
        quotation.setCustomerId(booking.getIUserId());
        quotation.setCurrency(currency.getCode());
        //todo add zwl and usd prices
        quotationRepository.save(quotation);

        //todo add zwl and usd prices to booking and save

        return true;
    }

*/



}
