package com.cleancity.service.impl;

import com.cleancity.common.GeneralResponse;
import com.cleancity.common.GeneralResponseBuilder;
import com.cleancity.common.enums.Status;
import com.cleancity.domain.Customer;
import com.cleancity.persistence.CustomerRepository;
import com.cleancity.persistence.elogistics.repository.GenericRepo;
import com.cleancity.service.api.ICustomerService;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.json.JsonMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;


@Service
@RequiredArgsConstructor
@Slf4j
public class CustomerServiceImpl implements ICustomerService {

    @Autowired
    CustomerRepository customerRepository;

    final GenericRepo genericRepo;

    @Value("${elogistics.customer.lookup.by-id}")
    String customerLookUpByIdQuery;

    @Value("${elogistics.customer.lookup.all}")
    String customerLookUpAllQuery;

    @Value("${elogistics.customer.update.email}")
    String updateCustomerEmailQuery;

    @Value("${elogistics.customer.lookup.by-email}")
    String customerLookUpByEmailQuery;

    @Override
    public GeneralResponse updateCustomer(Customer customer) {
        Optional<Customer> optionalCustomer = customerRepository.findCustomerByeLogisticsUserId(customer.getUserId());
        if (optionalCustomer.isPresent()) {

            customer = customerRepository.save(customer);

            return GeneralResponseBuilder.buildSuccess(customer, "Customer saved successfully");

        }
        return GeneralResponseBuilder.buildFailed(null, "Customer not found");
    }

    @Override
    public GeneralResponse getCustomerById(Long customerId) {
        Optional<Customer> optionalCustomer = customerRepository.findCustomerByeLogisticsUserId(customerId);
        if (optionalCustomer.isPresent()) {
            return GeneralResponseBuilder.buildSuccess(optionalCustomer.get(), "Customer saved successfully");
        }
        return GeneralResponseBuilder.buildFailed(null, "Customer not found");

    }

    @Override
    public GeneralResponse updateCustomerEmail(HashMap<String, Object> params) {
        String oldEmail = params.get("oldEmail").toString();
        String newEmail = params.get("newEmail").toString();

        List<Map<String, Object>> userDetailsList = genericRepo.getGenericList(params, customerLookUpByEmailQuery);

        if (userDetailsList.isEmpty())
            return GeneralResponseBuilder.buildFailed(null, "Customer not found");

        final int status = genericRepo.genericInsert(params, updateCustomerEmailQuery);

        Optional<Customer> optionalCustomer = customerRepository.findCustomerByEmail(oldEmail);
        if (optionalCustomer.isPresent() && status != 0) {
            Customer customer = optionalCustomer.get();
            customer.setEmail(newEmail);
            return GeneralResponseBuilder.buildSuccess(customerRepository.save(customer), "Customer email updated successfully");
        }
        if (status != 0) return GeneralResponseBuilder.buildSuccess(null, "Customer email updated successfully");

        return GeneralResponseBuilder.buildFailed(null, "Customer not found");

    }

    @Override
    public GeneralResponse getCustomerByElogisticsId(Long elogisticsId) {
        //  Optional<Map<String, Object>> optionalcustomerMap = dbLookUpService.getCustomerByUserId(elogisticsId);
        Optional<Map<String, Object>> optionalMap = getAndUpdateCustomerDetails(elogisticsId);
        if (optionalMap.isEmpty()) {

            return GeneralResponseBuilder.buildFailed(null, "Customer not found");
        }
        return GeneralResponseBuilder.buildSuccess(optionalMap.get(), "Success");
    }

    @Override
    public GeneralResponse getAllCustomers() {
        List<Customer> customerList = customerRepository.findAll();
        if (customerList.isEmpty()) {
            return GeneralResponseBuilder.buildFailed(null, "No customers");
        }
        return GeneralResponseBuilder.buildSuccess(customerList, "Successful");
    }

    @Override
    public GeneralResponse getAllElogisticsCustomers() {
        List<Map<String, Object>> users = genericRepo.getGenericMapList(customerLookUpAllQuery);
        if (users.isEmpty()) {
            return GeneralResponseBuilder.buildFailed(null, "No customers");
        }
        return GeneralResponseBuilder.buildSuccess(users, "Successful");
    }

    @Override
    public Optional<Map<String, Object>> getAndUpdateCustomerDetails(Long elogisticsId) {
        Map<String, Object> lookupParams = new HashMap<>();
        lookupParams.put("iUserId", elogisticsId);
        log.info("look up query {}", customerLookUpByIdQuery);
        List<Map<String, Object>> userDetailsList = genericRepo.getGenericList(lookupParams, customerLookUpByIdQuery);
        Optional<Customer> optionalCustomer = customerRepository.findCustomerByeLogisticsUserId(elogisticsId);
        if (userDetailsList.isEmpty() ){
        //    optionalCustomer.ifPresent(customer -> customerRepository.delete(customer));
            return Optional.empty();//
        }
        Map<String, Object> customerMap = userDetailsList.get(0);
         Customer customer = new Customer();
        customer.setStatus(Status.INACTIVE);
        if (optionalCustomer.isPresent()) customer = optionalCustomer.get();
        customer.setELogisticsUserId(elogisticsId);
        customer.setPhoneNo(customerMap.get("vPhone")==null?"":customerMap.get("vPhone").toString());
        customer.setName(customerMap.get("vFirstName")==null?"":customerMap.get("vFirstName").toString());
        customer.setEmail(customerMap.get("VEmail")==null?"":customerMap.get("VEmail").toString());
        customer.setSurname(customerMap.get("vLastName")==null?"":customerMap.get("vLastName").toString());
        customer.setCompanyName(customerMap.get("companyName")==null?"":customerMap.get("companyName").toString());
        customer.setAddress(customerMap.get("vCaddress")==null?"":customerMap.get("vCaddress").toString());
        customer.setPartnerType(customerMap.get("partnerType")==null?"":customerMap.get("partnerType").toString());
        customer.setZimraBPNumber(customerMap.get("zimraBpNummber")==null?"":customerMap.get("zimraBpNummber").toString());
        customer = customerRepository.save(customer);
        ObjectMapper oMapper = JsonMapper.builder()
                .addModule(new JavaTimeModule())
                .build();
        customerMap.putAll(oMapper.convertValue(customer, Map.class));
        return Optional.of(customerMap);
    }

    @Override
    public GeneralResponse approveCustomer(Long eLogisticsUserId, Status status) {
        getAndUpdateCustomerDetails(eLogisticsUserId);
        Optional<Customer>customerOptional=customerRepository.findCustomerByeLogisticsUserId(eLogisticsUserId);

        if (customerOptional.isEmpty())
            return GeneralResponseBuilder.buildFailed(null,"Customer not Found");

        Customer customer=customerOptional.get();
        if (customer.getNationId() != null && customer.getProofOfResidence() != null){
          customer.setStatus(Status.ACTIVE);
          return GeneralResponseBuilder.buildSuccess(customerRepository.save(customer),"Customer has been activated");
        }else return GeneralResponseBuilder.buildFailed(null,"customer could not be activated");
    }

    @Override
    public GeneralResponse deleteCustomer(Long userId) {
        Optional<Customer>customerOptional=customerRepository.findCustomerByeLogisticsUserId(userId);

        if (customerOptional.isEmpty())
            return GeneralResponseBuilder.buildFailed(null,"Customer not Found");
        Customer customer=customerOptional.get();
        customer.setStatus(Status.DELETED);
        return GeneralResponseBuilder.buildSuccess( customerRepository.save(customer),"Customer has been deleted");
    }



}



