package com.cleancity.service.impl;

import com.cleancity.service.api.ICityService;
import com.cleancity.common.GeneralResponse;
import com.cleancity.common.GeneralResponseBuilder;
import com.cleancity.domain.City;
import com.cleancity.persistence.CityRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Locale;
import java.util.Optional;


@Service
@RequiredArgsConstructor
public class CityServiceImpl implements ICityService {

    @Autowired
    CityRepository cityRepository;

    @Autowired
    private MessageSource messageSource;

    @Override
    public GeneralResponse createCity(City city) {
        Optional<City> newCity = cityRepository.findCityByCityName(city.getCityName());
        if(newCity.isPresent()) {
           return GeneralResponseBuilder.buildFailed(null, messageSource.getMessage("general.response.failed.createCity",new Object[0],new Locale("en")));
        }
        cityRepository.save(city);
        return GeneralResponseBuilder.buildSuccess(city, messageSource.getMessage("general.response.success.createCity",new Object[0],new Locale("en")));
    }

    @Override
    public GeneralResponse updateCity(City city) {
        Optional<City> updateCity = cityRepository.findCityById(city.getId());
        if(updateCity.isPresent()) {
            cityRepository.save(city);
            return GeneralResponseBuilder.buildSuccess(city, messageSource.getMessage("general.response.success.updateCity",new Object[0],new Locale("en")));
        }
        return GeneralResponseBuilder.buildFailed(null, messageSource.getMessage("general.response.failed.updateCity",new Object[0],new Locale("en")));
    }


    @Override
    public GeneralResponse getCityById(Long id) {
        Optional<City> cityVal = cityRepository.findCityById(id);
        if(cityVal.isPresent()) {
            return GeneralResponseBuilder.buildSuccess(cityVal.get(), messageSource.getMessage("general.response.success.getCity",new Object[0],new Locale("en")));
        }
        return GeneralResponseBuilder.buildFailed(null, messageSource.getMessage("general.response.failed.getCity",new Object[0],new Locale("en")));    }

    @Override
    public GeneralResponse getCityByCityName(String cityName) {
        Optional<City> cityVal = cityRepository.findCityByCityName(cityName);
        if(cityVal.isPresent()) {

            return GeneralResponseBuilder.buildSuccess(cityVal.get(), messageSource.getMessage("general.response.success.getCity",new Object[0],new Locale("en")));
        }
        return GeneralResponseBuilder.buildFailed(null, messageSource.getMessage("general.response.failed.getCity",new Object[0],new Locale("en")));    }


    @Override
    public GeneralResponse getCitiesByCountry(String country) {
        List<City> cityVal = cityRepository.findCitiesByCountry(country);
        if (!cityVal.isEmpty()){
            return GeneralResponseBuilder.buildSuccess(cityVal, messageSource.getMessage("general.response.success.getCity",new Object[0],new Locale("en")) );
        }
        return GeneralResponseBuilder.buildFailed(null, messageSource.getMessage("general.response.failed.getCity",new Object[0],new Locale("en")));
    }

    @Override
    public GeneralResponse getCitiesByCountryCode(String countryCode) {
        List<City> cityVal = cityRepository.findCitiesByCountryCode(countryCode);
        if (!cityVal.isEmpty()){
            return GeneralResponseBuilder.buildSuccess(cityVal, messageSource.getMessage("general.response.success.getCity",new Object[0],new Locale("en")) );
        }
        return GeneralResponseBuilder.buildFailed(null, messageSource.getMessage("general.response.failed.getCity",new Object[0],new Locale("en")));    }

    @Override
    public GeneralResponse getCitiesByProvince(String province) {
        List<City> cityVal = cityRepository.findCitiesByProvince(province);
        if (!cityVal.isEmpty()){
            return GeneralResponseBuilder.buildSuccess(cityVal, messageSource.getMessage("general.response.success.getCity",new Object[0],new Locale("en")) );
        }
        return GeneralResponseBuilder.buildFailed(null, messageSource.getMessage("general.response.failed.getCity",new Object[0],new Locale("en")));    }


    @Override
    public GeneralResponse getAllCities() {
        return GeneralResponseBuilder.buildSuccess(cityRepository.findAll(), messageSource.getMessage("general.response.success.getCity",new Object[0],new Locale("en")) );
    }
}
