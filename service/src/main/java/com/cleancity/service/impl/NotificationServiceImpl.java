package com.cleancity.service.impl;

import com.cleancity.service.api.INotificationService;
import com.cleancity.common.GeneralResponse;
import com.cleancity.common.GeneralResponseBuilder;
import com.cleancity.domain.Notification;
import com.cleancity.persistence.NotificationRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
@Service
@RequiredArgsConstructor
public class NotificationServiceImpl implements INotificationService {

    @Autowired
    private NotificationRepository notificationRepository;

    //@Value("${general.response.success.createNotification}")
    private String success_message="";
    //@Value("${general.response.fail.createNotification}")
    private String failed_message="";

    @Override
    public GeneralResponse createNotification(Notification notification) {
        notificationRepository.save(notification);

        return GeneralResponseBuilder.buildSuccess(notification, success_message);
    }

    @Override
    public GeneralResponse updateNotification(Notification notification) {
        Optional<Notification> notificationVal = notificationRepository.findById(notification.getNotificationId());
        if (notificationVal.isPresent()) {

            return GeneralResponseBuilder.buildSuccess(notificationVal.get(), success_message);

        } else {
            return GeneralResponseBuilder.buildFailed(null, failed_message);
        }
    }

    @Override
    public GeneralResponse getNotificationById(Long notificationId) {
        Optional<Notification> notificationVal = notificationRepository.findById(notificationId);
        if (notificationVal.isPresent()) {

            return GeneralResponseBuilder.buildSuccess(notificationVal.get(), success_message);

        } else {
            return GeneralResponseBuilder.buildFailed(null, failed_message);
        }

    }

    @Override
    public GeneralResponse getAllNotifications() {


        List<Notification> notificationsList = notificationRepository.findAll();

        return GeneralResponseBuilder.buildSuccess(notificationsList, success_message);
    }

    @Override
    public GeneralResponse getAllNotificationsByStatus(String notificationStatus) {

        List<Notification> allByStatus = notificationRepository.findAllByNotificationStatus(notificationStatus);

        return GeneralResponseBuilder.buildSuccess(allByStatus, success_message);
    }
}
