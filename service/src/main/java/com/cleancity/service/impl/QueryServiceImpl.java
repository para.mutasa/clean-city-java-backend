package com.cleancity.service.impl;

import com.cleancity.service.api.IQueryService;
import com.cleancity.common.GeneralResponse;
import com.cleancity.common.GeneralResponseBuilder;
import com.cleancity.domain.Query;
import com.cleancity.persistence.QueryRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Locale;
import java.util.Optional;
@Service
@RequiredArgsConstructor
public class QueryServiceImpl implements IQueryService {


    @Autowired
    QueryRepository queryRepository;

    @Autowired
    private MessageSource messageSource;

    @Override
    public GeneralResponse getQueriesByPhone(String phone) {
        List<Query> queryVal = queryRepository.findQueriesByPhone(phone);
        if (!queryVal.isEmpty()){
           return GeneralResponseBuilder.buildSuccess(queryVal, messageSource.getMessage("general.response.success.getQuery",new Object[0],new Locale("en")));
        }
        return GeneralResponseBuilder.buildFailed(null, messageSource.getMessage("general.response.fail.getQuery",new Object[0],new Locale("en")));
    }

    @Override
    public GeneralResponse getQueriesByEmail(String email) {
        List<Query> queryVal = queryRepository.findQueriesByEmail(email);
        if (!queryVal.isEmpty()){
           return GeneralResponseBuilder.buildSuccess(queryVal, messageSource.getMessage("general.response.success.getQuery",new Object[0],new Locale("en")));
        }
        return GeneralResponseBuilder.buildFailed(null, messageSource.getMessage("general.response.fail.getQuery",new Object[0],new Locale("en")));
    }

    @Override
    public GeneralResponse getQueriesByCustomerId(String customerId) {
        List<Query> queryVal = queryRepository.findQueriesByCustomerId(customerId);
        if (!queryVal.isEmpty()){
            return GeneralResponseBuilder.buildSuccess(queryVal, messageSource.getMessage("general.response.success.getQuery",new Object[0],new Locale("en")));

        }
        return GeneralResponseBuilder.buildFailed(null, messageSource.getMessage("general.response.fail.getQuery",new Object[0],new Locale("en")));
    }


    @Override
    public GeneralResponse createQuery(Query query) {
        Optional<Query> newQuery = queryRepository.findQueryById(query.getId());
        if(newQuery.isPresent()) {
           return GeneralResponseBuilder.buildFailed(null, messageSource.getMessage("general.response.failed.newQuery",new Object[0],new Locale("en")));
        }
        queryRepository.save(query);
        return GeneralResponseBuilder.buildSuccess(query, messageSource.getMessage("general.response.success.newQuery",new Object[0],new Locale("en")));
    }


    @Override
    public GeneralResponse updateQuery(Query query) {
        Optional<Query> queryVal = queryRepository.findQueryById(query.getId());
        if (queryVal.isPresent()){
            queryRepository.save(query);
            return GeneralResponseBuilder.buildSuccess(query, messageSource.getMessage("general.response.success.updateQuery",new Object[0],new Locale("en")));
        }
        return GeneralResponseBuilder.buildFailed(null, messageSource.getMessage("general.response.fail.updateQuery",new Object[0],new Locale("en")));
    }

    @Override
    public GeneralResponse getQueryById(Long Id) {
        Optional<Query> queryVal = queryRepository.findQueryById(Id);
        if (queryVal.isPresent()){
            return GeneralResponseBuilder.buildSuccess(queryVal.get(), messageSource.getMessage("general.response.success.getQuery",new Object[0],new Locale("en")) );
        }
        return GeneralResponseBuilder.buildFailed(null, messageSource.getMessage("general.response.fail.getQuery",new Object[0],new Locale("en")));
    }

    @Override
    public GeneralResponse getAllQueries() {
        return GeneralResponseBuilder.buildSuccess(queryRepository.findAll(), messageSource.getMessage("general.response.success.getQuery",new Object[0],new Locale("en")) );

    }

    @Override
    public GeneralResponse getQueriesByStatus(String queryStatus) {
        List<Query> queryVal = queryRepository.findQueriesByStatus(queryStatus);
        if (!queryVal.isEmpty()){
            return GeneralResponseBuilder.buildSuccess(queryVal, messageSource.getMessage("general.response.success.getQuery",new Object[0],new Locale("en")));
        }
        return GeneralResponseBuilder.buildFailed(null, messageSource.getMessage("general.response.fail.getQuery",new Object[0],new Locale("en")));
        }
}

