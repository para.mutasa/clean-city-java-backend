package com.cleancity.service.impl;

import com.cleancity.service.api.PasswordEncryptionService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.Pbkdf2PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class PasswordEncryptionServiceImpl implements PasswordEncryptionService {

    /**
     *
     * Pbkdf2PasswordEncoder relies on the PBKDF2 algorithm to hash passwords.
     *
     * It has three optional arguments:
     *
     * Secret - Key used during the encoding process. As the name implies, it should be secret.
     * Iteration - The number of iterations used to encode the password, the documentation
     *              advises as many iterations for your system to take 0.5 seconds to hash.
     * Hash Width - The size of the hash itself.
     *
     * A secret is object type of java.lang.CharSequence and when a developer supplies it
     * to the constructor, the encoded password will contain the secret.
     *
     * Constructors:
     *  1. Pbkdf2PasswordEncoder()
     *  2. Pbkdf2PasswordEncoder(java.lang.CharSequence secret)
     *  3. Pbkdf2PasswordEncoder(java.lang.CharSequence secret, int iterations, int hashWidth)
     */
    @Override
    public String encrypt(String password){
        Pbkdf2PasswordEncoder encoder = getPasswordEncoder();
        String encodedPassword = encoder.encode(password);
        return encodedPassword;
    }

    private Pbkdf2PasswordEncoder getPasswordEncoder() {
        Pbkdf2PasswordEncoder encoder = new Pbkdf2PasswordEncoder("q$Ajhh62xJ7{s", 10000, 32);
        encoder.setAlgorithm(Pbkdf2PasswordEncoder.SecretKeyFactoryAlgorithm.PBKDF2WithHmacSHA256);
        return encoder;
    }

    @Override
    public boolean checkPassword(String password, String storedPassword) {
        final Pbkdf2PasswordEncoder encoder = getPasswordEncoder();
        final boolean isCorrectPassword = encoder.matches(password, storedPassword);
        return isCorrectPassword;
    }

}
