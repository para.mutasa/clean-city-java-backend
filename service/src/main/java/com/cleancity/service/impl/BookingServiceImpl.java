package com.cleancity.service.impl;

import com.cleancity.common.GeneralResponse;
import com.cleancity.common.GeneralResponseBuilder;
import com.cleancity.common.enums.*;
import com.cleancity.domain.Currency;
import com.cleancity.domain.*;
import com.cleancity.domain.dto.AdminApproveCancelDTO;
import com.cleancity.persistence.*;
import com.cleancity.service.api.IBookingService;
import com.cleancity.service.api.ICustomerService;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.time.LocalDateTime;
import java.util.*;


@Service
@RequiredArgsConstructor
@Slf4j
public class BookingServiceImpl implements IBookingService {

    @Autowired
    BookingRepository bookingRepository;

    @Autowired
    FranchiseDriverRepository franchiseDriverRepository;

    @Autowired
    FranchiseRepository franchiseRepository;

    @Autowired
    CollectionSubTypeRepository collectionSubTypeRepository;

    @Autowired
    private MessageSource messageSource;

    @Value("${integrations.google-api-key}")
    String GOOGLE_API_KEY;

    @Autowired
    CurrencyRepository currencyRepository;

    @Autowired
    QuotationRepository quotationRepository;

    @Autowired
    CustomerRepository customerRepository;

    @Autowired
    ICustomerService customerService;


    @Override
    public GeneralResponse updateBooking(Booking booking) {

        Optional<Booking> bookingVal = bookingRepository.findById(booking.getIBookingId());

        if (bookingVal.isPresent()) {
            bookingRepository.save(booking);
            return GeneralResponseBuilder.buildSuccess(booking, messageSource.getMessage("general.response.success.booking", new Object[0], new Locale("en")));
        } else {
            return GeneralResponseBuilder.buildFailed(null, messageSource.getMessage("general.response.booking.notfound", new Object[0], new Locale("en")));
        }

    }

    @Override
    public GeneralResponse createBooking(Booking bookingRequest) {
        Optional<Booking> bookingVal = bookingRepository.findBookingByIBookingId(bookingRequest.getIBookingId());

        if (bookingVal.isPresent())
            return GeneralResponseBuilder.buildFailed(null, messageSource.getMessage("general.response.failed.booking", new Object[0], new Locale("en")));

            customerService.getAndUpdateCustomerDetails(bookingRequest.getIUserId());

            Optional<Customer> customerOptional = customerRepository.findCustomerByeLogisticsUserId(bookingRequest.getIUserId());
            if (customerOptional.isEmpty()) {
                // TODO: 4/5/22 create booking for guest customer
                return GeneralResponseBuilder.buildFailed(null, "Customer is not available");
            }

            if (customerOptional.get().getPartnerType().equals("Corporate")) {

                if (customerOptional.get().getNationId() == null) {
                    return GeneralResponseBuilder.buildFailed(null, "Please upload your national Id");
                }
                if (customerOptional.get().getProofOfResidence() == null) {
                    return GeneralResponseBuilder.buildFailed(null, "Please upload your proof of residence");
                }
                if (customerOptional.get().getStatus().equals(Status.INACTIVE)) {
                    return GeneralResponseBuilder.buildFailed(null, "Customer is not active");
                }

            }

            //todo: add actual billing , Also distance
            Optional<Currency> currencyOptional = currencyRepository.findCurrencyByCode(bookingRequest.getCurrencyCode());
            if (currencyOptional.isEmpty())
                return GeneralResponseBuilder.buildFailed(null, "Currency not specified");

            Long vSubTypeId = bookingRequest.getISubCollectionTypeId();
            CollectionType collectionType = bookingRequest.getCollectionType();
            Optional<CollectionSubType> collectionSubType = collectionSubTypeRepository.findCollectionSubTypeById(vSubTypeId);

            if (collectionSubType.isEmpty()) {
                List<CollectionSubType> collectionSubTypes = collectionSubTypeRepository.findCollectionSubTypesByCollectionType(collectionType);
                if (collectionSubTypes.isEmpty()) {
                    return GeneralResponseBuilder.buildFailed(null, "Failed. Collection subtype not found.");
                }
                collectionSubType = Optional.of(collectionSubTypes.get(0));
            }

            String sQuantity = bookingRequest.getIQuantity();
            double quantity = 1.0;
            //   double exchangeRate = currencyOptional.get().getExchangeRate();
            Optional<Currency> zwlCurrencyOptional = currencyRepository.findCurrencyByCode("ZWL");
            if (zwlCurrencyOptional.isEmpty())
                return GeneralResponseBuilder.buildFailed(null, "Failed. ZWL currency config not set");

            if (zwlCurrencyOptional.get().getStatus().equals(Status.INACTIVE) || zwlCurrencyOptional.get().getStatus().equals(Status.DELETED))
                return GeneralResponseBuilder.buildFailed(null, "Failed. ZWL currency not active");

            double zwlExchangeRate = zwlCurrencyOptional.get().getExchangeRate();
            if (bookingRequest.getVUnit().equals(VUnit.BAGS))
                quantity = Double.parseDouble(sQuantity);
            else switch (Tonnage.valueOf(sQuantity)) {//todo: ask for guidance
                case OneToTwo:
                    quantity = 2;
                    break;
                case Three:
                    quantity = 3;
                    break;
                case FourToSix:
                    quantity = 6;
                    break;
                case Seven:
                    quantity = 7;
                    break;
                case TenToTwelve:
                    quantity = 12;
                    break;
            }

            double usdAmount = (collectionSubType.get().getCostPerUnit() * quantity);
            double zwlAmount = usdAmount * zwlExchangeRate;
            bookingRequest.setEStatus(BookingStatus.PENDING);
            bookingRequest.setFPrice(usdAmount);
            bookingRequest.setZWLPrice(zwlAmount);
            Booking booking = bookingRepository.save(bookingRequest);

            if (booking.getCurrencyCode().isEmpty())
                return GeneralResponseBuilder.buildFailed(null, "Booking has no currency set"); //todo: use messages.p


            if (booking.getWasteType() == null)
                return GeneralResponseBuilder.buildFailed(null, "Booking has no waste type set"); //todo: use messages.p


            //todo: use messages.p

            String currency = currencyOptional.get().getName();
            Quotation quotation = Quotation.builder()
                    .bookingId(booking.getIBookingId())
                    .amount(usdAmount)
                    .zwlAmount(zwlAmount)
                    .createdDateTime(LocalDateTime.now())
                    .currency(currency)
                    .customerId(booking.getIUserId())
                    .wasteType(booking.getWasteType())
                    .recurrence(booking.getRecurrence())
                    .build();
            quotationRepository.save(quotation);
            return GeneralResponseBuilder.buildSuccess(bookingRequest, messageSource.getMessage("general.response.success.booking", new Object[0], new Locale("en")));

    }





    @Override
    public GeneralResponse getBookingByBookingId(Long bookingId) {
        Optional<Booking> bookingVal = bookingRepository.findBookingByIBookingId(bookingId);

        if (bookingVal.isPresent()) {

            return GeneralResponseBuilder.buildSuccess(bookingVal.get(), messageSource.getMessage("general.response.success.booking", new Object[0], new Locale("en")));


        } else {
            return GeneralResponseBuilder.buildFailed(null, messageSource.getMessage("general.response.booking.notfound", new Object[0], new Locale("en")));
        }

    }

    @Override
    public GeneralResponse getBookingsByiFranchiseId(Long iFranchiseId) {
        Optional<Franchise> optionalFranchise = franchiseRepository.findById(iFranchiseId);
        if (optionalFranchise.isEmpty())
            return GeneralResponseBuilder.buildFailed(null, "Franchise not found");
        if (optionalFranchise.get().getStatus().equals(Status.INACTIVE))
            return GeneralResponseBuilder.buildFailed(null, "Franchise not active");

        List<Booking> bookingVal = bookingRepository.findBookingsByiFranchiseId(iFranchiseId);
        if (!bookingVal.isEmpty()) {
            return GeneralResponseBuilder.buildSuccess(bookingVal, messageSource.getMessage("general.response.success.booking", new Object[0], new Locale("en")));
        } else {
            return GeneralResponseBuilder.buildFailed(null, messageSource.getMessage("general.response.booking.notfound", new Object[0], new Locale("en")));
        }
    }

    @Override
    public GeneralResponse getBookingsByiUserId(Long iUserId) {
        List<Booking> bookings = bookingRepository.findBookingsByiUserId(iUserId);
        if (!bookings.isEmpty()) {
            return GeneralResponseBuilder.buildSuccess(bookings, messageSource.getMessage("general.response.success.booking", new Object[0], new Locale("en")));
        } else {
            return GeneralResponseBuilder.buildFailed(null, messageSource.getMessage("general.response.booking.notfound", new Object[0], new Locale("en")));
        }
    }


    @Override
    public GeneralResponse startTrip(Long IBookingId) {
        Optional<Booking> startTripVal = bookingRepository.findBookingByIBookingId(IBookingId);
        if (startTripVal.isPresent()) {
            startTripVal.get().setIBookingId(IBookingId);
            startTripVal.get().setEStatus(BookingStatus.DISPATCHED);
            bookingRepository.save(startTripVal.get());
            return GeneralResponseBuilder.buildSuccess(startTripVal.get(), messageSource.getMessage("general.response.startTrip.success", new Object[0], new Locale("en")));
        } else {
            return GeneralResponseBuilder.buildFailed(null, messageSource.getMessage("general.response.startTrip.failed", new Object[0], new Locale("en")));
        }
    }

    @Override
    public GeneralResponse endTrip(Long IBookingId) {
        Optional<Booking> endTripVal = bookingRepository.findBookingByIBookingId(IBookingId);
        if (endTripVal.isPresent()) {
            endTripVal.get().setIBookingId(IBookingId);
            endTripVal.get().setEStatus(BookingStatus.COMPLETED);
            bookingRepository.save(endTripVal.get());
            return GeneralResponseBuilder.buildSuccess(endTripVal.get(), messageSource.getMessage("general.response.endTrip.success", new Object[0], new Locale("en")));
        } else {
            return GeneralResponseBuilder.buildFailed(null, messageSource.getMessage("general.response.endTrip.failed", new Object[0], new Locale("en")));
        }
    }

    @Override
    public GeneralResponse getAllBookingsByDriverId(Long driverId) {
        List<Booking> bookings = bookingRepository.findBookingsByiDriverId(driverId);
        return GeneralResponseBuilder.buildSuccess(bookings, messageSource.getMessage("general.response.success.booking", new Object[0], new Locale("en")));
    }

    @Override
    public GeneralResponse reAssignDriver(Long bookingId, Long driverId) {
        Optional<FranchiseDriver> franchiseDriverOptional = franchiseDriverRepository.findById(driverId);

        if (franchiseDriverOptional.isEmpty())
            return GeneralResponseBuilder.buildFailed(null, "Driver not found");

        if (franchiseDriverOptional.get().getStatus().equals(DriverStatus.INACTIVE))
            return GeneralResponseBuilder.buildFailed(null, "Driver not active");

        Optional<Booking> bookingVal = bookingRepository.findBookingByIBookingId(bookingId);
        if (bookingVal.isPresent()) {
            Booking booking = bookingVal.get();
            booking.setEStatus(BookingStatus.REASSIGNED);
            booking.setIDriverId(driverId);
            bookingRepository.save(booking);

            return GeneralResponseBuilder.buildSuccess(bookingVal, messageSource.getMessage("general.response.success.booking", new Object[0], new Locale("en")));
        } else {
            return GeneralResponseBuilder.buildFailed(null, messageSource.getMessage("general.response.booking.notfound", new Object[0], new Locale("en")));
        }
    }


    @Override
    public GeneralResponse historyByDriverId(Long driverId) {

        return GeneralResponseBuilder.buildSuccess(driverId, messageSource.getMessage("general.response.success.booking", new Object[0], new Locale("en")));
      /*  } else {
            return GeneralResponseBuilder.buildFailed(null, messageSource.getMessage("general.response.booking.notfound", new Object[0], new Locale("en")));
        }*/
    }


    @Override
    public GeneralResponse getBookingByBookingNumber(String bookingNumber) {
        Optional<Booking> bookingVal = bookingRepository.findBookingByVBookingNo(bookingNumber);
        if (bookingVal.isPresent()) {
            return GeneralResponseBuilder.buildSuccess(bookingVal, messageSource.getMessage("general.response.success.booking", new Object[0], new Locale("en")));
        } else {
            return GeneralResponseBuilder.buildFailed(null, messageSource.getMessage("general.response.booking.notfound", new Object[0], new Locale("en")));
        }
    }


    @Override
    public GeneralResponse getAllBookings() {
        List<Booking> bookingList = bookingRepository.findAll();
        return GeneralResponseBuilder.buildSuccess(bookingList, messageSource.getMessage("general.response.success", new Object[0], new Locale("en")));
    }

    @Override
    public GeneralResponse cancelBooking(Long bookingId) {
        Optional<Booking> bookingVal = bookingRepository.findBookingByIBookingId(bookingId);
        if (bookingVal.isPresent()) {
            bookingVal.get().setEStatus(BookingStatus.CANCELLED);
            bookingRepository.save(bookingVal.get());

            //   String bookingNo = bookingVal.get().getVBookingNo();

            //            TODO
            //             to send message through sms service using below infor
            //   Long iUserId = bookingVal.get().getIUserId();
            //   String message = "Dear valued customer, your booking ".concat(bookingNo).concat(" has been cancelled");

            return GeneralResponseBuilder.buildSuccess(bookingVal.get(), messageSource.getMessage("general.response.booking.cancelled", new Object[0], new Locale("en")));

        } else {
            return GeneralResponseBuilder.buildFailed(null, messageSource.getMessage("general.response.booking.notfound", new Object[0], new Locale("en")));
        }
    }


    @Override
    public GeneralResponse reAssignFranchise(Long bookingId, Long iFranchiseId) {
        Optional<Booking> bookingVal = bookingRepository.findBookingByIBookingId(bookingId);
        Optional<Franchise> optionalFranchise = franchiseRepository.findById(iFranchiseId);

        if (bookingVal.isPresent() && optionalFranchise.isPresent()) {

            Franchise franchise = optionalFranchise.get();
            if (franchise.getStatus().equals(Status.INACTIVE))
                return GeneralResponseBuilder.buildFailed(null, "Failed. Franchise inactive.");
            Booking booking = bookingVal.get();
//            Long vSubTypeId = bookingVal.get().getISubCollectionTypeId();
//            Optional<CollectionSubType> collectionSubType = collectionSubTypeRepository.findCollectionSubTypeByTypeId(vSubTypeId);
//            //    Long vCollectionTypeId = booking.getICollectionTypeId();
//
//            String destLatitude = franchise.getLattitude();
//            String destLongitude = franchise.getLongitude();
//
//            //   String destAddress = franchise.getAddress();
//            String sourceLatitude = booking.getVCollectionsRequestLat();
//            String sourceLongitude = booking.getVCollectionsRequestLong();
//            String quantity = booking.getIQuantity();
//            //  Long iZoneId = booking.getIZoneId();
//
//            double distance = getDrivingDistance(sourceLatitude, sourceLongitude, destLatitude, destLongitude);
//            float cost = 0.00f;
//            if (collectionSubType.isPresent()) {
//                double totalBill = 9.8;//(collectionSubType.get().getRatePerKm() * distance) + (collectionSubType.get().getCostPerUnit() * quantity);
//                cost = Float.parseFloat(String.format("%.2f", totalBill));
//            }
            booking.setIFranchiseId(iFranchiseId);
            // booking.setVDistance(Float.parseFloat(String.valueOf(distance)));
            //booking.setFPrice(cost);

            booking.setEStatus(BookingStatus.ACCEPTED);
            bookingRepository.save(booking);

            String bookingNo = String.valueOf(booking.getIBookingId());//booking.getVBookingNo();
            //Long iUserId = booking.getIUserId();

            String message = "Dear valued customer, your booking ".concat(bookingNo).concat(" has been accepted");
            //todo:: send SMS

            return GeneralResponseBuilder.buildSuccess(bookingVal.get(), messageSource.getMessage("general.response.booking.accepted", new Object[0], new Locale("en")));

        } else {
            return GeneralResponseBuilder.buildFailed(null, messageSource.getMessage("general.response.booking.acceptFailed", new Object[0], new Locale("en")));
        }
    }

    @Override
    public GeneralResponse acceptBooking(Long bookingId, Long iDriverId, Long iFranchiseId) {
        Optional<Booking> bookingVal = bookingRepository.findBookingByIBookingId(bookingId);
        Optional<Franchise> optionalFranchise = franchiseRepository.findById(iFranchiseId);

        if (optionalFranchise.isEmpty())
            return GeneralResponseBuilder.buildFailed(null, "Franchise not found");

        if (optionalFranchise.get().getStatus().equals(Status.INACTIVE))
            return GeneralResponseBuilder.buildFailed(null, "Franchise not active");

        Optional<FranchiseDriver> franchiseDriverOptional = franchiseDriverRepository.findById(iDriverId);

        if (franchiseDriverOptional.isEmpty())
            return GeneralResponseBuilder.buildFailed(null, "Driver not found");

        if (franchiseDriverOptional.get().getStatus().equals(DriverStatus.INACTIVE))
            return GeneralResponseBuilder.buildFailed(null, "Driver not active");

        if (bookingVal.isPresent()) {
            //    Franchise franchise = optionalFranchise.get();

            Booking booking = bookingVal.get();
//            Long vSubTypeId = bookingVal.get().getISubCollectionTypeId();
//            Optional<CollectionSubType> collectionSubType = collectionSubTypeRepository.findCollectionSubTypeByTypeId(vSubTypeId);
//             //  Long vCollectionTypeId = booking.getICollectionTypeId();
//
//            String destLatitude = franchise.getLattitude();
//            String destLongitude = franchise.getLongitude();
//
//            //   String destAddress = franchise.getAddress();
//            String sourceLatitude = booking.getVCollectionsRequestLat();
//            String sourceLongitude = booking.getVCollectionsRequestLong();
//            String quantity = booking.getIQuantity();
//            //  Long iZoneId = booking.getIZoneId();
//
//            double distance = getDrivingDistance(sourceLatitude, sourceLongitude, destLatitude, destLongitude);
//            float cost = 0.00f;
//            if (collectionSubType.isPresent()) {
//                double totalBill =9.0;// (collectionSubType.get().getRatePerKm() * distance) + (collectionSubType.get().getCostPerUnit() * quantity);
//                cost = Float.parseFloat(String.format("%.2f", totalBill));
//            }

            booking.setIDriverId(iDriverId);
            booking.setIFranchiseId(iFranchiseId);
            // booking.setVDistance(Float.parseFloat(String.valueOf(distance)));
            //booking.setFPrice(cost);
            booking.setIDriverId(iDriverId);
            booking.setEStatus(BookingStatus.ASSIGNED);

            bookingRepository.save(booking);

            String bookingNo = booking.getVBookingNo();
            //Long iUserId = booking.getIUserId();

            String message = "Dear valued customer, your booking has been accepted";
            //todo:: send SMS

            return GeneralResponseBuilder.buildSuccess(bookingVal.get(), messageSource.getMessage("general.response.booking.accepted", new Object[0], new Locale("en")));

        } else {
            return GeneralResponseBuilder.buildFailed(null, messageSource.getMessage("general.response.booking.acceptFailed", new Object[0], new Locale("en")));
        }
    }

    //TODO:: update method name
    @Override
    public GeneralResponse cleanCityConfirmRequest(Long IBookingId) {
        Optional<Booking> bookingVal = bookingRepository.findBookingByIBookingId(IBookingId);
        if (bookingVal.isPresent()) {
            bookingVal.get().setEStatus(BookingStatus.PENDING);
            bookingRepository.save(bookingVal.get());
            String bookingNo = bookingVal.get().getVBookingNo();

            //TODO:: to send message through sms service using below information
            Long iUserId = bookingVal.get().getIUserId();
            String message = "Dear valued customer, your booking ".concat(bookingNo).concat(" has been updated.");
            return GeneralResponseBuilder.buildSuccess(bookingVal.get(), messageSource.getMessage("general.response.booking.update", new Object[0], new Locale("en")));
        }
        return GeneralResponseBuilder.buildFailed(null, messageSource.getMessage("general.response.booking.notfound", new Object[0], new Locale("en")));
    }

    @Override
    public GeneralResponse getBookingsByeStatus(BookingStatus eStatus) {

        return GeneralResponseBuilder.buildSuccess(bookingRepository.findBookingsByeStatus(eStatus), messageSource.getMessage("general.response.booking.accepted", new Object[0], new Locale("en")));
    }

    @Override
    public GeneralResponse approveCancel(AdminApproveCancelDTO approveCancelDTO) {
        Optional<Booking> optionalBooking = bookingRepository.findById(approveCancelDTO.getBookingId());
        if (optionalBooking.isPresent()) {
            Booking booking = optionalBooking.get();
            if (!booking.getEStatus().equals(BookingStatus.DRIVER_CANCELLATION_IN_PROGRESS))
                return GeneralResponseBuilder.buildSuccess(booking, "Booking has no cancel request");

            booking.setCancellationApproveBy(approveCancelDTO.getUserId());
            booking.setEStatus(BookingStatus.CANCELLED);
            booking = bookingRepository.save(booking);

            return GeneralResponseBuilder.buildSuccess(booking, messageSource.getMessage("general.response.success.updateCollection", new Object[0], new Locale("en")));
        }

        return GeneralResponseBuilder.buildSuccess(null, messageSource.getMessage("general.response.success.updateCollection", new Object[0], new Locale("en")));
    }

    @Override
    public GeneralResponse getBookingsByPaymentsStatus(PaymentStatus paymentStatus) {
        List<Booking> bookingByPaymentStatusVal = bookingRepository.findBookingsByPaymentStatus(PaymentStatus.PAID);
        if (!bookingByPaymentStatusVal.isEmpty()) {
            return GeneralResponseBuilder.buildSuccess(bookingByPaymentStatusVal, messageSource.getMessage("general.response.paymentStatus.success", new Object[0], new Locale("en")));
        } else {
            return GeneralResponseBuilder.buildFailed(null, messageSource.getMessage("general.response.paymentStatus.failed", new Object[0], new Locale("en")));
        }
    }

    @Override
    public GeneralResponse getBookingsByIFranchiseIdAndPaymentStatus(Long iFranchiseId, PaymentStatus paymentStatus) {
        List<Booking> bookingsBypaymentstatusandfranchisidVal = bookingRepository.findBookingsByiFranchiseIdAndPaymentStatus(iFranchiseId, PaymentStatus.PAID);
        if (!bookingsBypaymentstatusandfranchisidVal.isEmpty()) {
            return GeneralResponseBuilder.buildSuccess(bookingsBypaymentstatusandfranchisidVal, messageSource.getMessage("general.response.paymentStatusandFranchise.success", new Object[0], new Locale("en")));
        } else {
            return GeneralResponseBuilder.buildFailed(null, messageSource.getMessage("general.response.paymentStatusandFranchise.failed", new Object[0], new Locale("en")));
        }
    }

    @Override
    public GeneralResponse getBookingsByWasteType(WasteType wasteType) {
        List<Booking> bookingsByWasteTypeVal = bookingRepository.findBookingsByWasteType(wasteType);
        if (!bookingsByWasteTypeVal.isEmpty()) {
            return GeneralResponseBuilder.buildSuccess(bookingsByWasteTypeVal, messageSource.getMessage("general.response.success.booking", new Object[0], new Locale("en")));
        } else {
            return GeneralResponseBuilder.buildFailed(null, messageSource.getMessage("general.response.booking.notfound", new Object[0], new Locale("en")));
        }
    }


    @Override
    public GeneralResponse updateCollection(Booking booking) {

        Optional<Booking> optionalBooking = bookingRepository.findBookingByIBookingId(booking.getIBookingId());

        if (optionalBooking.isPresent()) {
            String sourceLatitude = optionalBooking.get().getVCollectionsRequestLat();
            String sourceLongitude = optionalBooking.get().getVCollectionsRequestLong();
            String vCollectionPointLat = booking.getVCollectionPointLat();
            String vCollectionPointLong = booking.getVCollectionPointLong();
            String vCollectionAddress = booking.getVCollectionPointAddress();
            //String bookingNo = booking.getVBookingNo();
            // Long userId = booking.getIUserId();

            double distance = getDrivingDistance(sourceLatitude, sourceLongitude, vCollectionPointLat, vCollectionPointLong);

            log.info("distance {}", distance);

            if (distance > 0.5) {
                String message = "Collection is out of range";
                //  TODO::   to send message through sms service using above information
                return GeneralResponseBuilder.buildFailed(null, messageSource.getMessage("general.response.fail.updateCollection", new Object[0], new Locale("en")));

            } else {

                booking = optionalBooking.get();
                booking.setEStatus(BookingStatus.COMPLETED);
                booking.setVCollectionPointLat(vCollectionPointLat);
                booking.setVCollectionPointLong(vCollectionPointLong);
                booking.setVCollectionPointAddress(vCollectionAddress);
                bookingRepository.save(booking);
                String message = "Dear valued customer, your booking waste has been collected.";
                //todo:: send SMS

                return GeneralResponseBuilder.buildSuccess(booking, messageSource.getMessage("general.response.success.updateCollection", new Object[0], new Locale("en")));
            }

        } else {
            return GeneralResponseBuilder.buildSuccess(booking, messageSource.getMessage("general.response.booking.notfound", new Object[0], new Locale("en")));
        }
    }


    public double getDrivingDistance(String startLatitude, String startLng, String endLatitude, String endLongitude) {
/* Example::
        "lat": -17.8675202,
        "lng": 30.9518123 */

        String vGMapLangCode = "en";
        Map<String, Object> returnArr = new HashMap<>();
        //todo:: set vGMapLangCode from configs

        String url = "https://maps.googleapis.com/maps/api/directions/json?origin=".concat(startLatitude).concat(",").concat(startLng).concat("&destination=").concat(endLatitude).concat(",")
                .concat(endLongitude).concat("&sensor=false&key=").concat(GOOGLE_API_KEY).concat("&language=").concat(vGMapLangCode);

        log.info("url {}", url);

        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<String> response = null;
        try {
            response = restTemplate.getForEntity(new URI(url), String.class);
            log.info("response {}", response.getBody());
        } catch (URISyntaxException e) {
            returnArr.put("Action", "0");
        }

        JsonNode jsonData = null;
        try {
            ObjectMapper mapper = new ObjectMapper();
            jsonData = mapper.readTree(response.getBody());
            log.info("jsonData {}", jsonData);
        } catch (IOException e) {
            e.printStackTrace();
        }

        JsonNode distanceFromJsonObject = jsonData.get("routes").get(0).get("legs").get(0).get("distance").get("value");
        log.info("distanceFromJsonObject {}", distanceFromJsonObject);

        return distanceFromJsonObject.asDouble() / 1000;
    }


}
