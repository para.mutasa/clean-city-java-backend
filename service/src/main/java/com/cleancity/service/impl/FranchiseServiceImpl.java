package com.cleancity.service.impl;

import com.cleancity.domain.embeddables.UniqueIdGenerator;
import com.cleancity.service.api.EmailService;
import com.cleancity.service.api.IFranchiseService;
import com.cleancity.common.GeneralResponse;
import com.cleancity.common.GeneralResponseBuilder;
import com.cleancity.domain.Franchise;
import com.cleancity.domain.Vehicle;
import com.cleancity.persistence.FranchiseRepository;
import com.cleancity.service.api.PasswordEncryptionService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
@Service
@RequiredArgsConstructor
@Slf4j
public class FranchiseServiceImpl implements IFranchiseService {
    @Autowired
    private FranchiseRepository franchiseRepository;
    @Autowired
    private EmailService emailService;
    @Value("${general.response.success.franchise}")
    private String success_message="";
    @Value("${general.response.failed.franchise}")
    private String failed_message="";
    @Value("${general.response.success.franchiseLogin}")
    private String loginSuccessful="";
    @Value("${general.response.success.franchiseForgotPassword}")
    private String no_email="";
    @Value("${general.response.fail.franchiseForgotPassword}")
    private String sendEmailSuccess="";
    @Value("${general.response.success.franchiseForgotPassword}")
    private String loginFail="";
    @Value("${general.response.fail.failed_reset_password}")
    private String failed_reset_password="";

    @Value("${general.response.fail.WrongUsernameOrPassword}")
    private String wrongEmail="";
    @Value("${general.response.fail.WrongUsernameOrPassword}")
    private String wrongPassword="";
    @Value("${general.response.success.reset_password}")
    private String resetSuccess="";
    @Autowired
    PasswordEncryptionService passwordEncryptionService;

    @Override
    public GeneralResponse createFranchise(Franchise franchise) {


        franchiseRepository.save(franchise);
        return GeneralResponseBuilder.buildSuccess(franchise,success_message);
    }

    @Override
    public GeneralResponse updateFranchise(Franchise franchise) {
        Optional <Franchise> franchiseVal = franchiseRepository.findById(franchise.getFranchiseId());
        if (franchiseVal.isPresent()){
            franchise.setPassword(franchiseVal.get().getPassword());
            franchiseRepository.save(franchise);
            return GeneralResponseBuilder.buildSuccess(franchiseVal.get(),success_message);

        }else {
            return GeneralResponseBuilder.buildFailed(null,failed_message);
        }

    }

    @Override
    public GeneralResponse getFranchiseById(Long franchiseId) {
        Optional <Franchise> franchise = franchiseRepository.findById(franchiseId);
        if (franchise.isPresent()){

            return GeneralResponseBuilder.buildSuccess(franchise,success_message);

        }else {
            return GeneralResponseBuilder.buildFailed(null,failed_message);
        }
    }
    @Override
    public GeneralResponse getFranchiseByEmail(String email) {
        Optional <Franchise> franchiseEmail = franchiseRepository.getFranchiseByEmail(email);
        if (franchiseEmail.isPresent()){

            return GeneralResponseBuilder.buildSuccess(franchiseEmail,success_message);

        }else {
            return GeneralResponseBuilder.buildFailed(null,failed_message);
        }
    }

    @Override
    public GeneralResponse getFranchiseVehiclesById(Long franchiseId) {



        List<Vehicle> vehicles=franchiseRepository.getFranchiseVehiclesByFranchiseId(franchiseId);

        return GeneralResponseBuilder.buildSuccess(vehicles,success_message);    }



    @Override
    public GeneralResponse getAllFranchises() {

        List<Franchise> franchises=franchiseRepository.findAll();

        return GeneralResponseBuilder.buildSuccess(franchises,success_message);
    }
    @Override
    public GeneralResponse getFranchiseByUsernameAndPassword(String email, String password){
       // Optional<Franchise> franchiseUser = franchiseRepository.getFranchiseByEmailAndPassword(email,password);
        Optional<Franchise> optionalFranchise = franchiseRepository.getFranchiseByEmail(email);
        if (optionalFranchise.isEmpty()){
            return GeneralResponseBuilder.buildFailed(optionalFranchise,wrongEmail);
        }
        else {
            Franchise franchise = optionalFranchise.get();
            int attempts = franchise.getLoginAttempts();
            if (attempts < 3) {

                String storedPassword = optionalFranchise.get().getPassword();
                boolean checkedPassword = passwordEncryptionService.checkPassword(password, storedPassword);
                if (checkedPassword) {
                  //reset attempts
                    franchise.setLoginAttempts(0);
                    franchiseRepository.save(franchise);
                    return GeneralResponseBuilder.buildSuccess(optionalFranchise.get(), loginSuccessful);
                } else {
                    //increment attempt
                    franchise.setLoginAttempts(attempts+1);
                    franchiseRepository.save(franchise);
                    return GeneralResponseBuilder.buildFailed(optionalFranchise, wrongPassword);
                }
            }
            else
                return GeneralResponseBuilder.buildFailed(optionalFranchise, failed_reset_password);

        }   }
    @Override
    public GeneralResponse forgotPassword(String email) {
        String token = UniqueIdGenerator.tokenGenerator();
        Optional<Franchise> franchisemail = franchiseRepository.getFranchiseByEmail(email);
        if (!franchisemail.isPresent()) {
            return GeneralResponseBuilder.buildFailed(null, sendEmailSuccess );
        }
        franchisemail.get().setResetToken(token);

        //save token to database
        franchiseRepository.save(franchisemail.get());

        //Send Email To Franchise with token
        emailService.sendEmail(email, "Password Reset Token is: {}".replace("{}",token));
        return GeneralResponseBuilder.buildSuccess(franchisemail.get(), no_email);


        }


    @Override
    public GeneralResponse resetPassword(String email,String resetToken,String newpassword) {

        Optional<Franchise> franchisemail = franchiseRepository.getFranchiseByEmail(email);
        if (franchisemail.isEmpty()){
           return GeneralResponseBuilder.buildFailed(null, failed_message) ;
        }
        String token = franchisemail.get().getResetToken();
        if(token.equals("") || token.isEmpty() || token.equals(null)) {
              return GeneralResponseBuilder.buildFailed(null, "invalid token");
        }
        if(resetToken.equals(token)){
            Franchise franchise=franchisemail.get();
            franchise.setPassword(passwordEncryptionService.encrypt(newpassword));
            franchise.setLoginAttempts(0);
            franchise.setResetToken("");
            franchiseRepository.save(franchise);
            return  GeneralResponseBuilder.buildSuccess(null,resetSuccess);

        }
        else {
            return  GeneralResponseBuilder.buildSuccess(null,"invalid token");
        }








    }

}




