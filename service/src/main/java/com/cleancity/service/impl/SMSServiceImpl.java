package com.cleancity.service.impl;

import com.cleancity.domain.dto.SMS;
import com.cleancity.service.api.ISMSService;
import com.google.gson.Gson;
import lombok.RequiredArgsConstructor;
import okhttp3.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.IOException;


@Service
@RequiredArgsConstructor
public class SMSServiceImpl implements ISMSService {

    @Value("${sms.service}")
    private String smsUrl;

    @Override
    public String sendSMS(SMS sms) {
        OkHttpClient client = new OkHttpClient().newBuilder().build();
        MediaType mediaType = MediaType.parse("application/json");
         RequestBody body = RequestBody.create(new Gson().toJson(sms),mediaType);
        Request request = new Request.Builder().url(smsUrl).method("POST", body).addHeader("Content-Type",
                "application/json").build();
        try {
            Response response = client.newCall(request).execute();
           // log.info("SMS response :: {}", response.message());
            return String.valueOf(response.code());
        } catch (IOException e) {
            e.printStackTrace();
            return "Failed";
        }
    }
}
