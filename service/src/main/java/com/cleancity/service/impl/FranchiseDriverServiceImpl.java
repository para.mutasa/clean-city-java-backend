package com.cleancity.service.impl;

import com.cleancity.common.GeneralResponse;
import com.cleancity.common.GeneralResponseBuilder;
import com.cleancity.common.enums.BookingStatus;
import com.cleancity.common.enums.DriverStatus;
import com.cleancity.common.enums.Status;
import com.cleancity.domain.Booking;
import com.cleancity.domain.Franchise;
import com.cleancity.domain.FranchiseDriver;
import com.cleancity.domain.dto.DriverCancelBookingDTO;
import com.cleancity.persistence.BookingRepository;
import com.cleancity.persistence.FranchiseDriverRepository;
import com.cleancity.persistence.FranchiseRepository;
import com.cleancity.service.api.IELogisticsDbLookUpService;
import com.cleancity.service.api.IFranchiseDriverService;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.*;

@Slf4j
@Service
@RequiredArgsConstructor
public class FranchiseDriverServiceImpl implements IFranchiseDriverService {

    @Autowired
    private FranchiseDriverRepository driverRepository;

    @Autowired
    private FranchiseRepository franchiseRepository;

    @Autowired
    private IELogisticsDbLookUpService dbLookUpService;

    @Autowired
    private BookingRepository bookingRepository;

    @Autowired
    private MessageSource messageSource;

 /*   @Override
    public GeneralResponse createFranchiseDriver(FranchiseDriver driver) {
        driverRepository.save(driver);
        return GeneralResponseBuilder.buildSuccess(driver, messageSource.getMessage("general.response.success.driver", new Object[0], new Locale("en")));
    }*/

    @Override
    public GeneralResponse updateFranchiseDriver(FranchiseDriver driver) {
        Optional<FranchiseDriver> driverVal = driverRepository.findById(driver.getDriverId());
        if (driverVal.isPresent()) {
            driverRepository.save(driver);
            return GeneralResponseBuilder.buildSuccess(driverVal.get(), messageSource.getMessage("general.response.success.UpdateDriver", new Object[0], new Locale("en")));

        } else {
            return GeneralResponseBuilder.buildFailed(null, messageSource.getMessage("general.response.failed.driveNotFound", new Object[0], new Locale("en")));
        }

    }

    @Override
    @Transactional("transactionManager")
    public GeneralResponse getFranchiseDriverById(Long driverId) {
        Optional<FranchiseDriver> driverVal = driverRepository.findById(driverId);
        if (driverVal.isPresent()) {
            FranchiseDriver franchiseDriver = driverVal.get();
            if (franchiseDriver.getELogisticsDriverId() != null) {
                Optional<Map<String, Object>> optionalDriverByUserId = dbLookUpService.getDriverByUserId(franchiseDriver.getELogisticsDriverId());
                if (optionalDriverByUserId.isPresent()) {
                    Map<String, Object> stringObjectMap = optionalDriverByUserId.get();
                    ObjectMapper oMapper = new ObjectMapper();
                    stringObjectMap.putAll(oMapper.convertValue(franchiseDriver, Map.class));
                    return GeneralResponseBuilder.buildSuccess(stringObjectMap, messageSource.getMessage("general.response.success", new Object[0], new Locale("en")));
                } else
                    return GeneralResponseBuilder.buildFailed(null, "User does not exist");
            }
        }
        return GeneralResponseBuilder.buildFailed(null, messageSource.getMessage("general.response.failed.not-assigned", new Object[0], new Locale("en")));
    }

    @Override
    @Transactional("transactionManager")
    public GeneralResponse approveDriver(Long driverId) {
        getAndSaveDriverDetails(driverId);
        Optional<FranchiseDriver> driverVal = driverRepository.findFranchiseDriverByeLogisticsDriverId(driverId);
        if (driverVal.isPresent()) {
            FranchiseDriver franchiseDriver = driverVal.get();
            franchiseDriver.setStatus(DriverStatus.ONLINE);
            return GeneralResponseBuilder.buildSuccess(driverRepository.save(franchiseDriver), messageSource.getMessage("general.response.success", new Object[0], new Locale("en")));
        }
        return GeneralResponseBuilder.buildFailed(null, "Failed.");
    }


    @Override
    @Transactional("transactionManager")
    public GeneralResponse getFranchiseDriverByElogisticsId(Long eLogisticsDriverId) {
        getAndSaveDriverDetails(eLogisticsDriverId);
        Optional<Map<String, Object>> optionalDriverByUserId = dbLookUpService.getDriverByUserId(eLogisticsDriverId);
        if (optionalDriverByUserId.isPresent()) {
            Map<String, Object> driverByUserId = optionalDriverByUserId.get();
            Optional<FranchiseDriver> driverVal = driverRepository.findFranchiseDriverByeLogisticsDriverId(Long.parseLong(driverByUserId.get("IDriverId").toString()));
            if (driverVal.isPresent()) {
                Map<String, Object> stringObjectMap = optionalDriverByUserId.get();
                ObjectMapper oMapper = new ObjectMapper();
                //todo: user better implementation
                stringObjectMap.putAll(oMapper.convertValue(driverVal.get(), Map.class));
                return GeneralResponseBuilder.buildSuccess(stringObjectMap, messageSource.getMessage("general.response.success.driver", new Object[0], new Locale("en")));
            }
            return GeneralResponseBuilder.buildFailed(null, messageSource.getMessage("general.response.failed.not-assigned", new Object[0], new Locale("en")));

        }
        return GeneralResponseBuilder.buildFailed(null, messageSource.getMessage("general.response.failed.driveNotFound", new Object[0], new Locale("en")));
    }


    @Override
    @Transactional("transactionManager")
    public GeneralResponse getFranchiseDriversByFranchiseId(Long franchiseId) {
        //todo::
        List<FranchiseDriver> driverByFranchiseId = driverRepository.getAllFranchiseDriverByFranchiseId(franchiseId);
        if (!driverByFranchiseId.isEmpty()) {

            return GeneralResponseBuilder.buildSuccess(driverByFranchiseId, messageSource.getMessage("general.response.success", new Object[0], new Locale("en")));
        }
        return GeneralResponseBuilder.buildSuccess(Collections.emptyList(), messageSource.getMessage("general.response.failed.driveNotFound", new Object[0], new Locale("en")));
    }

    @Override
    public GeneralResponse getFranchiseDriversByStatus(DriverStatus status) {
        List<FranchiseDriver> driversByStatus = driverRepository.getAllFranchiseDriversByStatus(status);
        if (!driversByStatus.isEmpty()) {
            return GeneralResponseBuilder.buildSuccess(driversByStatus, "Success");
        }
        return GeneralResponseBuilder.buildSuccess(Collections.emptyList(), "Empty");
    }

    @Override
    @Transactional("transactionManager")
    public GeneralResponse getAllFranchiseDrivers() {
        List<FranchiseDriver> driverList = driverRepository.findAll();
        return GeneralResponseBuilder.buildSuccess(driverList, messageSource.getMessage("general.response.success", new Object[0], new Locale("en")));
    }

    @Override
    public GeneralResponse getFranchiseDriverDocuments(Long eLogisticsId) {
        return GeneralResponseBuilder.buildSuccess(dbLookUpService.getDriverDocuments(eLogisticsId), messageSource.getMessage("general.response.success", new Object[0], new Locale("en")));
    }

    @Override
    @Transactional("transactionManager")
    public GeneralResponse assignFranchise(Long franchiseId, Long eLogisticsDriverId) {
        getAndSaveDriverDetails(eLogisticsDriverId);
        Optional<Franchise> optionalFranchise = franchiseRepository.findById(franchiseId);
        if (optionalFranchise.isEmpty())
            return GeneralResponseBuilder.buildFailed(null, "Franchise not found");
        if (optionalFranchise.get().getStatus().equals(Status.INACTIVE))
            return GeneralResponseBuilder.buildFailed(null, "Franchise not active");

        Optional<FranchiseDriver> optionalFranchiseDriver = getAndSaveDriverDetails(eLogisticsDriverId);
       
        if (optionalFranchiseDriver.isEmpty())
            return GeneralResponseBuilder.buildFailed(null, "Driver not available or inactive");

        if(optionalFranchiseDriver.get().getStatus().equals(DriverStatus.INACTIVE))
            return GeneralResponseBuilder.buildFailed(null, "Driver not active");

        FranchiseDriver franchiseDriver = optionalFranchiseDriver.get();
        franchiseDriver.setFranchiseId(franchiseId);
        return GeneralResponseBuilder.buildSuccess(driverRepository.save(franchiseDriver), messageSource.getMessage("general.response.success", new Object[0], new Locale("en")));
    }

    @Override
    public Optional<FranchiseDriver> getAndSaveDriverDetails(Long eLogisticsDriverId) {
        Optional<Map<String, Object>> optionalElogisticsDriver = dbLookUpService.getDriverByUserId(eLogisticsDriverId);

        if (optionalElogisticsDriver.isEmpty())
            return Optional.empty();

        Map<String, Object> elogisticsDriver = optionalElogisticsDriver.get();
        Optional<FranchiseDriver> optionalFranchiseDriver = driverRepository.findFranchiseDriverByeLogisticsDriverId(eLogisticsDriverId);
        FranchiseDriver franchiseDriver = new FranchiseDriver();
        franchiseDriver.setStatus(DriverStatus.INACTIVE);
        if (optionalFranchiseDriver.isPresent()) {
            franchiseDriver = optionalFranchiseDriver.get();
            if (franchiseDriver.getStatus().equals(DriverStatus.INACTIVE))
                return Optional.empty();
        }


        franchiseDriver.setELogisticsDriverId(eLogisticsDriverId);
        franchiseDriver.setPhoneNo(elogisticsDriver.get("phoneNumber")==null?"":String.valueOf(elogisticsDriver.get("phoneNumber")));
        franchiseDriver.setName(elogisticsDriver.get("name")==null?"":String.valueOf(elogisticsDriver.get("name")));
        franchiseDriver.setEmail(elogisticsDriver.get("VEmail")==null?"":String.valueOf(elogisticsDriver.get("VEmail")));
        franchiseDriver.setSurname(elogisticsDriver.get("surname")==null?"":String.valueOf(elogisticsDriver.get("surname")));
        franchiseDriver.setPartnerType(elogisticsDriver.get("partnerType")==null?"":String.valueOf(elogisticsDriver.get("partnerType")));

        driverRepository.save(franchiseDriver);
        return Optional.of(franchiseDriver);

    }

    @Override
    @Transactional("transactionManager")
    public GeneralResponse cancelBooking(DriverCancelBookingDTO cancelBookingDTO) {
        log.info("***************cancel-bookin ************************** \n{}\n,cancelBookingDTO");
        Optional<FranchiseDriver> optionalFranchiseDriver = driverRepository.findById(cancelBookingDTO.getICancelledByUserId());
        if (optionalFranchiseDriver.isPresent()) {
            Optional<Booking> optionalBooking = bookingRepository.findById(cancelBookingDTO.getBookingId());
            if (optionalBooking.isPresent()) {
                Booking booking = optionalBooking.get();
                booking.setDCanceledDate(LocalDateTime.now());
                booking.setECancelledBy(optionalFranchiseDriver.get().getPhoneNo());
                booking.setICancelledByUserId(cancelBookingDTO.getICancelledByUserId());
                booking.setVCancelReason(cancelBookingDTO.getVCancelReason());
                booking.setEStatus(BookingStatus.DRIVER_CANCELLATION_IN_PROGRESS);
                booking = bookingRepository.save(booking);
                return GeneralResponseBuilder.buildSuccess(booking, messageSource.getMessage("general.response.success", new Object[0], new Locale("en")));
            }
            return GeneralResponseBuilder.buildFailed(null, messageSource.getMessage("general.response.failed", new Object[0], new Locale("en")));
        }
        return GeneralResponseBuilder.buildFailed(null, messageSource.getMessage("general.response.failed", new Object[0], new Locale("en")));
    }


    /*    @Override
    public GeneralResponse getFranchiseDriverByVehicleId(Long vehicleId) {
        Optional <FranchiseDriver> driverByVehicleId = driverRepository.getAllFranchiseDriversByVehicleId(vehicleId);
        if (driverByVehicleId.isPresent()){

            return GeneralResponseBuilder.buildSuccess(driverByVehicleId.get(),messageSource.getMessage("general.response.success",new Object[0],new Locale("en")));

        }else {
            return GeneralResponseBuilder.buildFailed(null,messageSource.getMessage("general.response.failed.driveNotFound",new Object[0],new Locale("en")));
        }
    }*/
}
