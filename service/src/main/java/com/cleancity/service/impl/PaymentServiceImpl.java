package com.cleancity.service.impl;

import com.cleancity.common.UtilityFunctions;
import com.cleancity.common.enums.PaymentMethod;
import com.cleancity.common.enums.TransactionType;
import com.cleancity.domain.*;
import com.cleancity.domain.dto.*;
import com.cleancity.domain.embeddables.PersistenceDates;
import com.cleancity.persistence.BookingRepository;
import com.cleancity.persistence.CurrencyRepository;
import com.cleancity.persistence.CustomerRepository;
import com.cleancity.service.api.IPaymentService;
import com.cleancity.common.GeneralResponse;
import com.cleancity.common.GeneralResponseBuilder;
import com.cleancity.common.enums.PaymentStatus;
import com.cleancity.persistence.PaymentRepository;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpMethod;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.net.URISyntaxException;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.Optional;
@Service
@RequiredArgsConstructor
@Slf4j
public class PaymentServiceImpl implements IPaymentService {
    @Autowired
    PaymentRepository paymentRepository;

    @Autowired
    private MessageSource messageSource;

    @Autowired
    BookingRepository bookingRepository;

    @Autowired
    CustomerRepository customerRepository;

    @Autowired
    CurrencyRepository currencyRepository;

    //@Value("${general.response.success.newPayment}")
    private String newPayment_success="";

    @Value("${backend.api.payment.url}")
    private String ecocashEIPPaymentURL ;//= "https://dev.vayaafrica.com/ecocash-payment-module/v1/payments";
    @Value("${backend.api.payment.params.merchantCode}")
    private String merchantCode;
    @Value("${backend.api.payment.params.merchantNumber}")
    private String merchantNumber;
    @Value("${backend.api.payment.params.merchantPin}")
    private String merchantPin;
    @Value("${backend.api.payment.params.currency}")
    private String currency;

    @Value("${backend.api.payment.get-all-banks.url}")
    private String getAllBanksUrl;

    @Value("${backend.api.payment.bank.url}")
    private String bankPaymentUrl;

    @Value("${backend.api.payment.returnUrl}")
    private String returnUrl;

    @Value("${backend.api.payment.source}")
    private String sourceAppName;

    private final   RestTemplate restTemplate = new RestTemplate();

    @Override
    public GeneralResponse makeEcocashPayment(Payment payment) {
        Optional<Booking> optionalBooking = bookingRepository.findBookingByIBookingId(payment.getBookingId());
        if(optionalBooking.isEmpty())
            return GeneralResponseBuilder.buildFailed(null, "Invalid booking id");

        Optional<Payment> optionalPayment = paymentRepository.findPaymentByBookingId(payment.getBookingId());

        if(optionalPayment.isPresent() && optionalPayment.get().getPaymentStatus().equals(PaymentStatus.PAID))
            return GeneralResponseBuilder.buildFailed(null, "Payment already made");

        optionalPayment.ifPresent(payment1 -> {
            payment.setPaymentId(payment1.getPaymentId());
        });

        Double amount = payment.getCurrencyCode().equals("USD") ? optionalBooking.get().getFPrice() : optionalBooking.get().getZWLPrice();

        payment.setAmount(amount);
        paymentRepository.save(payment);

        EIPRequest request = new EIPRequest();
        request.setSource(sourceAppName);
        request.setReturnUrl(returnUrl);
        request.setSourceMsisdn(payment.getEcocashNumber());
        request.setDestinationMsisdn(merchantNumber);
        request.setMerchantCode(merchantCode);
        request.setAmount(amount);
        request.setReference(payment.getReference());
        request.setMerchantPin(merchantPin);
        //TODO: ASK IF USD IF SUPPORTED YET
        if(payment.getCurrencyCode().equals("USD")) currency = "USD";
        request.setCurrency(currency);
        request.setTransactionTime(LocalDateTime.now());
        log.info("\n\n************** EIPRequest****************\n {}\n\n", request);
        final RequestEntity<EIPRequest> requestEntity;
        try {
            requestEntity = new RequestEntity<>(request, HttpMethod.POST, new URI(ecocashEIPPaymentURL));
            log.info("\n\n************** EIP Request****************\n {}\n\n", requestEntity);

            final ResponseEntity<EIPResponse> responseEntity = restTemplate.exchange(requestEntity, EIPResponse.class);
            log.info("\n\n************** EIP Response****************\n {}\n\n", responseEntity);

        } catch (URISyntaxException e) {
            e.printStackTrace();
            return GeneralResponseBuilder.buildFailed(null, messageSource.getMessage("general.response.failed.newPayment",new Object[0],new Locale("en")));
        }
        return GeneralResponseBuilder.buildSuccess(payment, messageSource.getMessage("general.response.success.newPayment",new Object[0],new Locale("en")));
    }

    @Override
    public GeneralResponse getAllBanks() {
     try {
         Object response = restTemplate.getForObject(
                 getAllBanksUrl,
                 Object.class);
         return GeneralResponseBuilder.buildSuccess(response,"Successful");
     } catch(HttpClientErrorException e) {
         return GeneralResponseBuilder.buildFailed(null, "Failed to process. Please try again."+e.getLocalizedMessage());
     }
    }

    @Override
    public GeneralResponse createPayment(Payment payment) {
        Optional<Booking> optionalBooking = bookingRepository.findBookingByIBookingId(payment.getBookingId());
        if(optionalBooking.isEmpty())
            return GeneralResponseBuilder.buildFailed(null, "Invalid booking id");
        Optional<Payment> optionalPayment = paymentRepository.findPaymentByBookingId(payment.getBookingId());

        if(optionalPayment.isPresent()&&optionalPayment.get().getPaymentStatus().equals(PaymentStatus.PAID))
            return GeneralResponseBuilder.buildFailed(null, "Payment already made");

        if(optionalPayment.isPresent())
            payment.setPaymentId(optionalPayment.get().getPaymentId());
        Optional<Currency> currencyOptional = currencyRepository.findCurrencyByCode(payment.getCurrencyCode());
        if(currencyOptional.isEmpty())
            return GeneralResponseBuilder.buildFailed(null, "Unsupported currency.");


     //   payment.setAmount(optionalBooking.get().getCurrencyCode()..getFPrice());
        payment.setAmount(payment.getCurrencyCode().equals("USD")?optionalBooking.get().getFPrice():optionalBooking.get().getZWLPrice());

        payment =  paymentRepository.save(payment);
        return GeneralResponseBuilder.buildSuccess(payment, messageSource.getMessage("general.response.success.newPayment",new Object[0],new Locale("en")));
    }



    @Override
    public GeneralResponse updatePayment(Payment payment) {
         Optional<Payment> paymentVal = paymentRepository.findPaymentByPaymentId(payment.getPaymentId());
        if (paymentVal.isPresent()){
            paymentRepository.save(payment);
            return GeneralResponseBuilder.buildSuccess(paymentVal.get(),messageSource.getMessage("general.response.success.updatePayment",new Object[0],new Locale("en"))
            );
        }
        return GeneralResponseBuilder.buildFailed(null, messageSource.getMessage("general.response.fail.updatePayment",new Object[0],new Locale("en"))
        );
    }

    @Override
    public GeneralResponse updatePaymentStatus(UpdatePaymentStatusDTO paymentStatusDTO) {
        Optional<Payment> paymentVal = paymentRepository.findPaymentByPaymentId(paymentStatusDTO.getPaymentId());
        if (paymentVal.isPresent()){
            Payment   payment = paymentVal.get();
            payment.setPaymentStatus(paymentStatusDTO.getPaymentStatus());
            paymentRepository.save(payment);
            Optional<Booking> optionalBooking = bookingRepository.findBookingByIBookingId(payment.getBookingId());
            if(optionalBooking.isPresent()) {
                Booking booking =     optionalBooking.get();
                booking. setPaymentStatus(paymentStatusDTO.getPaymentStatus());
                bookingRepository.save(booking);

                return GeneralResponseBuilder.buildSuccess(paymentVal.get(),messageSource.getMessage("general.response.success.updatePayment",new Object[0],new Locale("en")));
            }
            return GeneralResponseBuilder.buildFailed(null, messageSource.getMessage("general.response.fail.updatePayment",new Object[0],new Locale("en")));
        }
        return GeneralResponseBuilder.buildFailed(null, messageSource.getMessage("general.response.fail.updatePayment",new Object[0],new Locale("en")));
    }


    @Override
    public GeneralResponse processEcocashPaymentCallback(EcocashRequestCallbackDTO requestCallback) {
        if(requestCallback.getTransactionStatus().equals("SUCCESS")){
            Optional<Payment> paymentVal = paymentRepository.findPaymentByReference(requestCallback.getPaymentReference());
            if (paymentVal.isPresent()) {
                Payment payment = paymentVal.get();
                payment.setPaymentStatus(PaymentStatus.PAID);
                paymentRepository.save(payment);
                Optional<Booking> optionalBooking = bookingRepository.findBookingByIBookingId(payment.getBookingId());
                if (optionalBooking.isPresent()) {
                    Booking booking = optionalBooking.get();
                    booking.setPaymentStatus(PaymentStatus.PAID);
                    bookingRepository.save(booking);
                }
            }
        } else log.info("\nPAYMENT FAILED:\n\n {} \n {} \n\n",requestCallback.getTransactionStatus(), requestCallback.getResponseMessage());
        return GeneralResponseBuilder.buildSuccess("", "Payment update received");
    }




    @Override
    public GeneralResponse getPaymentByPaymentId(Long paymentId) {
        Optional<Payment> paymentVal = paymentRepository.findPaymentByPaymentId(paymentId);
        if (paymentVal.isPresent()){
            return GeneralResponseBuilder.buildSuccess(paymentVal.get(), messageSource.getMessage("general.response.fail.getPayment",new Object[0],new Locale("en"))
            );
        }
        return GeneralResponseBuilder.buildFailed(null, messageSource.getMessage("general.response.fail.getPayment",new Object[0],new Locale("en"))
        );
    }

    @Override
    public GeneralResponse getPaymentsByCurrencyCode(String code) {
        List<Payment> paymentVal = paymentRepository.findPaymentByCurrencyCode(code);
        if (!paymentVal.isEmpty()){
            return GeneralResponseBuilder.buildSuccess(paymentVal, messageSource.getMessage("general.response.success.getPayment",new Object[0],new Locale("en"))
            );
        }
        return GeneralResponseBuilder.buildFailed(null, messageSource.getMessage("general.response.fail.getPayment",new Object[0],new Locale("en"))
        );
    }


    @Override
    public GeneralResponse getPaymentsByPaymentMethod(PaymentMethod paymentMethod) {
        List<Payment> paymentVal = paymentRepository.findPaymentsByPaymentMethod(paymentMethod);
        if (!paymentVal.isEmpty()){
            return GeneralResponseBuilder.buildSuccess(paymentVal,messageSource.getMessage("general.response.success.getPayment",new Object[0],new Locale("en"))
            );
        }
        return GeneralResponseBuilder.buildFailed(null, messageSource.getMessage("general.response.fail.getPayment",new Object[0],new Locale("en"))
        );
    }

    @Override
    public GeneralResponse getPaymentByBookingId(Long bookingId) {
        Optional<Payment> paymentVal = paymentRepository.findPaymentByBookingId(bookingId);
        if (paymentVal.isPresent()){
            return GeneralResponseBuilder.buildSuccess(paymentVal.get(), messageSource.getMessage("general.response.success.getPayment",new Object[0],new Locale("en"))
            );
        }
        return GeneralResponseBuilder.buildFailed(null, messageSource.getMessage("general.response.fail.getPayment",new Object[0],new Locale("en"))
        );
    }

    @Override
    public GeneralResponse getPaymentsByPaymentStatus(PaymentStatus paymentStatus) {
        List<Payment> paymentVal = paymentRepository.findPaymentsByPaymentStatus(paymentStatus);
        if (!paymentVal.isEmpty()){
            return GeneralResponseBuilder.buildSuccess(paymentVal, messageSource.getMessage("general.response.success.getPayment",new Object[0],new Locale("en"))
            );
        }
        return GeneralResponseBuilder.buildFailed(null, messageSource.getMessage("general.response.fail.getPayment",new Object[0],new Locale("en"))
        );
    }

    @Override
    public GeneralResponse getPaymentsByPaidBy(String paidBy) {
        List<Payment> paymentVal = paymentRepository.findPaymentsByPaidBy(paidBy);
        if (!paymentVal.isEmpty()){
            return GeneralResponseBuilder.buildSuccess(paymentVal, messageSource.getMessage("general.response.success.getPayment",new Object[0],new Locale("en"))
            );
        }
        return GeneralResponseBuilder.buildFailed(null, messageSource.getMessage("general.response.fail.getPayment",new Object[0],new Locale("en"))
        );
    }

    @Override
    public GeneralResponse getPaymentByApprovedBy(String approvedBy) {
        List<Payment> paymentVal = paymentRepository.findPaymentByApprovedBy(approvedBy);
        if (!paymentVal.isEmpty()){
            return GeneralResponseBuilder.buildSuccess(paymentVal, messageSource.getMessage("general.response.success.getPayment",new Object[0],new Locale("en"))
            );
        }
        return GeneralResponseBuilder.buildFailed(null, messageSource.getMessage("general.response.fail.getPayment",new Object[0],new Locale("en"))
        );
    }

    @Override
    public GeneralResponse getAllPayments() {
        return GeneralResponseBuilder.buildSuccess(paymentRepository.findAll(),
                messageSource.getMessage("general.response.success.getPayment",new Object[0],new Locale("en"))
        );
    }

    @Override
    public GeneralResponse makeBankPayment(BankPaymentDTO paymentDTO) {
        Optional<Payment> optionalPayment = paymentRepository.findPaymentByBookingId(paymentDTO.getBookingId());
        if(optionalPayment.isPresent()&&optionalPayment.get().getPaymentStatus().equals(PaymentStatus.PAID))
            return GeneralResponseBuilder.buildFailed(null, "Payment already made");

        Optional<Booking> optionalBooking = bookingRepository.findBookingByIBookingId(paymentDTO.getBookingId());
        if(optionalBooking.isEmpty())
            return GeneralResponseBuilder.buildFailed(null, "Invalid booking id");
        Booking booking = optionalBooking.get();
        Optional<Customer> optionalCustomer = customerRepository.findCustomerByeLogisticsUserId(booking.getIUserId());
        if(optionalCustomer.isEmpty())
            return GeneralResponseBuilder.buildFailed(null, "Customer not found");
         if(!paymentDTO.getPaymentMethod().equals(PaymentMethod.INTERNAL_FUNDS_TRANSFER)&&!paymentDTO.getPaymentMethod().equals(PaymentMethod.RTGS))
            return GeneralResponseBuilder.buildFailed(null, "Payment method not available on bank");
        Customer customer = optionalCustomer.get();
        String reference = UtilityFunctions.generateReference();
        Payment payment =
                Payment.builder()

                        .currencyCode(paymentDTO.getCurrencyCode())
                        .paymentMethod(paymentDTO.getPaymentMethod())
                        .paymentDateTime(LocalDateTime.now())
                        .amount(paymentDTO.getCurrencyCode().equals("USD")?optionalBooking.get().getFPrice():optionalBooking.get().getZWLPrice())
                        .bookingId(paymentDTO.getBookingId())
                        //.paidBy(paymentDTO.getPaidBy())
                        .persistenceDates(new PersistenceDates())
                        .reference(reference)
                        .ecocashNumber(paymentDTO.getEcocashNumber())
                        .paymentStatus(PaymentStatus.UNPAID)
                        .bankCode(paymentDTO.getBankCode())
                        .customerAccount(paymentDTO.getCustomerAccount())
                        .msisdn(customer.getPhoneNo())
                        .build();

        if(customer.getPartnerType()!=null) payment.setCustomerName(customer.getPartnerType().equals("Individual")?customer.getName()+" "+customer.getSurname(): customer.getCompanyName());

        optionalPayment.ifPresent(payment1 -> {
          payment.setPaymentId(payment1.getPaymentId());
      });

         paymentRepository.save(payment);

       BankPaymentRequest bankPaymentRequest = BankPaymentRequest.builder()
               . bankCode(payment.getBankCode())
               .amount(String.valueOf(payment.getAmount()))
               .currency(payment.getCurrencyCode())
               .customerName(payment.getCustomerName())
               .reason("Clean city Waste management payment")
               .jobId(String.valueOf(booking.getIBookingId()))
               .reference(reference)
               .customerAccount(payment.getCustomerAccount())
               .transactionType(TransactionType.valueOf(payment.getPaymentMethod().name()))
               .msisdn(payment.getMsisdn())
               .build();



        try {
            final RequestEntity<BankPaymentRequest>    requestEntity = new RequestEntity<>(bankPaymentRequest, HttpMethod.POST, new URI(bankPaymentUrl));
            log.info("\n\n************** BANK Request****************\n {}\n\n", requestEntity);

            final ResponseEntity<String> responseEntity = restTemplate.exchange(requestEntity, String.class);
            log.info("\n\n************** BANK Response****************\n {}\n\n", responseEntity);

            ObjectMapper objectMapper = new ObjectMapper();

            JsonNode jsonNode = objectMapper.readTree(Objects.requireNonNull(responseEntity.getBody()));



            return GeneralResponseBuilder.buildSuccess(payment,jsonNode.get("message") .asText());

        } catch (URISyntaxException | JsonProcessingException e) {
            e.printStackTrace();
            return GeneralResponseBuilder.buildFailed(payment, messageSource.getMessage("general.response.failed.newPayment",new Object[0],new Locale("en")));
        }

    }
}
