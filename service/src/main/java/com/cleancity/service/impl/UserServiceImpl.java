package com.cleancity.service.impl;

import com.cleancity.common.enums.Status;
import com.cleancity.domain.Customer;
import com.cleancity.persistence.CustomerRepository;
import com.cleancity.persistence.elogistics.repository.GenericRepo;
import com.cleancity.service.api.IUserService;
import com.cleancity.common.GeneralResponse;
import com.cleancity.common.GeneralResponseBuilder;
import com.cleancity.domain.User;
import com.cleancity.persistence.UserRepository;
import com.cleancity.service.api.PasswordEncryptionService;
import com.google.gson.Gson;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

import java.util.*;

@Slf4j
@Service
@RequiredArgsConstructor
public class UserServiceImpl implements IUserService {

    @Autowired
    UserRepository userRepository;
    @Autowired
    CustomerRepository customerRepository;
    @Autowired
    private MessageSource messageSource;
    @Autowired
    PasswordEncryptionService passwordEncryptionService;

    final GenericRepo genericRepo;

    @Value("${elogistics.user.lookup.by-username-or-email}")
    String userLookUpByUsernameOrEmailQuery;

    @Value("${elogistics.driver.lookup.all}")
    String driverLookUpAllQuery;

    @Value("${elogistics.user.lookup.all}")
    String userLookUpAllQuery;

    @Value("${elogistics.user.update.login-attempts}")
    String updateLoginAttemptsQuery;

    @Override
    public GeneralResponse getUserByUserId(Long userId) {
        Optional<User> UserVal = userRepository.findUserByUserId(userId);
        if (UserVal.isPresent()){
            return GeneralResponseBuilder.buildSuccess(UserVal.get(),  messageSource.getMessage("general.response.success.getUser",new Object[0],new Locale("en")) );
        }
        return GeneralResponseBuilder.buildFailed(null,  messageSource.getMessage("general.response.fail.getUser",new Object[0],new Locale("en")));
    }

    @Override
    public GeneralResponse getAllUsers() {
        return GeneralResponseBuilder.buildSuccess(userRepository.findAll(),  messageSource.getMessage("general.response.success.getUser",new Object[0],new Locale("en")));
    }


    @Override
    public GeneralResponse getAllElogisticsDrivers() {
        List<Map<String, Object>> drivers = genericRepo.getGenericMapList(driverLookUpAllQuery);
        if(drivers.isEmpty()){
            return  GeneralResponseBuilder.buildFailed(null, "No drivers" );
        } return GeneralResponseBuilder.buildSuccess(drivers,  "Successful" );
    }

    @Override
    public GeneralResponse getAllElogisticsUsers() {
        List<Map<String, Object>> drivers = genericRepo.getGenericMapList(userLookUpAllQuery);
        if(drivers.isEmpty()){
            return  GeneralResponseBuilder.buildFailed(null, "No users" );
        } return GeneralResponseBuilder.buildSuccess(drivers,  "Successful" );
    }

    @Override
    public GeneralResponse login(String username, String password){
        Map<String, Object> lookupParams = new HashMap<>();
        lookupParams.put("username", username);
       log.info("look up query {}", userLookUpByUsernameOrEmailQuery);

        List<Map<String, Object>> genericList = genericRepo.getGenericList(lookupParams, userLookUpByUsernameOrEmailQuery);

        if (!genericList.isEmpty()){
            //validate login attempts
            //id, VEmail, creationStatus, password, created_date,
            // creation_status, modified_date, phoneNumber, resetToken,
            // username, fistname, lastname, firstname, userid, loginAttempts
            Map<String, Object> user = genericList.get(0);
            int attempts = Integer.parseInt(String.valueOf(user.get("loginAttempts")));
            if(attempts>=3){
                return   GeneralResponseBuilder.buildFailed(null,"Login failed, please reset your password");
            }
            final String storedPassword = String.valueOf(user.get("password"));
            if(passwordEncryptionService.checkPassword(password, storedPassword)){
                //reset failed login attempts
               // user.get().setLoginAttempts(0);
                lookupParams.put("loginAttempts",0);
                genericRepo.genericInsert(lookupParams,updateLoginAttemptsQuery );
                //persist the login attempts
               // userRepository.save(user.get());
               // Gson gson = new Gson();
                user.remove("password");
               return  GeneralResponseBuilder.buildSuccess(user,  "Login successful");
            }
            else {
                //accumulate failed Login attempts
              //  user.get().setLoginAttempts(user.get().getLoginAttempts() + 1);
                lookupParams.put("loginAttempts", attempts+1);
                genericRepo.genericInsert(lookupParams,updateLoginAttemptsQuery );

                //persist the login attempts
               // userRepository.save(user.get());
                return GeneralResponseBuilder.buildFailed(null,  "Invalid Login Credentials. Please try again.");
            }
        }

        return   GeneralResponseBuilder.buildFailed(null,"Invalid Login Credentials. Please try again.");
    }


}



