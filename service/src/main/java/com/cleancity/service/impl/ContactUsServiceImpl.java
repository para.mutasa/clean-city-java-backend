package com.cleancity.service.impl;

import com.cleancity.common.GeneralResponse;
import com.cleancity.common.GeneralResponseBuilder;
import com.cleancity.domain.ContactUs;
import com.cleancity.persistence.ContactUsRepository;
import com.cleancity.service.api.IContactUsService;
import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Locale;
import java.util.Optional;

@Service
@AllArgsConstructor
public class ContactUsServiceImpl implements IContactUsService {


    @Autowired
    ContactUsRepository contactUsRepository;

    @Autowired
    private MessageSource messageSource;



    @Override
    public GeneralResponse createContactUS(ContactUs contactUs) {

        contactUsRepository.save(contactUs);
        return GeneralResponseBuilder.buildSuccess(contactUs, messageSource.getMessage("general.response.success.newMessage",new Object[0],new Locale("en")));
    }

    @Override
    public GeneralResponse getContactUsByPhone(String phone) {
        List<ContactUs> contactVal = contactUsRepository.findContactUsByPhone(phone);
        if (!contactVal.isEmpty()){
           return GeneralResponseBuilder.buildSuccess(contactVal, messageSource.getMessage("general.response.success.getMessage",new Object[0],new Locale("en")));
        }
        return GeneralResponseBuilder.buildFailed(null, messageSource.getMessage("general.response.fail.getMessage",new Object[0],new Locale("en")));
    }

    @Override
    public GeneralResponse getContactUSByByEmail(String email) {
        List<ContactUs> contactVal = contactUsRepository.findContactUsByEmail(email);
        if (!contactVal.isEmpty()){
           return GeneralResponseBuilder.buildSuccess(contactVal, messageSource.getMessage("general.response.success.getMessage",new Object[0],new Locale("en")));
        }
        return GeneralResponseBuilder.buildFailed(null, messageSource.getMessage("general.response.fail.getMessage",new Object[0],new Locale("en")));
    }



    @Override
    public GeneralResponse getContactUsById(Long Id) {
        Optional<ContactUs> contactVal = contactUsRepository.findById(Id);
        if (contactVal.isPresent()){
            return GeneralResponseBuilder.buildSuccess(contactVal.get(), messageSource.getMessage("general.response.success.getMessage",new Object[0],new Locale("en")) );
        }
        return GeneralResponseBuilder.buildFailed(null, messageSource.getMessage("general.response.fail.getMessage",new Object[0],new Locale("en")));
    }

    @Override
    public GeneralResponse getAllContactUs() {
        return GeneralResponseBuilder.buildSuccess(contactUsRepository.findAll(), messageSource.getMessage("general.response.success.getMessage",new Object[0],new Locale("en")) );

    }


}

