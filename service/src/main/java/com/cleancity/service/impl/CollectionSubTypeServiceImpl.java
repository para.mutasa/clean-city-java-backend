package com.cleancity.service.impl;

import com.cleancity.common.enums.CollectionType;
import com.cleancity.service.api.ICollectionSubTypeService;
import com.cleancity.common.GeneralResponse;
import com.cleancity.common.GeneralResponseBuilder;
import com.cleancity.domain.CollectionSubType;
import com.cleancity.persistence.CollectionSubTypeRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Locale;
import java.util.Optional;
@Service
@RequiredArgsConstructor
public class CollectionSubTypeServiceImpl implements ICollectionSubTypeService {



    @Autowired
    private CollectionSubTypeRepository collectionSubTypeRepository;

    @Autowired
    private MessageSource messageSource;

    @Value("${general.response.success.collectionSubtype}")
    private String success_message="";

    @Value("${general.response.failed.collectionSubtype}")
    private String failed_message="";

    @Override
    public GeneralResponse createCollectionSubType(CollectionSubType collectionSubType) {
        List<CollectionSubType> collectionSubTypes = collectionSubTypeRepository.findCollectionSubTypesByCollectionType(CollectionType.HouseHoldCollection);
        if(collectionSubTypes.size() >= 1 && collectionSubType.getCollectionType().equals(CollectionType.HouseHoldCollection))
            return GeneralResponseBuilder.buildFailed(null, "Collection subtype for Household already available");
        return GeneralResponseBuilder.buildSuccess(collectionSubTypeRepository.save(collectionSubType), messageSource.getMessage("general.response.success.collectionSubtype",new Object[0],new Locale("en")));
    }


    @Override
    public GeneralResponse updateCollectionSubType(CollectionSubType collectionSubType) {
        Optional<CollectionSubType> collectionSubTypeOptional = collectionSubTypeRepository.findById(collectionSubType.getId());

        if (collectionSubTypeOptional.isEmpty())
            return GeneralResponseBuilder.buildFailed(collectionSubType, "Failed to update.");

        List<CollectionSubType> collectionSubTypes = collectionSubTypeRepository.findCollectionSubTypesByCollectionType(CollectionType.HouseHoldCollection);

        if(collectionSubType.getCollectionType().equals(CollectionType.HouseHoldCollection)&&collectionSubTypes.size()>=1&& !collectionSubType.getId().equals(collectionSubTypeOptional.get().getId()))
            return GeneralResponseBuilder.buildFailed(null, "Collection subtype for Household already available");

            return GeneralResponseBuilder.buildSuccess(collectionSubTypeRepository.save(collectionSubType), messageSource.getMessage("general.response.success.collectionSubtype",new Object[0],new Locale("en")));
          }


    @Override
    public GeneralResponse getCollectionSubTypeById(Long collectionSubTypeId) {
        Optional<CollectionSubType> collectionSubType = collectionSubTypeRepository.findById(collectionSubTypeId);

        if (collectionSubType.isPresent()){
           return GeneralResponseBuilder.buildSuccess(null, messageSource.getMessage("general.response.success.collectionSubtype",new Object[0],new Locale("en")));
        }
        return GeneralResponseBuilder.buildFailed(null, messageSource.getMessage("general.response.fail.collectionSubtype",new Object[0],new Locale("en")));
    }


    @Override
    public GeneralResponse getAllCollectionSubType() {
        List<CollectionSubType> collectionSubTypes = collectionSubTypeRepository.findAll();
        return GeneralResponseBuilder.buildSuccess(collectionSubTypes, messageSource.getMessage("general.response.success.collectionSubtype",new Object[0],new Locale("en")));
    }


    @Override
    public GeneralResponse getAllCollectionSubTypeByCollectionType(CollectionType collectionType) {
        List<CollectionSubType>collectionSubType = collectionSubTypeRepository.findCollectionSubTypesByCollectionType(collectionType);
        if (!collectionSubType.isEmpty()){
            return GeneralResponseBuilder.buildSuccess(collectionSubType, messageSource.getMessage("general.response.success.collectionSubtype",new Object[0],new Locale("en")));
        }
        return GeneralResponseBuilder.buildFailed(null, messageSource.getMessage("general.response.fail.collectionSubtype",new Object[0],new Locale("en")));
    }
}
