package com.cleancity.service.impl;


import com.cleancity.common.GeneralResponse;
import com.cleancity.common.GeneralResponseBuilder;
import com.cleancity.common.enums.PaymentStatus;
import com.cleancity.domain.Booking;
import com.cleancity.domain.Customer;
import com.cleancity.domain.DocumentProperties;
import com.cleancity.domain.Payment;
import com.cleancity.domain.embeddables.PersistenceDates;
import com.cleancity.persistence.BookingRepository;
import com.cleancity.persistence.CustomerRepository;
import com.cleancity.persistence.DocumentPropertiesRepository;
import com.cleancity.persistence.PaymentRepository;
import com.cleancity.service.api.IDocumentStorageService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.usertype.UserType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Slf4j
public class DocumentStorageServiceImpl implements IDocumentStorageService {

    private final DocumentPropertiesRepository documentPropertiesRepository;
    //private final Path fileStorageLocation;
    //@Value("${file.upload-dir.local}")
    //private String uploadDir;

    @Value("${file.download.url}")
    private String downloadUrl;

    @Value("${file.upload-dir.local}")
    private Path fileStorageLocation;

    //private String uploadDir = String.valueOf(fileStorageLocation);


    @Autowired
    private  final BookingRepository bookingRepository;

    @Autowired
    private  final PaymentRepository paymentRepository;

    @Autowired
    private  final CustomerRepository customerRepository;

    @Autowired
    private final CustomerServiceImpl customerService;





    @Override
    public GeneralResponse storeProofOfPayment(MultipartFile file, String reference, Long bookinId){

        Optional<Booking> optionalBooking = bookingRepository.findById (bookinId);
        if (optionalBooking.isEmpty())
            return GeneralResponseBuilder.buildFailed("","Invalid booking Id ");
        Booking booking = optionalBooking.get();
        //Normalize file name
        String originalFileName = StringUtils.cleanPath(Objects.requireNonNull(file.getOriginalFilename()));
        String fileName = "";
        try {
            // Check if the file's name contains invalid characters
            if (originalFileName.contains("..")){
                return  GeneralResponseBuilder.buildSuccess("",  "Sorry! Filename contains invalid path sequence".concat(originalFileName));
            }

            String fileExtension = "";
            try {
                fileExtension = originalFileName.substring(originalFileName.lastIndexOf("."));

            } catch (Exception e) {
                log.info("Invalid file extension:  {} ", e.getMessage() );
                return GeneralResponseBuilder.buildFailed (e," Error! Invalid file extension" );
            }
            fileName = booking.getIUserId() +  "_" +bookinId + fileExtension;

            //Copy file to the target location (Replacing existing file with the same name)
            Path targetLocation = this.fileStorageLocation.resolve(fileName.trim().replace(" ", "_"));

            Files.copy(file.getInputStream(), targetLocation, StandardCopyOption.REPLACE_EXISTING);

            Optional<DocumentProperties> document = documentPropertiesRepository
                    .findByUserIdAndBookingId(booking.getIUserId(),bookinId);
            if (document.isPresent()){
                final DocumentProperties doc = document.get();
                doc.setDocumentFormat(file.getContentType());
                doc.setFileName(fileName);
                doc.setUploadDir(fileStorageLocation.toString());

                //update the existing document
                documentPropertiesRepository.save(doc);
            }
            else {


                Optional<Payment> optionalPayment = paymentRepository.findPaymentByBookingId(bookinId);
                 Payment payment = new Payment();
                if (optionalPayment.isPresent())
                    payment = optionalPayment.get();
                payment.setAmount(booking.getFPrice());
                payment.setPaymentStatus(PaymentStatus.UNPAID);
                payment.setCurrencyCode(booking.getCurrencyCode());
                payment.setBookingId(booking.getIBookingId());
                payment.setPaidBy(booking.getIUserId());
                payment.setReference(reference);
                payment.setPaymentDateTime(LocalDateTime.now());

                DocumentProperties doc = DocumentProperties.builder()
                        .userId(booking.getIUserId())
                        .bookingId(bookinId)
                        .documentFormat(file.getContentType())
                        .fileName(fileName)
                        .uploadDir(fileStorageLocation.toString())
                        .persistenceDates(PersistenceDates.builder()
                                .createdDate(LocalDateTime.now())
                                .modifiedDate(LocalDateTime.now())
                                .build())
                        .build();

                //save the new document
                documentPropertiesRepository.save(doc);


                booking.setProofOfPayment(fileName);
                bookingRepository.save(booking);


                payment.setProofOfPayment(fileName);
                paymentRepository.save(payment);

                Map<String, Object> response = new HashMap<>();
                response.put("downloadBaseUrl", downloadUrl);
                response.put("filename", fileName);
                return GeneralResponseBuilder.buildSuccess(response,"Document uploaded successfully"); //LBL_UPLOAD_SUCCESS

            }

            return GeneralResponseBuilder.buildSuccess("","Document updated successfully"); //LBL_UPLOAD_SUCCESS
        } catch (IOException e) {
            log.info("Could not upload file: {}  {}",  fileName, e.getMessage() );
            return GeneralResponseBuilder.buildFailed(null,"Could not upload file: " + fileName + ". Please try again!");
        }
    }



    @Override
    public ResponseEntity<Resource> loadFileAsResource(Long iDriverId,Long bookingId, HttpServletRequest request){
        final Optional<DocumentProperties> optionalDoc = documentPropertiesRepository
                .findByUserIdAndBookingId(iDriverId,bookingId);

        //TODO: validate docType
        if (optionalDoc.isPresent()){
            String fileName = optionalDoc.get().getFileName();
            if (fileName != null && !fileName.isEmpty()){
                try {
                    Path filePath = this.fileStorageLocation.resolve(fileName).normalize();
                    Resource resource = new UrlResource(filePath.toUri());
                    if (resource.exists()){
                        String contentType = "";

                        //Try to determine file content type
                        try {
                            contentType = request.getServletContext().getMimeType(resource.getFile().getAbsolutePath());
                        } catch (IOException e) {
                            log.info("could not determine file type: {} ", e.getStackTrace());
                            e.printStackTrace();
                        }

                        //Fallback to the default content type if type could not be determined
                        if (contentType.isEmpty())
                            contentType = "application/octet-stream";
                        return ResponseEntity
                                .ok()
                                .contentType(MediaType.parseMediaType(contentType))
                                .header(HttpHeaders.CONTENT_DISPOSITION,
                                        "attachment; filename\"" + resource.getFilename() + "\"")
                                .body(resource);
                    }
                    else {
                        return new ResponseEntity<Resource>(HttpStatus.NOT_FOUND);
                    }
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                    log.info("file not found : {}  {}", fileName, e.getStackTrace());
                    return new ResponseEntity<Resource>(HttpStatus.NOT_FOUND);
                }
            }else
                return new ResponseEntity<Resource>(HttpStatus.NO_CONTENT);

        } else {
            return new ResponseEntity<Resource>(HttpStatus.NOT_FOUND);
        }

    }

    @Override
    public GeneralResponse storeCustomerDocuments(MultipartFile nationalId, MultipartFile proofOfResidence, MultipartFile bankStatement, MultipartFile tradeLicense, Long userId) {

        customerService.getAndUpdateCustomerDetails(userId);
        Optional<Customer> optionalCustomer = customerRepository.findCustomerByeLogisticsUserId (userId);
        if (optionalCustomer.isEmpty())
            return GeneralResponseBuilder.buildFailed("","Customer not found ");
        Customer customer = optionalCustomer.get();

        Optional<String> optionalNationalId = saveCustomerDocument("nationalId", nationalId, customer);
        if(optionalNationalId.isEmpty()) return GeneralResponseBuilder.buildFailed(null,"Could not upload file: " + nationalId.getName() + ". Please try again!");
        customer.setNationId(optionalNationalId.get());

        Optional<String> optionalProofOfResidence = saveCustomerDocument("proofOfResidence", proofOfResidence, customer);
        if(optionalProofOfResidence.isEmpty()) return GeneralResponseBuilder.buildFailed(null,"Could not upload file: " + proofOfResidence.getName() + ". Please try again!");
        customer.setProofOfResidence(optionalProofOfResidence.get());

        Optional<String> optionalBankStatement = saveCustomerDocument("bankStatement", bankStatement, customer);
        optionalBankStatement.ifPresent(customer::setBankStatement);

        Optional<String> optionalTradeLicense = saveCustomerDocument("tradeLicense", tradeLicense, customer);
        optionalTradeLicense.ifPresent(customer::setTradeLicense);

     return GeneralResponseBuilder.buildSuccess(customerRepository.save(customer),"Documents updated successfully"); //LBL_UPLOAD_SUCCESS

    }



    Optional<String> saveCustomerDocument(String documentType, MultipartFile file, Customer customer){
        if(file==null)
            return Optional.empty();
        String originalFileName = StringUtils.cleanPath(Objects.requireNonNull(file.getOriginalFilename()));

        try{
            if (originalFileName.contains("..")) return  Optional.empty();
            String fileExtension = originalFileName.substring(originalFileName.lastIndexOf("."));
            String  fileName = customer.getELogisticsUserId() +  "_" +documentType + fileExtension;
            //Copy file to the target location (Replacing existing file with the same name)
            Path targetLocation = this.fileStorageLocation.resolve(fileName.trim().replace(" ", "_"));
            Files.copy(file.getInputStream(), targetLocation, StandardCopyOption.REPLACE_EXISTING);
            return Optional.of(fileName);
        }catch (Exception e){
            e.printStackTrace();
            return  Optional.empty();
        }

    }


}
