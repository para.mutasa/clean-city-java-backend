package com.cleancity.service.impl;

import com.cleancity.common.enums.Status;
import com.cleancity.domain.ExchangeRateHistory;
import com.cleancity.persistence.ExchangeRateHistoryRepository;
import com.cleancity.service.api.ICurrencyService;
import com.cleancity.common.GeneralResponse;
import com.cleancity.common.GeneralResponseBuilder;
import com.cleancity.domain.Currency;
import com.cleancity.persistence.CurrencyRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
@Service
@RequiredArgsConstructor
public class CurrencyServiceImpl implements ICurrencyService {

    @Autowired
    private CurrencyRepository currencyRepository;

    @Autowired
    private ExchangeRateHistoryRepository exchangeRateHistoryRepository;

    @Autowired
    private MessageSource messageSource;

    @Override
    public GeneralResponse createCurrency(Currency currency) {

        if(!currency.getCode().toUpperCase().equals("ZWL")||!currency.getCode().toUpperCase().equals("USD"))
            return GeneralResponseBuilder.buildFailed(null,"Currency should either be USD or ZWL");

        Optional<Currency> currencyVal = currencyRepository.findCurrencyByCode(currency.getCode());

        if(currencyVal.isEmpty()){
            //todo check names
            Currency savedCurrency = currencyRepository.save(currency);
            ExchangeRateHistory rateHistory = ExchangeRateHistory.builder()
                    .currency(currency.getName())
                    .oldRate(0)
                    .newRate(currency.getExchangeRate())
                    .dateChanged(LocalDateTime.now())
                    .build();
            exchangeRateHistoryRepository.save(rateHistory);
            return GeneralResponseBuilder.buildSuccess(savedCurrency,messageSource.getMessage("general.response.success.currency",new Object[0],new Locale("en")));
        }
        return GeneralResponseBuilder.buildFailed(null, messageSource.getMessage("general.response.currency.alreadyExisting",new Object[0],new Locale("en")));
    }

/*    @Override
    public GeneralResponse getDefaultCurrency() {
        return currencyRepository.findByDefault(true)
                .map(currency -> GeneralResponseBuilder.buildSuccess(currency,messageSource.getMessage("general.response.success.currency",new Object[0],new Locale("en")))
                ).orElseGet(() -> {
                    Currency currency = new Currency();
                    currency.setName("ZWL");
                    currency.setCode("ZWL");
                    currency.setDefault(true);
                    currency.setSymbol("ZWL");
                    currency.setExchangeRate(1);
                    return GeneralResponseBuilder.buildSuccess(currencyRepository.save(currency),messageSource.getMessage("general.response.success.currency",new Object[0],new Locale("en")));
                });
    }*/

    @Override
    public GeneralResponse updateCurrency(Currency currency) {

        if(!currency.getCode().toUpperCase().equals("ZWL")||!currency.getCode().toUpperCase().equals("USD"))
            return GeneralResponseBuilder.buildFailed(null,"Currency should either be USD or ZWL");

        Optional<Currency> currencyVal= currencyRepository.findById(currency.getCurrencyId());
        if (currencyVal.isPresent()){
             currencyRepository.save(currency);

         if(currency.getExchangeRate() != currencyVal.get().getExchangeRate()){
             ExchangeRateHistory rateHistory = ExchangeRateHistory.builder()
                     .currency(currencyVal.get().getName())
                     .oldRate(currencyVal.get().getExchangeRate())
                     .newRate(currency.getExchangeRate())
                     .dateChanged( LocalDateTime.now())
                     .build();
             exchangeRateHistoryRepository.save(rateHistory);
         }


            return GeneralResponseBuilder.buildSuccess(currencyVal.get(),messageSource.getMessage("general.response.success.UpdateCurrency",new Object[0],new Locale("en")));

        }else {
            return GeneralResponseBuilder.buildFailed(null,messageSource.getMessage("general.response.failed.UpdateCurrency",new Object[0],new Locale("en")));
        }
    }

    @Override
    public GeneralResponse getCurrencyById(Long currencyId) {
        Optional<Currency> currencyById=currencyRepository.findById(currencyId);
        if (currencyById.isPresent()){

            return GeneralResponseBuilder.buildSuccess(currencyById,messageSource.getMessage("general.response.success",new Object[0],new Locale("en")));

        }else {
            return GeneralResponseBuilder.buildFailed(null,messageSource.getMessage("general.response.currency.notfound",new Object[0],new Locale("en")));
        }

    }

    @Override
    public GeneralResponse getCurrencyByCode(String code) {
        Optional<Currency> currencyOptional = currencyRepository.findCurrencyByCode(code);
        if (currencyOptional.isPresent()){
            return GeneralResponseBuilder.buildSuccess(currencyOptional.get(),messageSource.getMessage("general.response.success",new Object[0],new Locale("en")));
        }else {
            return GeneralResponseBuilder.buildFailed(null,messageSource.getMessage("general.response.currency.notfound",new Object[0],new Locale("en")));
        }
    }

    @Override
    public GeneralResponse getAllCurrency() {
        List<Currency> currencies= currencyRepository.findAll();
        return GeneralResponseBuilder.buildSuccess(currencies,messageSource.getMessage("general.response.success",new Object[0],new Locale("en")));
    }

    @Override
    public  GeneralResponse getCurrencyByStatus(Status status) {
        List<Currency> currencyByStatus= currencyRepository.findAllByStatus(status);
        return GeneralResponseBuilder.buildSuccess(currencyByStatus,messageSource.getMessage("general.response.success",new Object[0],new Locale("en")));
    }
}
